package lukado.blik.configuration

object PreferenceConfig {
    /** Language */
    const val LANGUAGE = "locale"

    /** User info */
    const val EXISTING_USER = "logged.before"
    const val USER_LOGIN_TYPE = "login.provider"
    const val EMAIL = "custom.email"
    const val EMAIL_PASSWORD = "custom.password"
    const val USERNAME = "username"
    const val AGE = "age"
    const val GENDER = "gender"

    /** Settings */
    const val MUSIC_ENABLED = "music.enabled"
}