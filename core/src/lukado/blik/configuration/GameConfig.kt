package lukado.blik.configuration

object GameConfig {
    /** Box2D world settings */
    private const val FPS = 60f
    // World refresh rate per used by renderers
    const val STEP_TIME = 1 / FPS

    /** Abstract games settings */
    val COUNTDOWN_TIME = if (Config.RELEASE) 3f else 1f
    val GAME_DIFFICULTIES = IntArray(10){it+1}
    val GAME_CUSTOM_TIMERS = IntArray(10){(it+1)*30}
}