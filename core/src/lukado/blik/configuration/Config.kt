package lukado.blik.configuration

object Config {
    /** Screen virtual size  */
    const val APP_WIDTH = 320f
    const val APP_HEIGHT = 480f

    /** Filename where preferences saves (name.properties) */
    const val PREFERENCES = "blik_cognitive"

    /** Project constants  */
    const val NAV_BUTTON_SIZE = 40f
    const val DEFAULT_BUTTON_SIZE = 50f
    const val DEFAULT_TABLE_PADDING = 20f
    const val FADE_IN_OUT_SPEED = 0.3f

    /**  Observer responses */
    const val CONN_OK = "conn_ok"
    const val CONN_ERR = "conn_error"
    const val CONN_CANCEL = "conn_cancel"
    const val DATABASE_ERR = "db_error"
    const val LOGIN_FAILED = "login_fail"
    const val EMAIL_ALREADY_EXISTS = "email_exists"
    const val EMAIL_NOT_VALID = "email_invalid"
    const val PASSWORD_NOT_VALID = "password_invalid"
    const val PASSWORD_NOT_MATCH = "password_invalid_match"
    const val LOGGED_OUT = "log_out"
    const val USER_NOT_EXISTS = "no_user"
    const val PASSWORD_RESET_OK = "password_reset_ok"
    const val PASSWORD_RESET_FAIL = "password_reset_fail"

    /** Login types */
    const val FACEBOOK_APP_ID="1656557197840107"
    const val GOOGLE="google"
    const val FACEBOOK="facebook"
    const val EMAIL="email"

    /** Is release version */
    const val RELEASE = true
}