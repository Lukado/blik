package lukado.blik.configuration

import com.badlogic.gdx.graphics.Color

object AssetConfig {
    /** Animation textures */
    const val LOAD_DROP = "textures/animations/loading/loading_drop.atlas"
    const val LOAD_REPEAT = "textures/animations/repeat/loading_repeat.atlas"
    const val MAINMENU = "textures/animations/menu/mainmenu.atlas"
    const val ENDING = "textures/animations/ending/game_end.atlas"

    /** Textures  */
    const val GAME_THUMBNAILS = "games/game_thumbnails.atlas"
    const val GAME_GUIDES = "textures/guides/game_guides.atlas"
    const val UNIVERSAL = "universal"
    const val BUTTON = "text_button"
    const val BUTTON_DOWN = "text_button_down"

    /** Skin  */
    const val TEXTURES = "textures/skin_pack.atlas"
    const val SKIN = "textures/skin_pack.json"

    /** Locales */
    const val LOCALES = "locales/blik"

    /** Graphs */
    val PROG_COLOR_A = Color(Color.rgba8888(Color.valueOf("EDEDED55")))
    val PROG_COLOR_B = Color(Color.rgba8888(Color.valueOf("EDEDEDAA")))
}