package lukado.blik.enums

/** Enum for locales
 *  @param languageName Full language name in english
 *  @param languageTag two letter language abbreviation
 */
enum class ELocale(val languageName: String, val languageTag: String) {
    EN("English", "en"),
    CZ("Czech", "cs");
}