package lukado.blik.enums

/** Enum for user sex options
 *  @param localeName language name in locale key
 */
enum class EGenderType(val localeName: String) {
    UNKNOWN("unknown"),
    MALE("gender_male"),
    FEMALE("gender_female")

}