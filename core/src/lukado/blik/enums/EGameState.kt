package lukado.blik.enums

/** Enum for game states */
enum class EGameState {
    RUNNING, STOPPED, READY, END
}