package lukado.blik.enums

import com.badlogic.gdx.graphics.Color
import lukado.blik.screens.games.*
import lukado.blik.screens.games.interfaces.IGameCompanion

/** Enum for game types with games they contain
 * @param color Game type color
 * @param games List of games for game type
 */
enum class EGameType(val color: Color, val games: ArrayList<IGameCompanion>) {
    ATTENTION(Color.valueOf("FF7C65"), arrayListOf(FlyingFrog, TwoCars)),
    MEMORY(Color.valueOf("7B86D8"), arrayListOf(BoxFinder, DangerousPath)),
    LANGUAGE(Color.valueOf("00A359"), arrayListOf(ColorConfusion)),
    VISUAL_SPATIAL(Color.valueOf("D6D83E"), arrayListOf(Mosquito, FollowOwl)),
    EXECUTIVE(Color.valueOf("BF73C8"), arrayListOf(Estimation, Bubbles))
}