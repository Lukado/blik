package lukado.blik.enums

/** Enum for screen implementations */
enum class EScreen {
    MENU, LOGIN, GAMES, SETTINGS, STATS, SETTINGS_LANGUAGE, SETTINGS_USER, EMAIL_LOGIN, EMAIL_CREATE, EMAIL_RECOVERY
}