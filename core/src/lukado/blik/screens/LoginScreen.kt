package lukado.blik.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.utils.Align
import lukado.blik.configuration.AssetConfig
import lukado.blik.Blik
import lukado.blik.configuration.Config
import lukado.blik.configuration.PreferenceConfig
import lukado.blik.enums.EScreen
import lukado.blik.models.UserModel
import java.util.*

/**
 * Screen controlling initial login into application using predefined options. Shows always
 * after loading screen, and automatically log-in user if he was logged-in previously.
 */
class LoginScreen(parent: Blik) : BlikNavigationPanel(parent), Screen, Observer {
    //Class unique
    private val table: Table
    private val labelStyle: Label.LabelStyle

    //Animations
    private var loading: TextureRegion? = null
    private var loadRepeatAnim: Animation<TextureRegion>? = null
    private var stateTime = 0f

    //Table actors
    private var emailConfirmNewButton: TextButton? = null
    private var emailLoginButton: TextButton? = null
    private var emailNewButton: TextButton? = null

    private var noRegistration: TextButton? = null
    private var emailRegistrationButton: TextButton? = null
    private var googleRegistrationButton: TextButton? = null
    private var facebookRegistrationButton: TextButton? = null
    private var back: TextButton? = null
    private var emailPassRecoveryConfirm: TextButton? = null
    private var emailPassRecoveryButton: TextButton? = null

    private var emailTextField: TextField? = null
    private var passwordTextField: TextField? = null
    private var confirmPasswordTextField: TextField? = null

    private var emailLabel: Label? = null
    private var passwordLabel: Label? = null
    private var passwordConfirmLabel: Label? = null
    private var errorLabel: Label? = null

    init {
        parent.firebaseManager.addObserver(this)
        table = Table()

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontSmall")
        ls.fontColor = Color.RED

        labelStyle = Label.LabelStyle()
        labelStyle.font = parent.skin?.getFont("osifontSmall")
        labelStyle.fontColor = Color.BLACK
        labelStyle.background = parent.skin?.getDrawable("text_button")

        errorLabel = Label("", ls)
        errorLabel?.setAlignment(Align.center)
        errorLabel?.setWrap(true)

        emailRegistrationButton = TextButton(parent.locale?.get("email_login"), parent.skin)
        facebookRegistrationButton = TextButton(parent.locale?.get("facebook_login"), parent.skin)
        googleRegistrationButton = TextButton(parent.locale?.get("google_login"), parent.skin)
        noRegistration = TextButton(parent.locale?.format("without_registration"), parent.skin, "hyperlink")
        emailLoginButton = TextButton(parent.locale?.get("login_title"), parent.skin)
        emailConfirmNewButton = TextButton(parent.locale?.get("create_account"), parent.skin)
        emailNewButton = TextButton(parent.locale?.get("create_account_label"), parent.skin, "hyperlink")
        emailPassRecoveryButton = TextButton(parent.locale?.get("pass_reset_label"), parent.skin, "hyperlink")
        emailPassRecoveryConfirm = TextButton(parent.locale?.get("pass_reset"), parent.skin)
        back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
        emailTextField = TextField("", parent.skin)
        passwordTextField = TextField("", parent.skin)
        confirmPasswordTextField = TextField("", parent.skin)
        passwordTextField?.isPasswordMode = true
        confirmPasswordTextField?.isPasswordMode = true
        passwordTextField?.setPasswordCharacter('*')
        confirmPasswordTextField?.setPasswordCharacter('*')

        val loadBackground: TextureAtlas = parent.manager.am.get(AssetConfig.LOAD_REPEAT)
        val background = loadBackground.createSprites()

        loadRepeatAnim = Animation<TextureRegion>(0.05f, background)
        loading = background.first()

        table.background = Image(loading).drawable
    }

    /** Creates default interface for login. Interface changes if user wants to use
     *  non-social solution (Google, Facebook,..) and wants to use custom email to login.
     *  @param emailLayout Required screen layout
     */
    private fun createUi(emailLayout: EScreen) {
        table.clear()
        table.setFillParent(true)

        when (emailLayout) {
            EScreen.EMAIL_CREATE -> {
                emailLabel = Label(parent.locale?.get("new_email") + ":", labelStyle)
                emailLabel?.setAlignment(Align.right)
                passwordLabel = Label(parent.locale?.get("new_password") + ":", labelStyle)
                passwordLabel?.setAlignment(Align.right)
                passwordConfirmLabel = Label(parent.locale?.get("confirm_password") + ":", labelStyle)
                passwordConfirmLabel?.setAlignment(Align.right)
                back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
                back?.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        if (!parent.firebaseManager.isConnecting) {
                            confirmPasswordTextField?.text = ""
                            passwordTextField?.text = ""
                            emailTextField?.text = ""
                            errorLabel?.setText("")
                            createUi(EScreen.EMAIL_LOGIN)
                        }
                    }
                })
                table.row().padBottom(Config.DEFAULT_TABLE_PADDING).padTop(Config.DEFAULT_TABLE_PADDING * 2).right()
                table.add(emailLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                table.add(emailTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                table.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                table.add(passwordLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                table.add(passwordTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                table.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                table.add(passwordConfirmLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                table.add(confirmPasswordTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                table.row().colspan(2).padTop(Config.DEFAULT_TABLE_PADDING)
                table.add(emailConfirmNewButton).expandX().center().width(Value.percentWidth(0.6f, table))
                table.row().pad(Config.DEFAULT_TABLE_PADDING / 2).expand().bottom().padBottom(Config.DEFAULT_TABLE_PADDING).colspan(2)
                table.add(back)
            }
            EScreen.EMAIL_LOGIN -> {
                emailLabel = Label(parent.locale?.get("email") + ":", labelStyle)
                emailLabel?.setAlignment(Align.right)
                passwordLabel = Label(parent.locale?.get("password") + ":", labelStyle)
                passwordLabel?.setAlignment(Align.right)
                back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
                back?.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        if (!parent.firebaseManager.isConnecting) {
                            errorLabel?.setText("")
                            createUi(EScreen.LOGIN)
                        }
                    }
                })

                table.row().padBottom(Config.DEFAULT_TABLE_PADDING).padTop(Config.DEFAULT_TABLE_PADDING * 2).right()
                table.add(emailLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                table.add(emailTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                table.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                table.add(passwordLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                table.add(passwordTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                table.row().colspan(2).padTop(Config.DEFAULT_TABLE_PADDING)
                table.add(emailLoginButton).expandX().center().width(Value.percentWidth(0.6f, table))
                table.row().pad(Config.DEFAULT_TABLE_PADDING).colspan(2)
                table.add(emailPassRecoveryButton)
                table.row().pad(Config.DEFAULT_TABLE_PADDING / 2).expand().bottom().colspan(2)
                table.add(emailNewButton)
                table.row().pad(Config.DEFAULT_TABLE_PADDING / 2).padBottom(Config.DEFAULT_TABLE_PADDING).colspan(2)
                table.add(back)
            }
            EScreen.EMAIL_RECOVERY -> {
                emailLabel = Label(parent.locale?.get("email") + ":", labelStyle)
                emailLabel?.setAlignment(Align.right)
                back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
                back?.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        if (!parent.firebaseManager.isConnecting) {
                            errorLabel?.setText("")
                            createUi(EScreen.EMAIL_LOGIN)
                        }
                    }
                })

                table.row().padBottom(Config.DEFAULT_TABLE_PADDING).padTop(Config.DEFAULT_TABLE_PADDING * 2).right()
                table.add(emailLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                table.add(emailTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                table.row().colspan(2).padTop(Config.DEFAULT_TABLE_PADDING)
                table.add(emailPassRecoveryConfirm).expandX().center().width(Value.percentWidth(0.6f, table))
                table.row().pad(Config.DEFAULT_TABLE_PADDING / 2).expand().bottom().padBottom(Config.DEFAULT_TABLE_PADDING).colspan(2)
                table.add(back)
            }
            else -> {
                table.row().pad(10f)
                table.add(Image(parent.skin?.getDrawable("email"))).size(Config.DEFAULT_BUTTON_SIZE).padTop(50f)
                table.add(emailRegistrationButton).growX().padTop(50f)
                table.row().pad(10f)
                table.add(Image(parent.skin?.getDrawable("facebook"))).size(Config.DEFAULT_BUTTON_SIZE)
                table.add(facebookRegistrationButton).growX()
                table.row().pad(10f)
                table.add(Image(parent.skin?.getDrawable("google"))).size(Config.DEFAULT_BUTTON_SIZE)
                table.add(googleRegistrationButton).growX()

                table.row().padTop(50f).colspan(2)
                table.add(noRegistration)
            }
        }
        table.row().padTop(10f).padBottom(10f).colspan(2)
        table.add(errorLabel)

        parent.stage.addActor(table)
    }

    /**
     * Function that creates listeners and animation for all buttons in this interface.
     */
    private fun buttonsListening() {
        noRegistration?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    if (parent.preferences.getString(PreferenceConfig.USERNAME, null) == null) {
                        parent.preferences.putString(PreferenceConfig.USERNAME, "universal_name")
                        parent.preferences.flush()
                    }
                    parent.changeScreen(EScreen.MENU)
                }
            }
        })

        googleRegistrationButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.googleLogin()
                    table.children.forEach { it.addAction(Actions.fadeOut(Config.FADE_IN_OUT_SPEED)) }
                }
            }
        })

        facebookRegistrationButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.facebookLogin()
                    table.children.forEach { it.addAction(Actions.fadeOut(Config.FADE_IN_OUT_SPEED)) }
                }
            }
        })

        emailRegistrationButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    errorLabel?.setText("")
                    createUi(EScreen.EMAIL_LOGIN)
                }
            }
        })

        emailConfirmNewButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.emailLogin(emailTextField!!.text, passwordTextField!!.text, confirmPasswordTextField?.text)
                    table.children.forEach { it.addAction(Actions.fadeOut(Config.FADE_IN_OUT_SPEED)) }
                    parent.stage.unfocusAll()
                    Gdx.input.setOnscreenKeyboardVisible(false)
                }
            }
        })


        emailLoginButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.emailLogin(emailTextField!!.text, passwordTextField!!.text)
                    table.children.forEach { it.addAction(Actions.fadeOut(Config.FADE_IN_OUT_SPEED)) }
                    parent.stage.unfocusAll()
                    Gdx.input.setOnscreenKeyboardVisible(false)
                }
            }
        })

        emailNewButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    errorLabel?.setText("")
                    createUi(EScreen.EMAIL_CREATE)
                }
            }
        })

        emailPassRecoveryButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    errorLabel?.setText("")
                    createUi(EScreen.EMAIL_RECOVERY)
                }
            }
        })

        emailPassRecoveryConfirm?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    errorLabel?.setText("")
                    parent.firebaseManager.sendLostPassEmail(emailTextField!!.text)
                    parent.stage.unfocusAll()
                    Gdx.input.setOnscreenKeyboardVisible(false)
                }
            }
        })
    }

    /** Resets background animation and other interface animations */
    private fun resetGui() {
        stateTime = 0f
        loading!!.setRegion(loadRepeatAnim!!.getKeyFrame(stateTime, true))
        table.act(Gdx.graphics.deltaTime)
        table.children.forEach { it.addAction(Actions.fadeIn(Config.FADE_IN_OUT_SPEED)) }
    }

    /** This method is inherited. Updates screen based on data send by observer (database connection manager). */
    override fun update(observable: Observable, data: Any?) {
        Gdx.app.postRunnable(Runnable {
            if (parent.screen !is LoginScreen) return@Runnable
            when (data) {
                is UserModel -> {
                    parent.changeScreen(EScreen.MENU)
                    parent.stage.unfocusAll()
                    Gdx.input.setOnscreenKeyboardVisible(false)
                }
                is String -> {
                    errorLabel?.isVisible = true
                    errorLabel?.setText("")
                    errorLabel?.style?.fontColor = Color.RED
                    errorLabel?.clearActions()
                    when (data) {
                        Config.CONN_ERR -> {
                            errorLabel?.setText(parent.locale?.get("connection_error"))
                        }

                        Config.EMAIL_ALREADY_EXISTS -> {
                            errorLabel?.setText(parent.locale?.get("email_already_exists"))
                        }

                        Config.LOGIN_FAILED -> {
                            errorLabel?.setText(parent.locale?.get("login_failed"))
                        }

                        Config.DATABASE_ERR -> {
                            errorLabel?.setText(parent.locale?.get("database_error"))
                        }

                        Config.EMAIL_NOT_VALID -> {
                            errorLabel?.setText(parent.locale?.get("email_invalid"))
                        }

                        Config.PASSWORD_NOT_VALID -> {
                            errorLabel?.setText(parent.locale?.get("password_invalid"))
                        }

                        Config.PASSWORD_NOT_MATCH -> {
                            errorLabel?.setText(parent.locale?.get("password_invalid_match"))
                        }

                        Config.USER_NOT_EXISTS -> {
                            errorLabel?.setText(parent.locale?.get("user_not_exists"))
                        }
                        Config.PASSWORD_RESET_FAIL -> {
                            errorLabel?.setText(parent.locale?.get("pass_reset_fail"))
                        }
                        Config.PASSWORD_RESET_OK -> {
                            createUi(EScreen.EMAIL_LOGIN)
                            errorLabel?.style?.fontColor = Color.GREEN
                            errorLabel?.setText(parent.locale?.get("pass_reset_sent"))
                            errorLabel?.addAction(Actions.sequence(
                                    Actions.delay(3f),
                                    Actions.fadeOut(1f),
                                    Actions.run {
                                        errorLabel?.setText("")
                                        errorLabel?.style?.fontColor = Color.RED
                                        errorLabel?.isVisible = true
                                    }))
                        }
                        else -> {
                            errorLabel?.setText("")
                        }
                    }
                    resetGui()
                }
                data is Boolean -> { }
                else -> {
                    resetGui()
                    Gdx.app.log("Fireapp Login Screen", data.toString())
                }
            }
        })
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.show()
     */
    override fun show() {
        parent.stage.clear()

        createUi(EScreen.LOGIN)
        buttonsListening()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.render()
     */
    override fun render(delta: Float) {
        Gdx.gl.glClearColor(1f, 0.784f, 0.275f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        if (delta < 0.3 && parent.firebaseManager.isConnecting) {
            stateTime += delta
            loading!!.setRegion(loadRepeatAnim!!.getKeyFrame(stateTime, true))
        }

        parent.stage.act(Gdx.graphics.deltaTime)
        parent.stage.draw()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.hide()
     */
    override fun hide() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.pause()
     */
    override fun pause() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.resume()
     */
    override fun resume() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.dispose()
     */
    override fun dispose() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.resize()
     */
    override fun resize(width: Int, height: Int) {
        parent.stage.viewport.update(width, height, true)
    }
}