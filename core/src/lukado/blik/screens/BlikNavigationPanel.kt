package lukado.blik.screens

import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import lukado.blik.Blik
import lukado.blik.configuration.Config
import lukado.blik.configuration.PreferenceConfig
import lukado.blik.enums.EScreen


/**
 * Class that presents obtainable object => navigation panel. This class provides
 * functions that creates navigation panel at the bottom of the device screen.
 */
abstract class BlikNavigationPanel(val parent: Blik) {
    //Table actors
    private var bottomBar: Table = Table()
    private var menu: ImageButton? = null
    private var settings: ImageButton? = null
    private var games: ImageButton? = null
    private var stats: ImageButton? = null
    private var debug: ImageButton? = null

    private var menuLabel: Label? = null
    private var settingsLabel: Label? = null
    private var gamesLabel: Label? = null
    private var statsLabel: Label? = null
    private var debugLabel: Label? = null

    // Sound
    private var sound: Sound

    init {
        games = ImageButton(parent.skin, "games")
        menu = ImageButton(parent.skin)
        settings = ImageButton(parent.skin, "profile")
        stats = ImageButton(parent.skin, "stats")
        debug = ImageButton(parent.skin, "lightbulb")

        sound = parent.manager.createSound("music/menu.mp3")
    }

    /**
     * Fully creates and returns table which represents navigation panel
     */
    fun getNavigationBar(): Table {
        bottomBar.clear()

        val bgPixmap = Pixmap(1, 1, Pixmap.Format.RGB565)
        bgPixmap.setColor(Color.WHITE)
        bgPixmap.fill()
        bottomBar.background = TextureRegionDrawable(TextureRegion(Texture(bgPixmap)))
        bgPixmap.dispose()

        bottomBar.row().pad(10f, 0f, 10f, 0f).expandX().uniformX()
        attachIconToPanel(games!!)
        attachIconToPanel(menu!!)
        attachIconToPanel(settings!!)
        attachIconToPanel(stats!!)

        if (!Config.RELEASE) bottomBar.add(debug).size(Config.NAV_BUTTON_SIZE * 0.8f)

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontMini")
        ls.fontColor = Color.BLACK

        gamesLabel = Label(parent.locale?.get("games_navigation_bar"), ls)
        menuLabel = Label(parent.locale?.get("menu_navigation_bar"), ls)
        settingsLabel = Label(parent.locale?.get("settings_navigation_bar"), ls)
        statsLabel = Label(parent.locale?.get("stats_navigation_bar"), ls)
        debugLabel = Label("Debug", ls)

        bottomBar.row().padTop(-gamesLabel!!.height).uniformX()
        attachIconLabelToPanel(gamesLabel!!)
        attachIconLabelToPanel(menuLabel!!)
        attachIconLabelToPanel(settingsLabel!!)
        attachIconLabelToPanel(statsLabel!!)

        if (!Config.RELEASE) bottomBar.add(debugLabel)

        buttonsListening()
        return bottomBar
    }

    /**
     * Function that creates listeners and animation for all buttons in navigation panel.
     */
    private fun buttonsListening() {
        when (parent.screen) {
            is MenuScreen -> {
                menu?.isChecked = true
                menu?.setScale(0.8f)
                menuLabel?.color?.a = 1f
            }
            is GamesScreen -> {
                games?.isChecked = true
                games?.setScale(0.8f)
                gamesLabel?.color?.a = 1f
            }
            is SettingsScreen -> {
                settings?.isChecked = true
                settings?.setScale(0.8f)
                settingsLabel?.color?.a = 1f
            }
            is StatisticsScreen -> {
                stats?.isChecked = true
                stats?.setScale(0.8f)
                statsLabel?.color?.a = 1f
            }
        }
        if (!Config.RELEASE && parent.stage.isDebugAll) debug?.isChecked = true

        attachListenerToNavIcon(games!!, gamesLabel!!, EScreen.GAMES)
        attachListenerToNavIcon(menu!!, menuLabel!!, EScreen.MENU)
        attachListenerToNavIcon(settings!!, settingsLabel!!, EScreen.SETTINGS)
        attachListenerToNavIcon(stats!!, statsLabel!!, EScreen.STATS)

        if (!Config.RELEASE) {
            debug?.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    super.clicked(event, x, y)
                    parent.stage.isDebugAll = !parent.stage.isDebugAll
                }
            })
        }
    }

    /**
     * Generic method to attach listener to navigation button. Each button does animation with
     * screen change if clicked.
     *
     * @param imgBut image button
     * @param buttonLabel image button label
     * @param screen screen to change after clicking the button
     */
    private fun attachListenerToNavIcon(imgBut: ImageButton, buttonLabel: Label, screen: EScreen) {
        imgBut.clearListeners()
        imgBut.addListener(object : ClickListener() {
            override fun touchDown(event: InputEvent?, x: Float, y: Float, pointer: Int, button: Int): Boolean {
                if (tapCount > 3) {
                    parent.firebaseManager.checkAdminRights()
                }
                if (!imgBut.isChecked) {
                    if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) sound.play()
                    bottomBar.parent.getChild(0).addAction(Actions.sequence(
                            Actions.parallel(
                                    Actions.fadeOut(Config.FADE_IN_OUT_SPEED),
                                    Actions.run {
                                        imgBut.addAction(Actions.scaleTo(0.8f, 0.8f, Config.FADE_IN_OUT_SPEED))
                                        buttonLabel.addAction(Actions.fadeIn(Config.FADE_IN_OUT_SPEED))
                                    }),
                            Actions.run {
                                parent.changeScreen(screen)
                            }))
                }
                imgBut.isChecked = true

                return super.touchDown(event, x, y, pointer, button)
            }
        })
    }

    /**
     * Generic button settings to any navigation icon.
     */
    private fun attachIconToPanel(button: ImageButton) {
        button.clearActions()
        button.isChecked = false
        button.isTransform = true
        button.setScale(1f)
        button.setOrigin(Config.NAV_BUTTON_SIZE / 2f, Config.NAV_BUTTON_SIZE / 2f)
        bottomBar.add(button).size(Config.NAV_BUTTON_SIZE)
    }

    /**
     *  Generic label settings to any navigation label
     */
    private fun attachIconLabelToPanel(label: Label) {
        label.clearActions()
        label.color.a = 0f
        bottomBar.add(label)
    }
}