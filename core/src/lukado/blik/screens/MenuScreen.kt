package lukado.blik.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import lukado.blik.Blik
import lukado.blik.configuration.AssetConfig
import lukado.blik.configuration.Config
import lukado.blik.screens.games.interfaces.IGameCompanion
import java.text.SimpleDateFormat
import java.util.*
import kotlin.reflect.full.primaryConstructor

/**
 * Main screen containing recommended games for today. User can quick start those games right
 * from this screen.
 */
class MenuScreen(parent: Blik) : BlikNavigationPanel(parent), Screen {
    //Class unique
    private val glist: List<IGameCompanion> = parent.gameManager.getDailyGames()
    private val table = Table()
    private val innerTable = Table()

    //Animations
    private var currAnimTexture: TextureRegion? = null
    private var mainmenuAnimation: Animation<TextureRegion>? = null
    private var stateTime = 0f

    init {
        val mainmenuTextures: TextureAtlas = parent.manager.am.get(AssetConfig.MAINMENU)
        val mainmenuSprites = mainmenuTextures.createSprites("menu")

        mainmenuAnimation = Animation(0.033f, mainmenuSprites)
        currAnimTexture = mainmenuSprites.first()
    }

    /** Creates default interface for menu, containing all recommended games for the day received
     *  from the inline factory class.
     */
    private fun createUi() {
        table.clear()
        table.setFillParent(true)
        table.debug = parent.stage.isDebugAll
        table.background = Image(currAnimTexture).drawable

        innerTable.clear()

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontSmall")
        ls.fontColor = Color.BLACK

        val descriptionLabel = Label(parent.locale?.get("menu_description"), ls)
        descriptionLabel.setWrap(true)
        descriptionLabel.setAlignment(Align.center)
        if (!mainmenuAnimation!!.isAnimationFinished(stateTime)) {
            descriptionLabel.color.a = 0f
            descriptionLabel.addAction(Actions.sequence(Actions.delay(2f + 0.3f * (glist.size)), Actions.fadeIn(3f)))
        }

        innerTable.row().width(Config.APP_WIDTH - 80f).colspan(2)
        innerTable.add(descriptionLabel)

        val gtc = GameTemplateFactory(parent, mainmenuAnimation!!, stateTime)
        glist.forEach {
            gtc.createNewGameTemplate(it)
            innerTable.row().left().space(5f)
            innerTable.add(gtc.imageButton).size(Config.DEFAULT_BUTTON_SIZE).padLeft(50f)
            innerTable.add(gtc.label).grow()
        }

        // Table begin
        table.row().grow().align(Align.bottom).padBottom(Value.percentHeight(0.03f, table)).maxHeight(Value.percentHeight(0.65f, table))
        table.add(innerTable)
        table.row()
        table.add(getNavigationBar()).bottom().growX()
        parent.stage.addActor(table)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.show()
     */
    override fun show() {
        parent.stage.clear()
        createUi()
        innerTable.addAction(Actions.sequence(Actions.alpha(0f), Actions.fadeIn(Config.FADE_IN_OUT_SPEED)))
        //Forced act of action.alpha(), otherwise screen stutters before first stage.act()
        innerTable.act(Gdx.graphics.deltaTime)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.render()
     */
    override fun render(delta: Float) {
        Gdx.gl.glClearColor(1f, 0.784f, 0.275f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        if (!mainmenuAnimation!!.isAnimationFinished(stateTime)) {
            stateTime += Gdx.graphics.deltaTime
            currAnimTexture!!.setRegion(mainmenuAnimation!!.getKeyFrame(stateTime, false))
        }

        parent.stage.act(Gdx.graphics.deltaTime)
        parent.stage.draw()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.resize()
     */
    override fun resize(width: Int, height: Int) {
        parent.stage.viewport.update(width, height, true)
    }

    /** This method is inherited. Description is in parent class
     * @see Screen.pause()
     */
    override fun pause() {}

    /** This method is inherited. Description is in parent class
     * @see Screen.resume()
     */
    override fun resume() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.hide()
     */
    override fun hide() {
        if (!mainmenuAnimation!!.isAnimationFinished(stateTime)) {
            stateTime = mainmenuAnimation!!.animationDuration
            currAnimTexture!!.setRegion(mainmenuAnimation!!.getKeyFrame(stateTime, false))
        }
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.dispose()
     */
    override fun dispose() {}

    /**
     * Inline factory class to provide recommended games.
     */
    class GameTemplateFactory(private var parent: Blik, private var anim: Animation<TextureRegion>, private var time: Float) {
        var imageButton: ImageButton
        var label: Label
        private var inc: Float = 1.5f

        init {
            imageButton = ImageButton(parent.skin)
            label = Label("first", parent.skin)
        }

        /**
         * Defines class properties for a game. Sets launching image and game name label.
         * Sets animations and listeners for those actors.
         * @param gameInfo game information interface
         */
        fun createNewGameTemplate(gameInfo: IGameCompanion) {
            inc -= 0.3f // Depends on the background animation speed

            imageButton = if (hasAlreadyPlayedToday(gameInfo.weekScoresPreference)) {
                ImageButton(parent.skin, "play_done")
            }else{
                ImageButton(parent.skin, "play")
            }
            label = Label(parent.locale!!.get(gameInfo.localeName), parent.skin)

            val listener = object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    parent.screen = gameInfo.getClassInstance().primaryConstructor?.call(parent, false) as Screen?
                    super.clicked(event, x, y)
                }
            }

            imageButton.addListener(listener)
            label.addListener(listener)

            if (!anim.isAnimationFinished(time)) {
                imageButton.color.a = 0f
                imageButton.addAction(Actions.sequence(Actions.touchable(Touchable.disabled), Actions.delay(inc + 2f), Actions.fadeIn(3f), Actions.touchable(Touchable.enabled)))

                label.color.a = 0f
                label.addAction(Actions.sequence(Actions.touchable(Touchable.disabled), Actions.delay(inc + 2f), Actions.fadeIn(3f), Actions.touchable(Touchable.enabled)))
            }
        }

        private fun hasAlreadyPlayedToday(preferenceName: String): Boolean {
            return parent.preferences.getString(preferenceName, "").contains(SimpleDateFormat("yy,d,M", Locale.ENGLISH).format(Calendar.getInstance().time))
        }
    }
}