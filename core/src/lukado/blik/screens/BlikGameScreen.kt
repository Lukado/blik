package lukado.blik.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import com.badlogic.gdx.utils.Align
import lukado.blik.Blik
import lukado.blik.configuration.AssetConfig
import lukado.blik.configuration.Config
import lukado.blik.configuration.GameConfig
import lukado.blik.configuration.PreferenceConfig
import lukado.blik.enums.EGameState
import lukado.blik.enums.EScreen
import lukado.blik.models.ScoreModel
import lukado.blik.screens.games.*
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.interfaces.IGameInfo
import lukado.blik.tools.BodyEditorLoader
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.ceil
import kotlin.math.min
import kotlin.math.roundToInt

/**
 * Abstract class for every game. Provides interface with description, difficulty information
 * and online statistics. Also manages general game rendering, preparing and ending and
 * input processing.
 */
@Suppress("UNCHECKED_CAST")
abstract class BlikGameScreen(private val parent: Blik, private val description: Boolean, private val gameInfo: IGameCompanion) : Screen, IGameInfo, InputProcessor, ContactListener, Observer {
    //Class unique
    private val multiplexer: InputMultiplexer = InputMultiplexer()
    private var gameTime = 0f
    private var chartOffsetHeight: Float = 0f

    private var accumulator: Float = 0f
    private var userTopTen: List<Int>
    private var userTopWeek: Map<String, Pair<String, Int>>

    private val universalWindowHeight: Float
    private var isChartDrawable: Boolean

    //Inheritable variables
    protected val thumbnails: TextureAtlas = parent.manager.am.get(AssetConfig.GAME_THUMBNAILS)
    protected val guides: TextureAtlas = parent.manager.am.get(AssetConfig.GAME_GUIDES)
    protected var bel: BodyEditorLoader? = null
    protected var state: EGameState = EGameState.STOPPED
    protected var camera: Camera

    protected var difficulty: Int
        private set
    protected var score: Int = 0
    protected var customGame = false
    protected var customTime = 0f
    protected var currentGameDuration = 0f

    //Scene objects
    private val timeBG = ButtonGroup<TextButton>()
    private val diffBG = ButtonGroup<TextButton>()
    private var pauseButton: ImageButton? = null
    private var customWarningLabel: Label? = null

    protected var infoLabel: Label? = null
    protected var infoContainer: Container<Label>? = null

    private var scoreLabel: Label? = null
    protected var topRightLabel: Label? = null
    protected var debugLabel: Label? = null

    private val table: Table = Table()
    private val pauseTable: Table = Table()

    private var chartBuffer: FrameBuffer
    private val chartTable: Table = Table()
    private var scrollPane: ScrollPane? = null

    //Game (Box2D) objects
    var world: World
    val sb: SpriteBatch
    private val debugRenderer = Box2DDebugRenderer(true, false, false, true, true, false)

    //Animations
    private var currAnimTexture: TextureRegion? = null
    private var endingAnimation: Animation<TextureRegion>? = null
    private var stateTime = 0f

    //Sounds
    private var buttonSound: Sound

    init {
        multiplexer.addProcessor(parent.stage)
        multiplexer.addProcessor(this)
        Gdx.input.inputProcessor = multiplexer
        Gdx.input.setCatchKey(Input.Keys.BACK, true)

        difficulty = parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0])
        userTopTen = parent.gameManager.getTopScorePreferences(gameInfo)
        userTopWeek = parent.gameManager.getWeekScorePreferences(gameInfo)

        camera = parent.stage.camera
        world = World(Vector2(0f, 0f), true)
        world.setContactListener(this)
        sb = SpriteBatch()
        sb.projectionMatrix = camera.combined
        chartBuffer = FrameBuffer(Pixmap.Format.RGBA8888, camera.viewportWidth.roundToInt(), camera.viewportHeight.roundToInt(), false)
        universalWindowHeight = camera.viewportHeight * 0.2f

        isChartDrawable = false
        buttonSound = parent.manager.createSound("music/click.mp3")
    }

    /** Main function to create default game interface. This contains game image, description,
     *  graph statistics, user current difficulty and top achieved game scores.
     */
    private fun createDescriptionUi() {
        var lss = Label.LabelStyle()
        lss.font = parent.skin?.getFont("osifontSmall")
        lss.fontColor = Color.BLACK

        val descriptionLabel = Label(gameDescription, lss)
        descriptionLabel.setWrap(true)
        descriptionLabel.setAlignment(Align.center)

        var lsm = Label.LabelStyle()
        lsm.font = parent.skin?.getFont("osifontMedium")
        lsm.fontColor = Color.BLACK

        chartOffsetHeight = 0f

        val playButton = TextButton(parent.locale?.get("play"), parent.skin)
        playButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                createGameStartScreen()
            }
        })

        val backButton = ImageButton(parent.skin, "previous")
        backButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                keyDown(Input.Keys.BACK)
            }
        })

        val helpButton = ImageButton(parent.skin, "help")
        helpButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                showGuideScreen(true)
            }
        })

        val difficultyButton = ImageButton(parent.skin, "time")
        difficultyButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                createCustomGameUi()
            }
        })

        // Chart preparation
        checkChartDrawing()
        if (isChartDrawable) {
            camera = parent.stage.camera
            sb.projectionMatrix = camera.combined

            val sr = ShapeRenderer()
            sr.projectionMatrix = camera.combined
            sr.color = Color.BLACK

            val pm = Pixmap(1, 1, Pixmap.Format.RGBA8888)
            pm.setColor(Color.WHITE)
            pm.fill()

            chartBuffer.begin()
            Gdx.gl.glClearColor(gameType.color.r, gameType.color.g, gameType.color.b, 0f)
//            Gdx.gl.glClearColor(1f, 1f, 1f, 0.5f)
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

            val axisOffset = ((camera.viewportWidth - Config.DEFAULT_TABLE_PADDING * 2f - Config.DEFAULT_TABLE_PADDING / 6f) / (userTopWeek.size))
            var i = 0
            var xPos = 0f
            val prevVect = Vector2()
            var prevKey = ""
            sr.begin(ShapeRenderer.ShapeType.Line)
            userTopWeek.forEach {it->
                i++
                if (xPos != 0f) {
                    val percentage = userTopWeek[prevKey]!!.second / userTopWeek.values.map { it.second }.max()!!.toFloat()
                    prevVect.set(xPos, universalWindowHeight * 0.02f + (universalWindowHeight * 0.73f) * (1 - percentage))
                }
                xPos = if (i > 1) axisOffset / 2f + (i - 1) * axisOffset + Config.DEFAULT_TABLE_PADDING + (i - 1) * (Config.DEFAULT_TABLE_PADDING / 3f / (userTopWeek.size - 1))
                else axisOffset / 2f + Config.DEFAULT_TABLE_PADDING
                sr.line(Vector2(xPos, universalWindowHeight * 0.02f), Vector2(xPos, universalWindowHeight * 0.75f))

                if (prevVect.x != 0f) {
                    val percentage = it.value.second / userTopWeek.values.map { it.second }.max()!!.toFloat()
                    drawDashedLine(sr, prevVect, Vector2(xPos, universalWindowHeight * 0.02f + (universalWindowHeight * 0.73f) * (1 - percentage)), 10, 3f / (3f + userTopWeek.size.toFloat() / 2f))
                }
                prevKey = it.key
            }
            sr.end()


            i = 0
            sr.color = Color.WHITE
            sr.begin(ShapeRenderer.ShapeType.Filled)
            userTopWeek.forEach { it ->
                i++
                xPos = if (i > 1) axisOffset / 2f + (i - 1) * axisOffset + Config.DEFAULT_TABLE_PADDING + (i - 1) * (Config.DEFAULT_TABLE_PADDING / 3f / (userTopWeek.size - 1))
                else axisOffset / 2f + Config.DEFAULT_TABLE_PADDING

                val percentage = it.value.second / userTopWeek.values.map { it.second }.max()!!.toFloat()
                sr.circle(xPos, universalWindowHeight * 0.02f + (universalWindowHeight * 0.73f) * (1 - percentage), universalWindowHeight * 0.02f)
            }
            sr.end()
            sr.dispose()
            chartBuffer.end()
        }


        // scrollTable preparation
        parent.stage.clear()
        table.clear()
        table.setFillParent(true)
        val spTable = Table()

        //Image and description
        spTable.add(gameThumbnail).size(Config.APP_WIDTH)

        spTable.row().expandX().width(Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 2).padTop(Config.DEFAULT_TABLE_PADDING / 2)
        spTable.add(descriptionLabel)

        spTable.row().growX().padTop(Config.DEFAULT_TABLE_PADDING)
        spTable.add(Image(parent.skin?.getRegion("textfield")))

        //Difficulty
        spTable.row().expandX().pad(Config.DEFAULT_TABLE_PADDING)
        spTable.add(Label(parent.locale?.get("current_difficulty"), lsm))
        spTable.row().expandX().pad(0f, Config.DEFAULT_TABLE_PADDING, 0f, Config.DEFAULT_TABLE_PADDING)

        val diffTable = Table()
        diffTable.row().uniformX().growX().space(Config.DEFAULT_TABLE_PADDING / 3)
        GameConfig.GAME_DIFFICULTIES.forEach {
            val bgPixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
            if (difficulty == it) bgPixmap.setColor(0f, 0f, 0f, 1f)
            else bgPixmap.setColor(1f, 1f, 1f, 1f)
            bgPixmap.fill()

            lss = Label.LabelStyle()
            lss.font = parent.skin?.getFont("osifontSmall")
            if (difficulty == it) lss.fontColor = Color.WHITE
            else lss.fontColor = Color.BLACK
            lss.background = Image(Texture(bgPixmap)).drawable
            bgPixmap.dispose()

            val label = if (difficulty == it) Label("$it", lss)
            else Label("", lss)
            label.setAlignment(Align.bottom)
            diffTable.add(label).bottom().height(Value.percentHeight(it / GameConfig.GAME_DIFFICULTIES.last().toFloat(), diffTable))
        }
        spTable.add(diffTable).growX().height(universalWindowHeight)

        //Graph chart
        if (isChartDrawable) {
            spTable.row().growX().padTop(Config.DEFAULT_TABLE_PADDING)
            spTable.add(Image(parent.skin?.getRegion("textfield")))

            spTable.row().expandX().padTop(Config.DEFAULT_TABLE_PADDING)
            spTable.add(Label(parent.locale?.get("your_progress"), lsm))
            spTable.row().pad(Config.DEFAULT_TABLE_PADDING / 2, Config.DEFAULT_TABLE_PADDING, 0f, Config.DEFAULT_TABLE_PADDING)

            chartTable.clear()
            chartTable.row().uniformX().growX().space(Config.DEFAULT_TABLE_PADDING / 3)

            lss = Label.LabelStyle()
            lss.font = parent.skin?.getFont("osifontSmall")
            lss.fontColor = Color.BLACK
            userTopWeek.forEach {
                val label = Label(it.value.first, lss)
                label.setAlignment(Align.bottom)
                chartTable.add(label).expand().bottom()
            }
            spTable.add(chartTable).growX().height(universalWindowHeight)
        }

        if(!(userTopTen.size==1 && userTopTen.contains(0))) {
            spTable.row().growX().padTop(Config.DEFAULT_TABLE_PADDING)
            spTable.add(Image(parent.skin?.getRegion("textfield")))

            //User score
            spTable.row().expandX().pad(Config.DEFAULT_TABLE_PADDING / 2)
            spTable.add(Label(parent.locale?.get("your_top_10"), lsm)).center()
            spTable.row().padTop(Config.DEFAULT_TABLE_PADDING / 2)

            val innerTable = Table()
            userTopTen.forEachIndexed { i, score ->
                if (score == 0) return@forEachIndexed
                val bgPixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
                if ((i + 1) % 2 == 0) bgPixmap.setColor(1f, 1f, 1f, 0.1f)
                else bgPixmap.setColor(1f, 1f, 1f, 0.2f)
                bgPixmap.fill()

                lsm = Label.LabelStyle()
                lsm.font = parent.skin?.getFont("osifontMedium")
                lsm.fontColor = Color.BLACK
                lsm.background = Image(Texture(bgPixmap)).drawable
                bgPixmap.dispose()

                val label = Label("$score", lsm)
                label.setAlignment(Align.center)
                innerTable.add(label).growX()

                innerTable.row()
            }
            spTable.add(innerTable).grow()
            spTable.row()
        }

        scrollPane = ScrollPane(spTable)
        scrollPane?.setSmoothScrolling(true)
        scrollPane?.setScrollingDisabled(true, false)

        if (isChartDrawable) {
            spTable.validate()
            kotlin.run {
                spTable.children?.forEachIndexed { i, it ->
                    chartOffsetHeight += if (it is Label) it.prefHeight
                    else it.height
                    if (i == 6) return@run
                }
            }
            chartOffsetHeight += Config.DEFAULT_TABLE_PADDING * 7
        }

        table.add(scrollPane).colspan(4).grow()

        table.row().bottom().pad(Config.DEFAULT_TABLE_PADDING / 4f)
        table.add(backButton).size(playButton.height, playButton.height)
        table.add(helpButton).size(playButton.height, playButton.height)
        table.add(difficultyButton).size(playButton.height, playButton.height)
        table.add(playButton).growX()
        parent.stage.addActor(table)
    }

    /**
     * Creates interface for choosing custom game settings. This includes choosing from any already
     * achieved difficulty and custom timer.
     */
    private fun createCustomGameUi() {
        customGame = true

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontSmall")
        ls.fontColor = Color.BLACK

        val lsWarn = Label.LabelStyle()
        lsWarn.font = parent.skin?.getFont("osifontSmall")
        lsWarn.fontColor = Color.RED

        val harderLabel = Label(parent.locale?.get("custom_game_difficulty_info"), ls)
        harderLabel.setWrap(true)
        harderLabel.setAlignment(Align.center)

        val difficultyLabel = Label(parent.locale?.get("custom_game_difficulty"), parent.skin)
        difficultyLabel.setAlignment(Align.bottom)

        customWarningLabel = Label(parent.locale?.get("custom_game_warning"), lsWarn)
        customWarningLabel?.setWrap(true)
        customWarningLabel?.setAlignment(Align.center)

        val playButton = TextButton(parent.locale?.get("play"), parent.skin)
        playButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                createGameStartScreen()
            }
        })

        val backButton = TextButton(parent.locale?.get("back"), parent.skin)
        backButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                keyDown(Input.Keys.BACK)
                customGame = false
            }
        })

        diffBG.clear()
        timeBG.clear()

        //Table begin
        parent.stage.clear()
        table.clear()
        table.setFillParent(true)

        table.add(difficultyLabel).expandX().colspan(2).bottom().growY()
        table.row()
        table.add(harderLabel).width(Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 2).expandX().colspan(2)
        table.row()
        var innerTable = Table()
        GameConfig.GAME_DIFFICULTIES.forEach {
            if (diffBG.buttons.size % 5 == 0) innerTable.row().padTop(5f)
            val tf = TextButton(it.toString(), parent.skin, "checkgroup")
            if (it > difficulty) {
                tf.isDisabled = true
                tf.color = Color.LIGHT_GRAY
            }
            diffBG.add(tf)
            innerTable.add(tf).expandX()
        }
        diffBG.setChecked(difficulty.toString())
        table.add(innerTable).colspan(2).growX()
        table.row()

        innerTable = Table()
        table.add(Label(parent.locale?.get("custom_game_timer"), parent.skin)).expandX().colspan(2).padTop(20f)
        table.row()
        GameConfig.GAME_CUSTOM_TIMERS.forEach {
            if (timeBG.buttons.size % 5 == 0) innerTable.row().padTop(5f)
            val tf = TextButton(String.format("%d:%02d", it / 60, it % 60), parent.skin, "checkgroup")
            tf.color = Color(0.9f, 0.9f, 0.9f, 0.8f)
            timeBG.add(tf)
            if (it / 60f == 1f) {
                tf.color = Color.WHITE
                timeBG.setChecked(String.format("%d:%02d", it / 60, it % 60))
            }
            innerTable.add(tf).expandX()
        }
        table.add(innerTable).colspan(2).growX()

        table.row().expandX().colspan(2).growY().width(Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 2)
        table.add(customWarningLabel)

        table.row().bottom().growX().pad(Config.DEFAULT_TABLE_PADDING / 2f)
        table.add(backButton)
        table.add(playButton)
        parent.stage.addActor(table)

        //Warning label showing
        customWarningLabel?.isVisible = false
    }

    /**
     * Creates starting screen interface for any game. It is composed of top info panel
     * (pause, score and timer), pause interface and response container. Also prepares default
     * properties settings for any game.
     */
    private fun createGameStartScreen() {
        // If game not done yet
        if (camera.viewportWidth == 0f) {
            loadDescriptionUi()
            return
        } else if (parent.preferences.getBoolean(gameInfo.isFirstTimePreference, true)){
            showGuideScreen(false)
            return
        }

        //World reset
        Gdx.input.inputProcessor = multiplexer

        world = World(Vector2(0f, 0f), true)
        world.setContactListener(this)

        score = 0
        difficulty = parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0])

        if (customGame) {
            difficulty = Integer.parseInt(diffBG.checked.text.toString())
            customTime = GameConfig.GAME_CUSTOM_TIMERS[timeBG.checkedIndex].toFloat()
        }

        //Change game state
        state = EGameState.READY

        // Prepare UI
        infoLabel = Label(parent.locale?.get("game_start"), parent.skin)
        scoreLabel = Label("$score", parent.skin)
        infoLabel?.setOrigin(Align.center)
        infoLabel?.setAlignment(Align.center)
        infoContainer = Container(infoLabel!!)
        infoContainer?.isTransform = true
        infoContainer?.setOrigin(Align.center)
        debugLabel = Label("D=$difficulty : S=$score", parent.skin)
        topRightLabel?.setText("")
        gameTime = 0f
        currentGameDuration = 0f

        pauseButton = ImageButton(parent.skin, "pause")
        pauseButton?.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                keyDown(Input.Keys.BACK)
            }
        })

        val ls = Label.LabelStyle(parent.skin?.getFont("osifontMedium"), Color.BLACK)

        val continueButton = ImageButton(parent.skin, "continue")
        continueButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                pauseTable.isVisible = false
                pauseButton?.touchable = Touchable.enabled
                state = EGameState.RUNNING
                sb.setColor(1f, 1f, 1f, 1f)
            }
        })

        val restartButton = ImageButton(parent.skin, "restart")
        restartButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                createGameStartScreen()
            }
        })

        val homeButton = ImageButton(parent.skin, "home")
        homeButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                keyDown(Input.Keys.BACK)
            }
        })

        val bg = Pixmap(1, 1, Pixmap.Format.RGBA8888)
        bg.setColor(gameType.color.r, gameType.color.g, gameType.color.b, 0.9f)
        bg.fill()

        // Create game UI
        parent.stage.clear()
        table.clear()
        table.setFillParent(true)
        table.background = null
        createGame()
        sb.projectionMatrix = camera.combined

        table.add(pauseButton).left().padLeft(5f).padTop(5f).size(infoLabel!!.height)
        table.add(scoreLabel).left().expandX().padLeft(5f).padTop(5f)
        if (topRightLabel != null) table.add(topRightLabel).right().expandX().padRight(5f).padTop(5f).spaceRight(5f)
        table.row()
        table.add(infoContainer).expand().center().colspan(3).padBottom(infoLabel!!.height).align(infoLabel!!.labelAlign)

        if (parent.stage.isDebugAll) {
            table.row().colspan(3).center()
            table.add(debugLabel)
        }

        // Create pause UI
        pauseTable.clear()
        pauseTable.setFillParent(true)
        pauseTable.isVisible = false
        pauseTable.background = TextureRegionDrawable(Texture(bg))

        pauseTable.add(continueButton).padTop(Config.DEFAULT_TABLE_PADDING).size(Value.percentWidth(0.3f, pauseTable))
        pauseTable.row()
        pauseTable.add(Label(parent.locale?.get("game_continue"), ls))
        pauseTable.row()
        pauseTable.add(restartButton).padTop(Config.DEFAULT_TABLE_PADDING).size(Value.percentWidth(0.3f, pauseTable))
        pauseTable.row()
        pauseTable.add(Label(parent.locale?.get("game_reset"), ls))
        pauseTable.row()
        pauseTable.add(homeButton).padTop(Config.DEFAULT_TABLE_PADDING).size(Value.percentWidth(0.3f, pauseTable))
        pauseTable.row()
        pauseTable.add(Label(parent.locale?.get("game_home"), ls))

        parent.stage.addActor(table)
        parent.stage.addActor(pauseTable)
    }

    /**
     * Shows game instructions about the game if played for the first time or if
     * clicked help button.
     */
    private fun showGuideScreen(isHelp: Boolean) {
        parent.stage.clear()
        table.clear()
        table.setFillParent(true)
        var index = 0
        val descrLabel = Label("", parent.skin, "osifontMedium", Color.BLACK)
        descrLabel.wrap = true
        descrLabel.setAlignment(Align.center)

        val range = when(gameInfo.localeName){
            FlyingFrog.localeName -> Pair(20, 26)
            TwoCars.localeName -> Pair(0, 3)
            Bubbles.localeName -> Pair(45, 50)
            BoxFinder.localeName -> Pair(51, 55)
            ColorConfusion.localeName -> Pair(40, 44)
            FollowOwl.localeName -> Pair(4, 9)
            Estimation.localeName -> Pair(27, 33)
            Mosquito.localeName -> Pair(10, 19)
            DangerousPath.localeName -> Pair(34, 39)
            else -> Pair(0,0)
        }

        val guideImages: ArrayList<TextureAtlas.AtlasRegion> = ArrayList()
        val guideTexts: ArrayList<String> = ArrayList()
        for (i in range.first..range.second){
            guideImages.add(guides.findRegion("guide", i))
            guideTexts.add(parent.locale?.get("${gameInfo.localeName}_instructions_${i-range.first}") ?: "No text found")
        }
        table.background = TextureRegionDrawable(guideImages[index])
        descrLabel.setText(guideTexts[index])

        val nextButton = ImageButton(parent.skin, "continue")
        nextButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                index++
                if (index>guideImages.size-1) {
                    parent.preferences.putBoolean(gameInfo.isFirstTimePreference, false)
                    parent.preferences.flush()
                    if(isHelp) keyDown(Input.Keys.BACK) else createGameStartScreen()
                }
                else {
                    descrLabel.setText(guideTexts[index])
                    table.background = TextureRegionDrawable(guideImages[index])
                }
            }
        })

        val prevButton = ImageButton(parent.skin, "previous")
        prevButton.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                index--
                if (index<0) keyDown(Input.Keys.BACK)
                else {
                    descrLabel.setText(guideTexts[index])
                    table.background = TextureRegionDrawable(guideImages[index])
                }
            }
        })
        table.add(Label("", parent.skin)).expand().colspan(2).row()
        table.add(prevButton).expandX().left().padLeft(Config.DEFAULT_TABLE_PADDING/4f).size(Config.DEFAULT_BUTTON_SIZE)
        table.add(nextButton).expandX().right().padRight(Config.DEFAULT_TABLE_PADDING/4f).size(Config.DEFAULT_BUTTON_SIZE)
        table.row()
        table.add(descrLabel).size(camera.viewportWidth-Config.DEFAULT_TABLE_PADDING, camera.viewportHeight*0.22f).colspan(2).pad(Config.DEFAULT_TABLE_PADDING/2f, Config.DEFAULT_TABLE_PADDING/2f, 0f, Config.DEFAULT_TABLE_PADDING/2f)
        parent.stage.addActor(table)
    }

    /**
     * Creates end game animation and shows finished game results.
     */
    private fun createGameEndScreen() {
        // Change game state
        state = EGameState.END
        sb.setColor(0.5f, 0.5f, 0.5f, 1f)

        // Update game preferences
        if (!customGame || (customGame && gameDuration == currentGameDuration)) {
            var isScoreUpdated = false
            if (userTopWeek[ScoreModel.mapFormat]?.first!=ScoreModel.mapDayFormat || score > userTopWeek[ScoreModel.mapFormat]?.second ?: 0) {
                parent.gameManager.saveScorePreference(gameInfo, score)
                userTopTen = parent.gameManager.getTopScorePreferences(gameInfo)
                userTopWeek = parent.gameManager.getWeekScorePreferences(gameInfo)
                isScoreUpdated = true
            }
            if (!customGame && difficulty > parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0])) parent.preferences.putInteger(gameInfo.currentDifficultyPreference, difficulty)
            parent.preferences.putInteger(gameInfo.timesPlayedPreference, parent.preferences.getInteger(gameInfo.timesPlayedPreference) + 1)
            parent.preferences.flush()

            // If online upload it to database
            parent.firebaseManager.uploadGameData(gameInfo, isScoreUpdated)
        }
        customGame = false

        // Create end screen
        parent.stage.clear()
        table.clear()

        val endScoreLabel = Label(parent.locale?.format("game_score", score), parent.skin)
        val returnLabel = Label(parent.locale?.get("game_end"), parent.skin)
        endScoreLabel.isVisible = false
        returnLabel.isVisible = false

        returnLabel.setFontScale(0.45f)

        val bg = Pixmap(1, 1, Pixmap.Format.RGBA8888)
        bg.setColor(gameType.color)
        bg.fill()

        val endingTextures: TextureAtlas = parent.manager.am.get(AssetConfig.ENDING)
        val endingSprites = endingTextures.createSprites("ending")

        endingAnimation = Animation(0.033f, endingSprites)
        currAnimTexture = endingSprites.first()

        val bgTable = Table()
        bgTable.setFillParent(true)
        bgTable.background = Image(currAnimTexture).drawable

        table.background = TextureRegionDrawable(Texture(bg))
        table.touchable = Touchable.disabled

        table.add(endScoreLabel)
        table.row()
        table.add(returnLabel)
        table.addAction(Actions.sequence(Actions.alpha(0f), Actions.fadeIn(0.3f), Actions.delay(endingAnimation!!.animationDuration - 0.5f), Actions.run {
            endScoreLabel.isVisible = true
            returnLabel.isVisible = true
        }, Actions.touchable(Touchable.enabled)))
        bgTable.addAction(Actions.sequence(Actions.delay(0.3f), Actions.alpha(0f), Actions.fadeIn(0.2f)))

        parent.stage.addActor(table)
        parent.stage.addActor(bgTable)
    }

    /** Method to draw dashed line using ShapeRenderer class
     * @param sr ShapeRenderer class that already called ShapeRenderer.begin()
     * @param p1 starting point vector
     * @param p2 ending point vector
     * @param numDashes number of dashes between points
     * @param dashSize size of dashes
     */
    private fun drawDashedLine(sr: ShapeRenderer, p1: Vector2, p2: Vector2, numDashes: Int, dashSize: Float) {
//        for (i in 0 until numDashes) {
//            val start = i.toFloat() / numDashes.toFloat()
//            val end = (i + dashSize) / numDashes.toFloat()
//            sr.line(p1.x + (p2.x - p1.x) * start, p1.y + (p2.y - p1.y) * start,
//                    p1.x + (p2.x - p1.x) * end, p1.y + (p2.y - p1.y) * end)
//        }
        val max = universalWindowHeight * 0.02f + (universalWindowHeight * 0.73f)
        val colStart = Color(1f * (p1.y / max), 1f * (1 - p1.y / max), 0f, 1f)
        val colEnd = Color(1f * (p2.y / max), 1f * (1 - p2.y / max), 0f, 1f)
        sr.line(p1.x, p1.y, p2.x, p2.y, colStart, colEnd)
    }

    /**
     * Tries to load data from database before creating description interface.
     */
    private fun loadDescriptionUi() {
//        parent.firebaseManager.addObserver(this)
//        parent.firebaseManager.loadScoreModel(gameInfo)
        createDescriptionUi()
    }


    /** Draws top info panel including pause button, score and time */
    private fun redrawGameInfo() {
        if (gameTime < currentGameDuration && gameTime != 0f) topRightLabel?.setText("%.1f".format(gameTime))
        else if (!topRightLabel!!.textEquals("%.1f".format(currentGameDuration)) && gameTime == 0f && state != EGameState.RUNNING) topRightLabel?.setText("%.1f".format(currentGameDuration))
        scoreLabel?.setText(score)
    }

    /** Abstract method to initialize new game and reset all properties to default value */
    abstract fun createGame()

    /** Abstract method to do logic step for game. Computes everything in game every time render loops */
    abstract fun doLogicStep(delta: Float)

    /** Abstract method to redraw all sprites and textures for game*/
    abstract fun redrawSprites()

    /** Abstract method that checks if game met the conditions to end */
    abstract fun isGameEnd(): Boolean

    /** Abstract method that disposes current game data */
    abstract fun gameDispose()

    /** Method for games, that needs to change logic even when game is paused */
    protected open fun pausedGameStep() {}

    /** Returns static path to the game atlas. All atlas files are neccessary to be in this strict folder structure. */
    protected fun getGameAssetPath(gameName: String): String {
        return "games/$gameName/$gameName.atlas"
    }

    /** Method to rise difficulty with input handling (for emulators) */
    protected fun difficultyUp() {
        if (!customGame || (customGame && gameDuration == customTime)) difficulty += if (difficulty < GameConfig.GAME_DIFFICULTIES.size) 1 else 0
    }

    /** Method to lower difficulty with input handling (for emulators) */
    protected fun difficultyDown() {
        if (!customGame || (customGame && gameDuration == customTime)) difficulty -= if (difficulty > GameConfig.GAME_DIFFICULTIES[0]) 1 else 0
    }

    /** Logic and behind "get ready" countdown before game starts */
    protected fun gameReadyCountdown(currentTime: Float, fullGameTime: Float): Float {
        gameTime = currentTime
        currentGameDuration = fullGameTime
        if (currentTime > fullGameTime && !infoLabel!!.textEquals("${ceil(currentTime - fullGameTime).toInt()}")) {
            if (currentTime > fullGameTime && infoLabel!!.textEquals("")) {
                gameTime = fullGameTime + GameConfig.COUNTDOWN_TIME
            }
            infoLabel?.setText(ceil(gameTime - fullGameTime).toInt())
            infoContainer?.clearActions()
            if (ceil(gameTime - fullGameTime).toInt() == 1) {
                infoContainer?.addAction(Actions.sequence(Actions.scaleTo(3f, 3f), Actions.scaleTo(1f, 1f, 1f), Actions.run { infoLabel?.setText("") }))
            } else {
                infoContainer?.addAction(Actions.sequence(Actions.scaleTo(3f, 3f), Actions.scaleTo(1f, 1f, 1f)))
            }
        } else if (currentTime < fullGameTime && infoLabel!!.textEquals("1")) {
            infoContainer?.clearActions()
            infoLabel?.setText("")
        }

        return gameTime
    }

    /** Stepping the world physics simulation */
    private fun doPhysicStep(delta: Float) {
        accumulator += min(delta, 0.25f)
        while (accumulator >= GameConfig.STEP_TIME) {
            world.step(GameConfig.STEP_TIME, 6, 2)
            accumulator -= GameConfig.STEP_TIME
        }
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen#show()
     */
    override fun show() {
        parent.stage.clear()

        state = EGameState.STOPPED
        difficulty = parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0])

        if (Gdx.files.internal("games/${gameInfo.localeName}/physics.json").exists()) bel = BodyEditorLoader(Gdx.files.internal("games/${gameInfo.localeName}/physics.json"))

        if (description) {
            loadDescriptionUi()
        } else {
            createGameStartScreen()
        }
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen#render()
     */
    override fun render(delta: Float) {
        Gdx.gl.glClearColor(gameType.color.r, gameType.color.g, gameType.color.b, gameType.color.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        // Main game loop render
        if (state != EGameState.STOPPED) {
            if (state == EGameState.RUNNING) {
                // Game logic
                doLogicStep(delta)

                // Set game end state
                if (isGameEnd()) {
                    createGameEndScreen()
                }
            }

            // Textures/sprites rendering part
            sb.begin()
            sb.enableBlending()
            redrawSprites()
            redrawGameInfo()
            sb.disableBlending()
            sb.end()

            // For games that need refresh variables in paused state
            if (state == EGameState.READY) pausedGameStep()
            if (state == EGameState.END && !endingAnimation!!.isAnimationFinished(stateTime)) {
                stateTime += Gdx.graphics.deltaTime
                currAnimTexture!!.setRegion(endingAnimation!!.getKeyFrame(stateTime, false))
            }
        } else if (customGame) {
            customWarningLabel?.isVisible = gameDuration != GameConfig.GAME_CUSTOM_TIMERS[timeBG.checkedIndex].toFloat()
        } else {
            if (isChartDrawable) {
                sb.begin()
                sb.draw(chartBuffer.colorBufferTexture, 0f, -chartOffsetHeight + scrollPane!!.scrollY)
                sb.end()
            }
        }

        if (state != EGameState.READY) parent.stage.act(min(Gdx.graphics.deltaTime, GameConfig.STEP_TIME))
        parent.stage.draw()

        // Update the world physics
        if (state == EGameState.RUNNING) {
            if (parent.stage.isDebugAll) debugRenderer.render(world, camera.combined)
            doPhysicStep(delta)
        }
    }

    fun checkChartDrawing() {
        isChartDrawable = (userTopWeek.size > 1 && userTopWeek.values.map { t -> t.second }.sum() > 0)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen#resize()
     */
    override fun resize(width: Int, height: Int) {
        parent.stage.viewport.update(width, height, true)
    }

    /** This method is inherited. Description is in parent class
     * @see Screen#hide()
     */
    override fun hide() {
        dispose()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen#pause()
     */
    override fun pause() {

    }

    /** This method is inherited. Description is in parent class
     *  @see Screen#resume()
     */
    override fun resume() {
        if (state == EGameState.RUNNING) {
            keyDown(Input.Keys.BACK)
        }
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen#dispose()
     */
    override fun dispose() {
        gameDispose()
        world.dispose()
        sb.dispose()
        debugRenderer.dispose()
        chartBuffer.dispose()
    }

    /**
     * This method is inherited. Description is in parent class
     * @see InputProcessor#keyDown()
     * Listens for HW back button and does actions instead of leaving application
     */
    override fun keyDown(keycode: Int): Boolean {
        return when (keycode) {
            Input.Keys.BACK -> {
                if (state == EGameState.RUNNING) {
                    pauseTable.isVisible = true
                    sb.setColor(0.5f, 0.5f, 0.5f, 1f)
                    state = EGameState.READY
                    pauseButton?.touchable = Touchable.disabled
                    infoContainer?.clearActions()
                    infoContainer?.setScale(1f)
                } else if ((state == EGameState.READY && description) || (state == EGameState.STOPPED && customGame)) {
                    customGame = false
                    difficulty = parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0])
                    state = EGameState.STOPPED
                    sb.setColor(1f, 1f, 1f, 1f)
                    loadDescriptionUi()
                } else if ((state == EGameState.STOPPED || state == EGameState.END) && description) {
                    customGame = false
                    parent.changeScreen(EScreen.GAMES)
                    Gdx.input.setCatchKey(Input.Keys.BACK, false)
                    Gdx.input.inputProcessor = parent.stage
                } else {
                    parent.changeScreen(EScreen.MENU)
                    Gdx.input.setCatchKey(Input.Keys.BACK, false)
                    Gdx.input.inputProcessor = parent.stage
                }
                true
            }
            Input.Keys.UP -> {
                if (parent.stage.isDebugAll) {
                    difficultyUp()
                    debugLabel?.setText("D=$difficulty : S=$score")
                }
                true
            }
            Input.Keys.DOWN -> {
                if (parent.stage.isDebugAll) {
                    difficultyDown()
                    debugLabel?.setText("D=$difficulty : S=$score")
                }
                true
            }
            else -> {
                false
            }
        }
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor#touchDown()
     */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return true
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor#touchUp()
     */
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return when (state) {
            EGameState.READY -> {
                if (!pauseTable.isVisible) {
                    pauseTable.isVisible = false
                    pauseButton?.touchable = Touchable.enabled
                    state = EGameState.RUNNING
                    sb.setColor(1f, 1f, 1f, 1f)
                    true
                } else false
            }
            EGameState.END -> {
                if (table.isTouchable) keyDown(Input.Keys.BACK)
                else false
            }
            else -> true
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see InputProcessor#mouseMoved()
     */
    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return true
    }

    /**
     * This method is inherited. Description is in parent class
     * @see InputProcessor#keyTyped()
     */
    override fun keyTyped(character: Char): Boolean {
        return true
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor#scrolled()
     */
    override fun scrolled(amount: Int): Boolean {
        return true
    }

    /** This method is inherited. Description is in parent class
     * @see InputProcessor#keyUp()
     */
    override fun keyUp(keycode: Int): Boolean {
        return true
    }

    /** This method is inherited. Description is in parent class
     * @see InputProcessor#touchDragged()
     */
    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return true
    }

    /** This method is inherited. Description is in parent class
     * @see ContactListener#endContact()
     */
    override fun endContact(contact: Contact) {
    }

    /** This method is inherited. Description is in parent class
     * @see ContactListener#beginContact()
     */
    override fun beginContact(contact: Contact) {

    }

    /** This method is inherited. Description is in parent class
     * @see ContactListener#preSolve()
     */
    override fun preSolve(contact: Contact?, oldManifold: Manifold?) {
    }

    /** This method is inherited. Description is in parent class
     * @see ContactListener#postSolve()
     */
    override fun postSolve(contact: Contact?, impulse: ContactImpulse?) {
    }

    /** This method is inherited. Description is in parent class
     *  Observer update method to receive data from custom firebase http connections
     */
    override fun update(observable: Observable, data: Any?) {
        Gdx.app.postRunnable(Runnable {
            if (parent.screen !is BlikGameScreen || state != EGameState.STOPPED) {
                parent.firebaseManager.deleteObserver(this)
                return@Runnable
            }

            Gdx.app.log("Fireapp Game Screen", data.toString())
            if (state != EGameState.READY && !customGame && data is ScoreModel) createDescriptionUi()
        })
    }
}