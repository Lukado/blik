package lukado.blik.screens.games

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameType
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.MosquitoProperty
import kotlin.math.round
import kotlin.random.Random
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Mosquito game
 */
class Mosquito(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = MosquitoProperty.LOCALE_NAME
        override val backgroundImagePath: String = MosquitoProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = MosquitoProperty.DIFFICULTY
        override val topScoresPreference: String = MosquitoProperty.TOP_SCORE
        override val weekScoresPreference: String = MosquitoProperty.WEEK_SCORE
        override val timesPlayedPreference: String = MosquitoProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = MosquitoProperty.FIRST_TIME
        override fun getClassInstance(): KClass<Mosquito> {
            return Mosquito::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("mo_game_description")
    override val gameType: EGameType = EGameType.VISUAL_SPATIAL
    override val gameDuration: Float = MosquitoProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null

    private var mosquitoPosition: Int = 0
    private var matrixSize: Int = 0
    private var guessedNumber: Int = 0
    private var directionsNumber: Int = 0
    private var difficultyCounter: Int = parent.preferences.getInteger(MosquitoProperty.DIFFICULTY_COUNTER, 0)

    private var gameTime: Float = MosquitoProperty.GAME_TIME
    private var firstTime: Boolean = true
    private var isShowing: Boolean = false
    private var gameEnd: Boolean = false

    //Game actors
    private var gjDirectionImage: Image? = null
    private var mosquitoImage: Image? = null
    private var gameTable: Table = Table()
    private var feedbackTable: Table = Table()

    private var directionMatrix: ArrayList<ArrayList<Int>> = ArrayList()

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)
        if (firstTime && gameTime <= currentGameDuration) {
            MosquitoProperty.SHOW_TIME.showMatrix(showMovement = true, fail = false)
        } else if (!firstTime && !isShowing && !gameTable.isTouchable) {
            isShowing = true
            gameTable.addAction(Actions.sequence(
                    Actions.alpha(1f, MosquitoProperty.SHOW_TIME / 10f),
                    Actions.run {
                        gameTable.touchable = Touchable.enabled
                        isShowing = false
                    }
            ))
        }

        //Debug method
        if (parent.stage.isDebugAll && !isShowing && matrixSize != round(difficulty / 2.5f).toInt() + 2) {
            createGameObjects()
        }

        gameTime -= delta
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {}

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return if (gameTime <= 0) {
            if (gameEnd) {
                if (!customGame) {
                    parent.preferences.putInteger(MosquitoProperty.DIFFICULTY_COUNTER, difficultyCounter)
                    parent.preferences.flush()
                }
                true
            } else {
                gameTime = 0f
                false
            }
        } else false
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        if (gameTable.isTouchable) {
            gameTable.touchable = Touchable.disabled
        }
    }

    /** Main grid displaying method. Controls startgame fadein, coloring boxes when tapping and calls functions afterwards. */
    private fun Float.showMatrix(showMovement: Boolean, fail: Boolean) {
        isShowing = true
        firstTime = false

        if (showMovement) {
            gameTable.addAction(Actions.sequence(Actions.fadeIn(0.5f, Interpolation.smoother), Actions.run {
                (gameTable.getChild(mosquitoPosition) as Image).addAction(Actions.sequence(
                        Actions.scaleTo(0f, 0f, this / 8, Interpolation.smoother),
                        Actions.run {
                            (gameTable.getChild(mosquitoPosition) as Image).drawable = mosquitoImage?.drawable
                        },
                        Actions.scaleTo(1f, 1f, this / 8, Interpolation.smoother),
                        Actions.delay(this / 2f),
                        Actions.scaleTo(0f, 0f, this / 8, Interpolation.smoother),
                        Actions.run {
                            (gameTable.getChild(mosquitoPosition) as Image).drawable = Image(textures?.findRegion(MosquitoProperty.BOX)).drawable
                        },
                        Actions.scaleTo(1f, 1f, this / 8, Interpolation.smoother),
                        Actions.run {
                            gameTable.addAction(Actions.sequence(
                                    Actions.alpha(0.1f, this / 10f),
                                    Actions.delay(this / 2f),
                                    Actions.run { moveMosquito(directionsNumber) }
                            ))
                        }))
            }))
        } else {
            if (fail) {
                (gameTable.getChild(mosquitoPosition) as Image).addAction(Actions.sequence(
                        Actions.scaleTo(0f, 0f, this / 8, Interpolation.smoother),
                        Actions.run {
                            (gameTable.getChild(mosquitoPosition) as Image).drawable = mosquitoImage?.drawable
                        },
                        Actions.scaleTo(1f, 1f, this / 8, Interpolation.smoother),
                        Actions.delay(this / 3),
                        Actions.run {
                            gameTable.addAction(Actions.sequence(Actions.fadeOut(0.5f, Interpolation.smoother), Actions.run {
                                difficultyCounter--
                                if (difficultyCounter <= -2) {
                                    difficultyCounter = 0
                                    difficultyDown()
                                }
                                guessedNumber = 0
                                if (gameTime <= 0) gameEnd = true
                                if (!gameEnd) createGameObjects()
                            }))
                        }))
            } else {
                difficultyCounter++
                if (difficultyCounter >= difficulty) {
                    difficultyUp()
                    difficultyCounter = 0
                }

                feedbackTable.addAction(Actions.sequence(Actions.delay(1f), Actions.parallel(
                        Actions.run {
                            gameTable.addAction(Actions.sequence(Actions.fadeOut(0.5f, Interpolation.smoother), Actions.run {
                                if (gameTime <= 0) gameEnd = true
                                if (!gameEnd) createGameObjects()
                            }))
                        },
                        Actions.fadeOut(0.5f, Interpolation.smoother))
                ))
            }
        }
    }

    /** Recursive direction display method */
    private fun moveMosquito(directionsNumber: Int) {
        //dog implementation, my mind cant process 8 direction movement in 1D array, in the time press given
        var badMove = true
        val mx = (mosquitoPosition - 1) % matrixSize
        val my = (mosquitoPosition - 1) / matrixSize

        var x: Int
        var y: Int
        do {
            x = Random.nextInt(3) - 1
            y = Random.nextInt(3) - 1
            if (mx + x in 0 until matrixSize && my + y in 0 until matrixSize && !(x == 0 && y == 0)) badMove = false
        } while (badMove)

        mosquitoPosition = directionMatrix[my + y][mx + x]

        if (y != 0) gjDirectionImage?.rotation = ((x - 1) / 2f) * 180 * y - 45 * y * x
        else gjDirectionImage?.rotation = ((x - 1) / 2f) * 180

        // 0.233f = min, 0.666f = max, dynamically changed speed
        feedbackTable.addAction(Actions.sequence(
                Actions.alpha(1f),
                Actions.delay(0.9f - difficulty / 15f),
                Actions.alpha(0f),
                Actions.delay(0.9f - difficulty / 15f),
                Actions.run {
                    if (directionsNumber == 0) isShowing = false
                    else moveMosquito(directionsNumber - 1)
                }
        ))
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))

        gameTime = if(!customGame) MosquitoProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)
        infoLabel?.setAlignment(Align.top)
        infoContainer?.padTop(20f)

        mosquitoImage = Image(textures?.findRegion(MosquitoProperty.MOSQUITO))
        gjDirectionImage = Image(textures?.findRegion(MosquitoProperty.ARROW))

        feedbackTable.clear()
        feedbackTable.setFillParent(true)
        feedbackTable.touchable = Touchable.disabled
        //filler to center response table
        feedbackTable.add(Label("", parent.skin)).row()
        feedbackTable.add(gjDirectionImage).expand()
        feedbackTable.color.a = 0f

        mosquitoPosition = 0
        matrixSize = 0
        guessedNumber = 0
        directionsNumber = 0
        difficultyCounter = parent.preferences.getInteger(MosquitoProperty.DIFFICULTY_COUNTER, 0)
        firstTime = true
        isShowing = false
        gameEnd = false
        directionMatrix.clear()
        gameTable.clear()

        createGameObjects()
        parent.stage.addActor(gameTable)
        parent.stage.addActor(feedbackTable)
    }

    /**
     * Creates main grid of boxes, where mosquito can spawn and move.
     */
    private fun createGameObjects() {
        gameTable.clear()
        gameTable.setFillParent(true)

        //filler to center game table
        gameTable.add(Label("", parent.skin))

        matrixSize = round(difficulty / 2.5f).toInt() + 2
        directionsNumber = (difficulty / 1.25f).toInt() + 2
        directionMatrix = ArrayList()
        var directionMatrixRow = ArrayList<Int>()
        //Game logic is in the click listener for each image
        for (i in 0 until matrixSize * matrixSize) {
            val image = Image(textures?.findRegion(MosquitoProperty.BOX))
            image.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    if (!gameTable.isTouchable) return
                    gameTable.touchable = Touchable.disabled
                    if (mosquitoPosition == i + 1) {
                        isShowing = true

                        guessedNumber++
                        score += difficulty * 50 + guessedNumber * 40 + directionsNumber * 30
                        debugLabel?.setText("D=$difficulty : S=$score : DC=$difficultyCounter")

                        gjDirectionImage?.drawable = Image(textures?.findRegion(MosquitoProperty.GOOD_JOB)).drawable
                        gjDirectionImage?.setOrigin(Align.bottomRight)
                        gjDirectionImage?.setScale(1f)
                        gjDirectionImage?.rotation = -20f

                        feedbackTable.addAction(Actions.parallel(
                                Actions.fadeIn(0.5f, Interpolation.smoother),
                                Actions.run { gjDirectionImage?.addAction(Actions.rotateBy(20f, 2f, Interpolation.elasticOut)) }
                        ))

                        image.addAction(Actions.sequence(
                                Actions.scaleTo(0f, 0f, MosquitoProperty.SHOW_TIME / 8, Interpolation.smoother),
                                Actions.run {
                                    (gameTable.getChild(mosquitoPosition) as Image).drawable = mosquitoImage?.drawable
                                },
                                Actions.scaleTo(1f, 1f, MosquitoProperty.SHOW_TIME / 8, Interpolation.smoother),
                                Actions.run {
                                    MosquitoProperty.SHOW_TIME.showMatrix(showMovement = false, fail = false)
                                }
                        ))
                    } else {
                        image.color = Color.RED
                        MosquitoProperty.SHOW_TIME.showMatrix(showMovement = false, fail = true)
                    }
                }
            })

            val spacing = MosquitoProperty.DEFAULT_BOX_SPACING / (matrixSize - 1)
            val sizing = (parent.stage.width - (matrixSize + 1) * spacing) / matrixSize
            if (i % matrixSize == 0) {
                gameTable.row()
                if (directionMatrixRow.isNotEmpty()) directionMatrix.add(directionMatrixRow)
                directionMatrixRow = ArrayList()
            }
            directionMatrixRow.add(i + 1)
            image.setOrigin(sizing / 2, sizing / 2)

            gameTable.add(image).fillX().space(0f, spacing, spacing, spacing).size(sizing)
        }
        directionMatrix.add(directionMatrixRow)

        gjDirectionImage?.drawable = Image(textures?.findRegion(MosquitoProperty.ARROW)).drawable
        gjDirectionImage?.rotation = 0f
        gjDirectionImage?.setOrigin(Align.center)
        gjDirectionImage?.setScale(2.5f)

        mosquitoPosition = Random.nextInt(gameTable.children.size - 1) + 1

        debugLabel?.setText("D=$difficulty : S=$score : DC=$difficultyCounter")
        gameTable.touchable = Touchable.disabled
        if (!firstTime) MosquitoProperty.SHOW_TIME.showMatrix(showMovement = true, fail = false)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }
}