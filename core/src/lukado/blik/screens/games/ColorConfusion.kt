package lukado.blik.screens.games

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Pool
import lukado.blik.Blik
import lukado.blik.configuration.AssetConfig
import lukado.blik.configuration.Config
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameType
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.ColorConfusionProperty
import kotlin.math.ceil
import kotlin.math.round
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Color Confusion game
 */
class ColorConfusion(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = ColorConfusionProperty.LOCALE_NAME
        override val backgroundImagePath: String = ColorConfusionProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = ColorConfusionProperty.DIFFICULTY
        override val topScoresPreference: String = ColorConfusionProperty.TOP_SCORE
        override val weekScoresPreference: String = ColorConfusionProperty.WEEK_SCORE
        override val timesPlayedPreference: String = ColorConfusionProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = ColorConfusionProperty.FIRST_TIME
        override fun getClassInstance(): KClass<ColorConfusion> {
            return ColorConfusion::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("cc_game_description")
    override val gameType: EGameType = EGameType.LANGUAGE
    override val gameDuration: Float = ColorConfusionProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null

    private var difficultyCounter: Int = 0
    private var gameTime: Float = ColorConfusionProperty.GAME_TIME

    //Game actors
    private val cardsStack = Stack()
    private val cardsTable = Table()
    private val optionsTable = Table()

    private var activeCards = ArrayList<Card>()
    private lateinit var cardsPool: Pool<Card>

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)

        if (!optionsTable.isTouchable && gameTime <= currentGameDuration && !activeCards.first().hit) {
            optionsTable.addAction(Actions.alpha(1f))
            optionsTable.isVisible = true
            optionsTable.touchable = Touchable.enabled
        }

        if (difficultyCounter >= difficulty * (ColorConfusionProperty.GUESSED_THRESHOLD / GameConfig.GAME_DIFFICULTIES.size)) {
            difficultyUp()
            if (!customGame) difficultyCounter = 0
            debugLabel?.setText("D:$difficulty S:$score")
        } else if (difficultyCounter <= -(GameConfig.GAME_DIFFICULTIES.last() - difficulty + 2)) {
            difficultyDown()
            if (!customGame) difficultyCounter = 0
            debugLabel?.setText("D:$difficulty S:$score")
        }

        for (i in activeCards.indices.reversed()) {
            val card = activeCards[i]
            if (!card.alive) {
                activeCards.removeAt(i)
                cardsPool.free(card)

                val newCard = cardsPool.obtain()
                newCard.init(difficulty)
                activeCards.add(newCard)

                createOptions()
            }
        }

        gameTime -= delta
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {}

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        if (optionsTable.isTouchable) {
            optionsTable.touchable = Touchable.disabled
            optionsTable.isVisible = false
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return gameTime <= 0
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))

        difficultyCounter = 0
        gameTime = if (!customGame) ColorConfusionProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)

        //Game actors
        cardsStack.clear()
        activeCards.clear()

        infoLabel?.setAlignment(Align.top)
        infoContainer?.padTop(20f)

        cardsTable.clear()
        cardsTable.setFillParent(true)

        optionsTable.clear()
        optionsTable.setFillParent(true)
        optionsTable.touchable = Touchable.disabled
        optionsTable.isVisible = false

        createCards()
        createOptions()

        parent.stage.addActor(cardsTable)
        parent.stage.addActor(optionsTable)
    }

    /** Creates pool of cards shown in the middle and fills it */
    private fun createCards() {
        cardsPool = object : Pool<Card>() {
            override fun newObject(): Card {
                return Card(parent, cardsStack, textures!!)
            }
        }

        for (i in 0..1) {
            val card = cardsPool.obtain()
            card.init(difficulty)
            activeCards.add(card)
        }

        cardsTable.add(cardsStack).size(Value.percentWidth(0.7f, cardsTable)).expand().colspan(2).padTop(infoLabel!!.height).padBottom(infoLabel!!.height * 4)
    }

    /** Creates possible options based on the difficulty */
    private fun createOptions() {
        optionsTable.clear()
        //add filler
        optionsTable.add(Actor()).expand()
        optionsTable.row()

        val listener = object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if ((event?.target as Label).text.toString() == parent.locale?.get(activeCards.first().cardColor?.localeName)) {
                    activeCards.first().throwAway(true)

                    if (difficultyCounter < 0) difficultyCounter = 0
                    else difficultyCounter++

                    score += 10 * difficulty + 10 * difficultyCounter
                    debugLabel?.setText("D:$difficulty S:$score")
                } else {
                    activeCards.first().throwAway(false)
                    if (difficultyCounter <= 0) difficultyCounter--
                    else difficultyCounter = 0
                }
                optionsTable.addAction(Actions.sequence(Actions.fadeOut(ColorConfusionProperty.THROW_DELAY, Interpolation.smoother)))
                optionsTable.touchable = Touchable.disabled
            }
        }

        var tbs: TextButton.TextButtonStyle
        var tb: TextButton

        var optionsArray: ArrayList<EGameColors> = arrayListOf()
        optionsArray.addAll(EGameColors.values().take(ceil(1 + difficulty * (EGameColors.values().size) / 10f).toInt()))
        optionsArray.remove(activeCards.first().cardColor)
        optionsArray.shuffle()
        optionsArray = ArrayList(optionsArray.take(round(1 + difficulty * 0.4f).toInt()))
        optionsArray.add(activeCards.first().cardColor!!)

        @Suppress("UNCHECKED_CAST") val optionsColors: ArrayList<EGameColors> = optionsArray.clone() as ArrayList<EGameColors>
        optionsArray.shuffle()
        optionsColors.shuffle()


        optionsArray.forEachIndexed { i, it ->
            tbs = TextButton.TextButtonStyle()
            tbs.font = parent.skin?.getFont("osifont")
            tbs.fontColor = optionsColors[i].color
            tbs.up = Image(parent.skin?.getRegion(AssetConfig.BUTTON)).drawable
            tbs.down = Image(parent.skin?.getRegion(AssetConfig.BUTTON_DOWN)).drawable
            tbs.checked = Image(parent.skin?.getRegion(AssetConfig.BUTTON_DOWN)).drawable

            tb = TextButton(parent.locale?.get(it.localeName), tbs)
            tb.align(Align.center)
            tb.addListener(listener)

            if (optionsArray.size - 1 == i && optionsArray.size % 2 != 0) optionsTable.add(tb).expandX().width(Value.percentWidth(0.5f, optionsTable)).pad(ColorConfusionProperty.DEFAULT_BOX_SPACING).colspan(2)
            else optionsTable.add(tb).uniformX().growX().pad(ColorConfusionProperty.DEFAULT_BOX_SPACING)

            if ((i + 1) % 2 == 0) optionsTable.row()
        }

        optionsTable.padBottom(infoLabel!!.height)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }
}

/**
 * Poolable class defining and generating main cards in game. When card is thrown away,
 * memory do not have to be freed as this can be reused by another new active card.
 * @param parent parent actor
 * @param textures card textures
 * @param stack stack actor for the card stack
 */
private class Card(val parent: Blik, val stack: Stack, val textures: TextureAtlas) : Pool.Poolable {
    var alive: Boolean = false
    var hit: Boolean = false
    var cardImage: Image? = null
    var cardLabel: Label? = null
    var cardColor: EGameColors? = null

    /**
     * Generation of new card with randomly color text and fill color
     * @param difficulty current difficulty
     */
    fun init(difficulty: Int) {
        alive = true
        cardImage = Image(textures.findRegion(ColorConfusionProperty.CARD))
        if (stack.hasChildren()) stack.addActorBefore(stack.children.first(), cardImage)
        else stack.add(cardImage)

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontHuge")
        cardColor = EGameColors.values().take(round(2f + difficulty * 0.4f).toInt()).random()
        ls.fontColor = cardColor?.color

        cardLabel = Label(parent.locale?.get(EGameColors.values().take(round(2f + difficulty * 0.4f).toInt()).random().localeName), ls)
        cardLabel?.setAlignment(Align.center)
        stack.addActorAfter(cardImage, cardLabel)
    }

    /**
     * Moves card to right or left position based on the picked option choice. Animates the card.
     */
    fun throwAway(right: Boolean) {
        hit = true
        cardLabel?.remove()
        if (right) {
            cardImage?.setOrigin(Align.bottomRight)

            val good = Image(textures.findRegion(ColorConfusionProperty.GOOD))
            good.setOrigin(cardImage!!.originX, cardImage!!.originY)
            stack.add(good)

            cardImage?.addAction(Actions.sequence(
                    Actions.delay(ColorConfusionProperty.THROW_DELAY),
                    Actions.parallel(
                            Actions.rotateBy(-90f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother),
                            Actions.moveBy(Config.APP_WIDTH / 2, 0f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother)),
                    Actions.visible(false),
                    Actions.run { alive = false },
                    Actions.removeActor()))

            good.addAction(Actions.sequence(
                    Actions.delay(ColorConfusionProperty.THROW_DELAY),
                    Actions.parallel(
                            Actions.rotateBy(-90f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother),
                            Actions.moveBy(Config.APP_WIDTH / 2, 0f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother)),
                    Actions.visible(false),
                    Actions.removeActor()))
        } else {
            cardImage?.setOrigin(Align.bottomLeft)

            val bad = Image(textures.findRegion(ColorConfusionProperty.BAD))
            bad.setOrigin(cardImage!!.originX, cardImage!!.originY)
            stack.add(bad)

            cardImage?.addAction(Actions.sequence(
                    Actions.delay(ColorConfusionProperty.THROW_DELAY),
                    Actions.parallel(
                            Actions.rotateBy(90f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother),
                            Actions.moveBy(-Config.APP_WIDTH, 0f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother)),
                    Actions.visible(false),
                    Actions.run { alive = false },
                    Actions.removeActor()))

            bad.addAction(Actions.sequence(
                    Actions.delay(ColorConfusionProperty.THROW_DELAY),
                    Actions.parallel(
                            Actions.rotateBy(90f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother),
                            Actions.moveBy(-Config.APP_WIDTH, 0f, ColorConfusionProperty.THROW_SPEED, Interpolation.smoother)),
                    Actions.visible(false),
                    Actions.removeActor()))
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see Pool.reset()
     */
    override fun reset() {
        alive = false
        hit = false

        cardImage = null
        cardLabel = null
        cardColor = null
    }
}

/**
 * Unique enum for this game color possibilities
 * @param localeName universal locale identification
 * @param color color
 */
enum class EGameColors(val localeName: String, val color: Color) {
    RED("cc_red", Color.valueOf("FF2F2F")),
    BLUE("cc_blue", Color.valueOf("2D32C8")),
    GREEN("cc_green", Color.valueOf("00A20D")),
    BLACK("cc_black", Color.valueOf("1B1B1B")),
    YELLOW("cc_yellow", Color.valueOf("DADD18")),
    PURPLE("cc_purple", Color.valueOf("6C119D")),
    BROWN("cc_brown", Color.valueOf("AF6E4D")),
    CYAN("cc_cyan", Color.valueOf("00E8E8")),
    ORANGE("cc_orange", Color.valueOf("FF7800")),
    PINK("cc_pink", Color.valueOf("FC74FD"))
}

