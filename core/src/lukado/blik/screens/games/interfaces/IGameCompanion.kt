package lukado.blik.screens.games.interfaces

import kotlin.reflect.KClass

/** Specific game information */
interface IGameCompanion {
    val localeName: String
    val backgroundImagePath: String
    val currentDifficultyPreference: String
    val topScoresPreference: String
    val weekScoresPreference: String
    val timesPlayedPreference: String
    val isFirstTimePreference: String
    fun getClassInstance(): KClass<*>
}