package lukado.blik.screens.games.interfaces

import com.badlogic.gdx.scenes.scene2d.ui.Image
import lukado.blik.enums.EGameType

/** General game information for game interface */
interface IGameInfo {
    val gameThumbnail: Image
    val gameDescription: String
    val gameType: EGameType
    val gameDuration: Float
}