package lukado.blik.screens.games

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.MathUtils.radiansToDegrees
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Pool
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameState
import lukado.blik.enums.EGameType
import lukado.blik.models.PhysicalObjectModel
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.TwoCarsProperty
import lukado.blik.tools.BodyEditorLoader
import kotlin.math.abs
import kotlin.math.floor
import kotlin.math.sign
import kotlin.random.Random
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Two Cars game
 */
class TwoCars(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = TwoCarsProperty.LOCALE_NAME
        override val backgroundImagePath: String = TwoCarsProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = TwoCarsProperty.DIFFICULTY
        override val topScoresPreference: String = TwoCarsProperty.TOP_SCORE
        override val weekScoresPreference: String = TwoCarsProperty.WEEK_SCORE
        override val timesPlayedPreference: String = TwoCarsProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = TwoCarsProperty.FIRST_TIME
        override fun getClassInstance(): KClass<TwoCars> {
            return TwoCars::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("tc_game_description")
    override val gameType: EGameType = EGameType.ATTENTION
    override val gameDuration: Float = TwoCarsProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null
    private var mainLaneTexture: Texture? = null
    private var sideLaneTexture: Texture? = null

    private var gameTime = TwoCarsProperty.GAME_TIME
    private var spawnTimer = System.currentTimeMillis()

    private var dead = false
    private var resetting = false

    private var pointsInRow: Int = 0
    private var objectsFailed: Int = 0
    private var resetBlink: Int = 0
    private var bcSign: Float = 0f
    private var rcSign: Float = 0f
    private var lastDeathTime: Long = 0
    private var linearDifficulty: Float = difficulty.toFloat()

    //Game actors
    private lateinit var redCar: PhysicalObjectModel
    private lateinit var blueCar: PhysicalObjectModel

    private val activeObjects = ArrayList<Objects>()
    private lateinit var objectsPool: Pool<Objects>

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)

        if (!dead) {
            doCarsMovement()
            doElementSpawning()
            doElementUpdate()
        } else {
            respawn()
        }

        //every 30 sec without fail diff up, linear speed ramp
        linearDifficulty += delta / 30
        if (linearDifficulty.toInt() > difficulty) {
            difficultyUp()
            objectsFailed = -1
        }
        gameTime -= delta
    }

    /**
     * Processes cars movement with given function in given position.
     */
    private fun doCarsMovement() {
        //Blue car
        var movingX = -5 * TwoCarsProperty.WORLD_WIDTH / 8 + (blueCar.body.userData as Int) * TwoCarsProperty.WORLD_WIDTH / 4
        bcSign = doCarMovement(blueCar.body, movingX, bcSign)

        //Red car
        movingX = -5 * TwoCarsProperty.WORLD_WIDTH / 8 + (redCar.body.userData as Int + 2) * TwoCarsProperty.WORLD_WIDTH / 4
        rcSign = doCarMovement(redCar.body, movingX, rcSign)

        // x*x*x*(x*(x*6-15)+10)
        // start + (end - start) * percent;
    }

    /**
     * Matematical function to calculate car movement on lane swap.
     */
    private fun doCarMovement(car: Body, movingX: Float, sign: Float): Float {
        var retSign = sign
        val movingPath = abs(movingX - car.position.x) * TwoCarsProperty.CAR_SPEED * difficulty
        if (movingPath.isFinite() && movingPath > 0.1f) {
            when {
                sign == 0f -> retSign = (movingX - car.position.x).sign
                sign != (movingX - car.position.x).sign -> {
                    car.linearVelocity = Vector2(0f, 0f)
                    car.angularVelocity = 0f
                    car.setTransform(movingX, car.position.y, 0f)
                }
                else -> {
                    car.linearVelocity = Vector2(retSign * movingPath, 0f)
//                    car.angularVelocity = Gdx.graphics.deltaTime
                }
            }
        } else if (movingPath != 0f) {
            car.linearVelocity = Vector2(0f, 0f)
            car.angularVelocity = 0f
            car.setTransform(movingX, -TwoCarsProperty.WORLD_HEIGHT / 2 + redCar.realHeight, 0f)
        }

        return retSign
    }

    /**
     * Generates pickable and obstacle elements in periodical time.
     */
    private fun doElementSpawning() {
        if (gameTime < currentGameDuration && (activeObjects.isEmpty() || activeObjects.last().element!!.body.position.y < TwoCarsProperty.WORLD_HEIGHT / 6)) {
            spawnTimer = System.currentTimeMillis()
            var element = objectsPool.obtain()
            element.init(true)
            activeObjects.add(element)

            element = objectsPool.obtain()
            element.init(false)
            activeObjects.add(element)
        }
    }

    /**
     * Processes element movement down the cars. Frees pool when they are out of view scope.
     */
    private fun doElementUpdate() {
        for (i in activeObjects.indices.reversed()) {
            val ele = activeObjects[i]
            ele.update(linearDifficulty)

            // if circle goes behind cars
            if (ele.element!!.body.userData.toString().contains("circle") && ele.element!!.body.position.y < -TwoCarsProperty.WORLD_HEIGHT / 2 + ele.element!!.realHeight / 2) {
                lastDeathTime = System.currentTimeMillis()
                dead = true
            }

            if (!ele.alive) {
                activeObjects.removeAt(i)
                objectsPool.free(ele)
            }
        }
    }

    /**
     * Cars respawn mechanics with difficulty lowering and stopping element spawning.
     */
    private fun respawn() {
        if ((System.currentTimeMillis() - lastDeathTime) / 1000 >= 0.5f && activeObjects.isNotEmpty()) {
            redCar.body.linearVelocity = Vector2(0f, 0f)
            redCar.body.angularVelocity = 0f
            redCar.body.userData = 1
            redCar.body.setTransform(-5 * TwoCarsProperty.WORLD_WIDTH / 8 + (redCar.body.userData as Int + 2) * TwoCarsProperty.WORLD_WIDTH / 4, -TwoCarsProperty.WORLD_HEIGHT / 2 + redCar.realHeight, 0f)

            blueCar.body.linearVelocity = Vector2(0f, 0f)
            blueCar.body.angularVelocity = 0f
            blueCar.body.userData = 2
            blueCar.body.setTransform(-5 * TwoCarsProperty.WORLD_WIDTH / 8 + (blueCar.body.userData as Int) * TwoCarsProperty.WORLD_WIDTH / 4, -TwoCarsProperty.WORLD_HEIGHT / 2 + blueCar.realHeight, 0f)

            activeObjects.forEach {
                objectsPool.free(it)
            }
            activeObjects.clear()
            resetting = true

            pointsInRow = 0
            objectsFailed++
            if (objectsFailed == 2) {
                difficultyDown()
                objectsFailed = 0
            }
            linearDifficulty = difficulty.toFloat()

            debugLabel?.setText("D=$difficulty : S=$score : PIR=$pointsInRow")
        } else if (activeObjects.isEmpty()) {
            // 0.3 divider alters the blinking speed (1/0.3*2 blinks per second)
            resetting = if (floor((System.currentTimeMillis() - lastDeathTime) / 1000f / 0.3f) % 2 == 0f) {
                if (!resetting) {
                    resetBlink++
                    // 3 blink to reset character
                    if (resetBlink == 3) {
                        dead = false
                        resetBlink = 0
                        return
                    }
                }
                true
            } else {
                false
            }
        } else {
            activeObjects.forEach {
                it.element?.body?.setLinearVelocity(0f, 0f)
            }
        }

        spawnTimer = System.currentTimeMillis()
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {
        //road lines
        sb.draw(mainLaneTexture, -0.5f, -TwoCarsProperty.WORLD_HEIGHT / 2, 1f, TwoCarsProperty.WORLD_HEIGHT)
        sb.draw(sideLaneTexture, -TwoCarsProperty.WORLD_WIDTH / 4 - 0.5f, -TwoCarsProperty.WORLD_HEIGHT / 2, 5 * TwoCarsProperty.WORLD_WIDTH / Gdx.graphics.width, TwoCarsProperty.WORLD_HEIGHT)
        sb.draw(sideLaneTexture, TwoCarsProperty.WORLD_WIDTH / 4 + 0.5f, -TwoCarsProperty.WORLD_HEIGHT / 2, 5 * TwoCarsProperty.WORLD_WIDTH / Gdx.graphics.width, TwoCarsProperty.WORLD_HEIGHT)

        //cars
        if (!resetting) {
            redCar.sprite?.setOriginBasedPosition(redCar.body.position.x, redCar.body.position.y)
            redCar.sprite?.rotation = radiansToDegrees * redCar.body.angle
            redCar.sprite?.draw(sb)
            blueCar.sprite?.setOriginBasedPosition(blueCar.body.position.x, blueCar.body.position.y)
            blueCar.sprite?.rotation = radiansToDegrees * blueCar.body.angle
            blueCar.sprite?.draw(sb)
        }

        //elements
        activeObjects.forEach { gameObject ->
            if (gameObject.element!!.body.userData != "picked") {
                gameObject.element?.sprite?.setOriginBasedPosition(gameObject.element!!.body.position.x, gameObject.element!!.body.position.y)
                gameObject.element?.sprite?.draw(sb)
            }
        }
    }

    /**
     * Detects touch to provide car movement for given car
     *
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.touchDown()
     */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return if (state == EGameState.RUNNING && !dead) {
            val vec = camera.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0f))
            if (vec.x > 0) {
                rcSign = 0f
                redCar.body.userData = if (redCar.body.userData as Int == 1) 2 else 1
            } else {
                bcSign = 0f
                blueCar.body.userData = if (blueCar.body.userData as Int == 1) 2 else 1
            }
            false
        } else super.touchDown(screenX, screenY, pointer, button)
    }

    /**
     * Detects collision between cars and elements and calculates outcome.
     *
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.beginContact()
     */
    override fun beginContact(contact: Contact) {
        val fa = contact.fixtureA
        val fb = contact.fixtureB
        if ((fa.body.userData != "picked" && fb.body.userData != "picked") && (fa.isSensor || fb.isSensor) && !dead) {
            if (fa.isSensor) fa.body.userData = "picked"
            else if (fb.isSensor) fb.body.userData = "picked"

            objectsFailed = 0
            pointsInRow++
            score += difficulty + pointsInRow * 2
            debugLabel?.setText("D=$difficulty : S=$score : PIR=$pointsInRow")
        } else if ((!fa.isSensor && !fb.isSensor) && !dead) {
            lastDeathTime = System.currentTimeMillis()
            dead = true
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return gameTime <= 0
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        spawnTimer = System.currentTimeMillis()
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))
        camera = OrthographicCamera(TwoCarsProperty.WORLD_WIDTH, TwoCarsProperty.WORLD_HEIGHT)

        gameTime = if (!customGame) TwoCarsProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)

        dead = false
        resetting = false
        pointsInRow = 0
        objectsFailed = 0
        resetBlink = 0
        bcSign = 0f
        rcSign = 0f
        lastDeathTime = 0
        linearDifficulty = difficulty.toFloat()
        spawnTimer = System.currentTimeMillis()
        activeObjects.clear()
        world.gravity = Vector2(0f, 0f)

        createLines()
        createCars()
        createObjects()
    }

    /**
     * Creates pool for element generating
     */
    private fun createObjects() {
        objectsPool = object : Pool<Objects>() {
            override fun newObject(): Objects? {
                return Objects(world, textures!!, bel!!)
            }
        }
    }

    /**
     * Creates lane textures
     */
    private fun createLines() {
        var bgPixmap = Pixmap(1, 1, Pixmap.Format.RGB565)
        bgPixmap.setColor(TwoCarsProperty.COLOR_MAIN_LINE)
        bgPixmap.fill()

        mainLaneTexture = Texture(bgPixmap)

        bgPixmap = Pixmap(1, 1, Pixmap.Format.RGB565)
        bgPixmap.setColor(TwoCarsProperty.COLOR_SIDE_LINE)
        bgPixmap.fill()

        sideLaneTexture = Texture(bgPixmap)
        bgPixmap.dispose()
    }

    /**
     * Defines cars physical objects with world properties
     */
    private fun createCars() {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.DynamicBody

        val rCar: Body = world.createBody(bodyDef)
        val bCar: Body = world.createBody(bodyDef)
        val fixtureDef = FixtureDef()
        fixtureDef.friction = 0.01f
        fixtureDef.density = 0.5f
        fixtureDef.restitution = 0.3f

        var sprite: Sprite = textures!!.createSprite(TwoCarsProperty.RED_CAR)
        sprite.setOriginCenter()

        bel?.attachFixture(rCar, TwoCarsProperty.CAR, fixtureDef, TwoCarsProperty.ELEMENTS_SCALE)
        rCar.fixtureList.first().userData = sprite
        rCar.userData = 1

        sprite = textures!!.createSprite(TwoCarsProperty.BLUE_CAR)
        sprite.setOriginCenter()

        bel?.attachFixture(bCar, TwoCarsProperty.CAR, fixtureDef, TwoCarsProperty.ELEMENTS_SCALE)
        bCar.fixtureList.first().userData = sprite
        bCar.userData = 2

        redCar = PhysicalObjectModel(rCar, TwoCarsProperty.ELEMENTS_SCALE)
        redCar.body.setTransform(-5 * TwoCarsProperty.WORLD_WIDTH / 8 + (rCar.userData as Int + 2) * TwoCarsProperty.WORLD_WIDTH / 4, -TwoCarsProperty.WORLD_HEIGHT / 2 + redCar.realHeight, 0f)

        blueCar = PhysicalObjectModel(bCar, TwoCarsProperty.ELEMENTS_SCALE)
        blueCar.body.setTransform(-5 * TwoCarsProperty.WORLD_WIDTH / 8 + (bCar.userData as Int) * TwoCarsProperty.WORLD_WIDTH / 4, -TwoCarsProperty.WORLD_HEIGHT / 2 + blueCar.realHeight, 0f)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
        textures = null
    }
}

/**
 * Poolable class defining and generating pickables and obstacles spawned in game. When element is out of view
 * scope, memory do not have to be freed as this can be reused by another new active obstacle.
 * @param world Box2D world
 * @param textures element textures
 * @param bel physical body editor class, for loading it
 */
private class Objects(private val world: World, private val textures: TextureAtlas, private val bel: BodyEditorLoader) : Pool.Poolable {
    var element: PhysicalObjectModel? = null
    var alive: Boolean = false

    /**
     * Initialization of new element randomly choosing if its pickable or obstacle and setting
     * appropriate properties
     * @param redColor if it's for game red side
     */
    fun init(redColor: Boolean) {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.KinematicBody
        val body: Body = world.createBody(bodyDef)

        val fixtureDef = FixtureDef()
        val sprite: Sprite
        val objectName: String

        //Randomly chosen obstacle or pickup point
        if (Random.nextBoolean()) {
            fixtureDef.friction = 0.01f
            fixtureDef.density = 0.5f
            fixtureDef.restitution = 0.3f

            objectName = if (redColor) {
                TwoCarsProperty.RED_SQUARE
            } else {
                TwoCarsProperty.BLUE_SQUARE
            }
        } else {
            fixtureDef.isSensor = true

            objectName = if (redColor) {
                TwoCarsProperty.RED_CIRCLE
            } else {
                TwoCarsProperty.BLUE_CIRCLE
            }
        }

        bel.attachFixture(body, objectName.substringAfter("_", "square"), fixtureDef, TwoCarsProperty.ELEMENTS_SCALE)
        sprite = textures.createSprite(objectName)
        sprite.setOriginCenter()
        body.fixtureList.first().userData = sprite
        body.userData = objectName

        element = PhysicalObjectModel(body, TwoCarsProperty.ELEMENTS_SCALE)

        val positionOffset = if (redColor) 3 else 1
        val lanePosition = Random.nextInt(2) + positionOffset
        element!!.body.setTransform(-5 * TwoCarsProperty.WORLD_WIDTH / 8 + lanePosition * TwoCarsProperty.WORLD_WIDTH / 4, positionOffset * TwoCarsProperty.ELEMENTS_OFFSET + TwoCarsProperty.WORLD_HEIGHT / 2, 0f)
        element!!.body.setLinearVelocity(0f, -10f)

        alive = true
    }

    /**
     * This method is inherited. Description is in parent class
     * @see Pool.reset()
     */
    override fun reset() {
        alive = false

        world.destroyBody(element!!.body)
        element = null
    }

    /** Updates element moving and checks if it's out of view scope */
    fun update(linearDifficulty: Float) {
        if (element!!.body.userData == "picked" || element!!.body.position.y < -TwoCarsProperty.WORLD_HEIGHT / 2 - element!!.realHeight) {
            alive = false
        } else {
            element!!.body.setLinearVelocity(0f, -10f - TwoCarsProperty.ELEMENTS_SPEED * linearDifficulty)
        }
    }
}