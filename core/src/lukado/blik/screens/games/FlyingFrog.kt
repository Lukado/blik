package lukado.blik.screens.games

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.utils.Pool
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameType
import lukado.blik.enums.EGameState
import lukado.blik.models.PhysicalObjectModel
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.FlyingFrogProperty
import lukado.blik.tools.BodyEditorLoader
import kotlin.math.*
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Flying Frog game
 */
class FlyingFrog(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = FlyingFrogProperty.LOCALE_NAME
        override val backgroundImagePath: String = FlyingFrogProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = FlyingFrogProperty.DIFFICULTY
        override val topScoresPreference: String = FlyingFrogProperty.TOP_SCORE
        override val weekScoresPreference: String = FlyingFrogProperty.WEEK_SCORE
        override val timesPlayedPreference: String = FlyingFrogProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = FlyingFrogProperty.FIRST_TIME
        override fun getClassInstance(): KClass<FlyingFrog> {
            return FlyingFrog::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("ff_game_description")
    override val gameType: EGameType = EGameType.ATTENTION
    override val gameDuration: Float = FlyingFrogProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null

    private var movingSign: Float = 0f
    private var lastDeathTime: Long = 0
    private var difficultyTime: Long = System.currentTimeMillis()
    private var lastSpawnedObstacleTime: Long = 0

    private var paused: Boolean = false
    private var pauseTime: Float = 0f
    private var lastTimePaused: Long = 0L
    private var lastGamePause: Long = 0L

    private var resetBlink: Float = 0f
    private var resetting: Boolean = false

    private var gameTime = FlyingFrogProperty.GAME_TIME

    private var bgWidth: Float = 0f
    private var bgMovSpeed: Float = 0f
    private var bgOverMovSpeed: Float = 0f

    private var obstaclesFailed: Int = 0

    //Game objects
    private var ground: PhysicalObjectModel? = null
    private var hero: PhysicalObjectModel? = null

    private var lastClickedYPos: Float = 0f

    private val bodies: ArrayList<PhysicalObjectModel> = ArrayList()
    private val activeObstacles: ArrayList<Obstacle> = ArrayList()
    private lateinit var obstaclePool: Pool<Obstacle>

    private lateinit var bg: Sprite
    private lateinit var bgOver: Sprite

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)

        if (hero!!.dead.not()) {
            if(lastGamePause>0L){
                difficultyTime += System.currentTimeMillis()-lastGamePause
                lastGamePause = 0L
            }
            doCharacterMovement()
            doObstacleSpawning()
            doObstacleUpdate()
        } else {
            releaseCharacter()
        }

        if (!paused) gameTime -= delta
    }

    /** Methematical operations to smoothly move character to clicked position */
    private fun doCharacterMovement() {
        var movingPath = abs(lastClickedYPos - hero!!.body.position.y) * FlyingFrogProperty.CHARACTER_SPEED * difficulty
        if (movingPath > 120) movingPath = 120f
        if (movingPath.isFinite() && movingPath > 0.1f) {
            when {
                movingSign == 0f -> movingSign = (lastClickedYPos - hero!!.body.position.y).sign
                movingSign != (lastClickedYPos - hero!!.body.position.y).sign -> {
                    hero?.body?.linearVelocity = Vector2(0f, 0f)
                    hero?.body?.setTransform(hero!!.body.position.x, lastClickedYPos, 0f)
                }
                else -> hero?.body?.linearVelocity = Vector2(0f, movingSign * movingPath)
            }
        } else {
            hero?.body?.linearVelocity = Vector2(0f, 0f)
            hero?.body?.setTransform(hero!!.body.position.x, lastClickedYPos, 0f)
        }
    }

    /** Logic behind obstacle spawning and resuming game from created events */
    private fun doObstacleSpawning() {
        if (!paused && gameTime <= currentGameDuration && (System.currentTimeMillis() - lastSpawnedObstacleTime) / 1000 > (FlyingFrogProperty.TUBES_SPAWN_TIME * 2f / difficulty)) {
            lastSpawnedObstacleTime = System.currentTimeMillis()
            val o = obstaclePool.obtain()
            o.init(FlyingFrogProperty.TUBES_SPAWN_TIME / difficulty)
            activeObstacles.add(o)
        } else if (paused && activeObstacles.isNotEmpty()) {
            FlyingFrogProperty.PAUSE_TIME.pauseGame()
        } else if (paused && infoLabel!!.text.isBlank()) {
            FlyingFrogProperty.PAUSE_TIME.pauseGame()
            if (obstaclesFailed > 0) {
                infoLabel?.setText(parent.locale?.get("ff_difficulty_decreased"))
            } else {
                infoLabel?.setText(parent.locale?.get("ff_difficulty_increased"))
            }
            debugLabel?.setText("D=$difficulty : DF=$obstaclesFailed : S=$score")
        } else if (paused && (System.currentTimeMillis() - lastTimePaused) / 1000 > pauseTime) {
            paused = false
            infoLabel?.setText("")
            obstaclesFailed = 0
            difficultyTime = System.currentTimeMillis()
            lastSpawnedObstacleTime = System.currentTimeMillis()
        }
    }

    /** Updates obstacle and counts user score logic */
    private fun doObstacleUpdate() {
        for (i in activeObstacles.indices.reversed()) {
            val obs = activeObstacles[i]
            obs.update()
            if (!obs.alive) {
                activeObstacles.removeAt(i)
                obstaclePool.free(obs)

                if (!paused) {
                    obstaclesFailed = 0
                    if ((System.currentTimeMillis() - difficultyTime) / 1000 > FlyingFrogProperty.LEVEL_INCREASE_TIME && difficulty < GameConfig.GAME_DIFFICULTIES.size) {
                        difficultyUp()
                        if (!customGame) FlyingFrogProperty.PAUSE_TIME.pauseGame()
                    }
                }

                score += 10 * difficulty + ((System.currentTimeMillis() - difficultyTime) / 1000 * 2).toInt()
                debugLabel?.setText("D=$difficulty : DF=$obstaclesFailed : S=$score")
            }
        }
    }

    /** Does whole logic when character dies, changes difficulty */
    private fun releaseCharacter() {
        if ((System.currentTimeMillis() - lastDeathTime) / 1000 >= 0.5f && hero!!.body.linearVelocity.isZero.not()) {
            hero?.body?.linearVelocity = Vector2(0f, 0f)
            hero?.body?.angularVelocity = 0f
            hero?.body?.setTransform(-FlyingFrogProperty.WORLD_WIDTH / 2 + hero!!.realWidth / 2 + FlyingFrogProperty.WORLD_WIDTH * 0.05f, 0f, 0f)
            lastClickedYPos = 0f
            activeObstacles.forEach {
                obstaclePool.free(it)
            }
            activeObstacles.clear()
            resetting = true

            obstaclesFailed++
            if (obstaclesFailed == 3 && !paused && difficulty > GameConfig.GAME_DIFFICULTIES[0]) {
                difficultyDown()
                if (!customGame) FlyingFrogProperty.PAUSE_TIME.pauseGame()
            }

            debugLabel?.setText("D=$difficulty : DF=$obstaclesFailed : S=$score")
        } else if (hero!!.body.linearVelocity.isZero) {
            // 0.3 divider alters the blinking speed (1/0.3*2 blinks per second)
            resetting = if (floor((System.currentTimeMillis() - lastDeathTime) / 1000f / 0.3f) % 2 == 0f) {
                if (!resetting) {
                    resetBlink++
                    // 3 blinks to reset character
                    if (resetBlink == 3f) {
                        hero?.dead = false
                        resetBlink = 0f
                        return
                    }
                }
                true
            } else {
                false
            }
        }
        difficultyTime = System.currentTimeMillis()
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {
        //bg draw
        if (state == EGameState.RUNNING) doBgMovement()
        sb.draw(bg, -FlyingFrogProperty.WORLD_WIDTH / 2f - bgMovSpeed, -FlyingFrogProperty.WORLD_HEIGHT / 2f, bgWidth, FlyingFrogProperty.WORLD_HEIGHT)
        sb.draw(bgOver, -FlyingFrogProperty.WORLD_WIDTH / 2f - bgOverMovSpeed, -FlyingFrogProperty.WORLD_HEIGHT / 2f, bgWidth, FlyingFrogProperty.WORLD_HEIGHT)

        //arrow draw
        activeObstacles.forEach {
            it.arrow?.draw(sb)
        }

        //world bodies
        bodies.forEach { gameObject ->
            gameObject.sprite?.setOriginBasedPosition(gameObject.body.position.x, gameObject.body.position.y)
            gameObject.sprite?.rotation = MathUtils.radiansToDegrees * gameObject.body.angle
            if (gameObject.dead.not()) {
                gameObject.sprite?.draw(sb)
            } else if (resetting.not()) {
                gameObject.sprite?.draw(sb)
            }
        }

        //ground
        sb.draw(ground?.body?.userData as Sprite, -FlyingFrogProperty.WORLD_WIDTH / 2, -FlyingFrogProperty.WORLD_HEIGHT / 2, FlyingFrogProperty.WORLD_WIDTH, FlyingFrogProperty.WORLD_GROUND_SIZE)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return if (gameTime <= 0) {
            if (activeObstacles.isNotEmpty()) {
                lastSpawnedObstacleTime = System.currentTimeMillis()
                gameTime = 0f
                false
            } else {
                hero?.body?.linearVelocity = Vector2(0f, 0f)
                hero?.body?.angularVelocity = 0f
                true
            }
        } else false
    }

    /**
     * Moves non-dead character to touched position.
     *
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.touchDown()
     */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return if (state == EGameState.RUNNING && hero?.dead == false) {
            val vec = camera.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0f))
            hero?.body?.angularVelocity = 0f
            lastClickedYPos = vec.y
            activeObstacles.forEach {
                if (!it.isDangerousObstacle() && lastClickedYPos in it.obstaclePosition.y - FlyingFrogProperty.TUBE_GAP_SIZE / 2..it.obstaclePosition.y + FlyingFrogProperty.TUBE_GAP_SIZE / 2) {
                    lastClickedYPos = it.obstaclePosition.y
                }
            }
            movingSign = 0f
            false
        } else super.touchDown(screenX, screenY, pointer, button)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.beginContact()
     */
    override fun beginContact(contact: Contact) {
        val fa = contact.fixtureA
        val fb = contact.fixtureB
        if ((fa.body.type == BodyDef.BodyType.DynamicBody || fb.body.type == BodyDef.BodyType.DynamicBody) && state === EGameState.RUNNING) {
            lastDeathTime = System.currentTimeMillis()
            hero?.dead = true
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        if (activeObstacles.size > 0) lastSpawnedObstacleTime = System.currentTimeMillis()
        lastGamePause = System.currentTimeMillis()
    }

    /** Method to pause game for changing difficulties */
    private fun Float.pauseGame() {
        paused = true
        lastTimePaused = System.currentTimeMillis()
        difficultyTime = System.currentTimeMillis()
        pauseTime = this
    }

    /** Method to continuously move background */
    private fun doBgMovement() {
        val move = (Gdx.graphics.deltaTime + difficulty / 30f)
        bgMovSpeed += move
        bgOverMovSpeed += move

        if (bgMovSpeed >= bgWidth) {
            bgMovSpeed = -bgWidth + bgOverMovSpeed + 2f
        }
        if (bgOverMovSpeed >= bgWidth) {
            bgOverMovSpeed = -bgWidth + bgMovSpeed + 2f
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))
        camera = OrthographicCamera(FlyingFrogProperty.WORLD_WIDTH, FlyingFrogProperty.WORLD_HEIGHT)

        gameTime = if (!customGame) FlyingFrogProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)

        world.gravity = Vector2(0f, 0f)

        lastClickedYPos = 0f
        movingSign = 0f
        lastDeathTime = 0
        lastSpawnedObstacleTime = 0
        difficultyTime = System.currentTimeMillis()
        paused = false
        pauseTime = 0f
        lastTimePaused = 0L
        lastGamePause = 0L
        resetBlink = 0f
        resetting = false
        bgWidth = 0f
        bgMovSpeed = 0f
        bgOverMovSpeed = 0f
        obstaclesFailed = 0
        bodies.clear()
        activeObstacles.clear()

        createObjects()
    }

    /**
     *  Creates world physics objects for game. For this game creates frog, floor and obstacles.
     */
    private fun createObjects() {
        createPlayer()
        createFloor()

        obstaclePool = object : Pool<Obstacle>() {
            override fun newObject(): Obstacle? {
                return Obstacle(world, textures!!, bel!!, bodies)
            }
        }

        bg = textures!!.createSprite(FlyingFrogProperty.BG_DAY)
        bgOver = textures!!.createSprite(FlyingFrogProperty.BG_DAY)
        bgWidth = FlyingFrogProperty.WORLD_HEIGHT * (bg.width / bg.height)
        bgOverMovSpeed = -bgWidth
    }

    /**
     * Defines player physical object and adds it the world pool.
     */
    private fun createPlayer() {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.DynamicBody

        val fixtureDef = FixtureDef()
        fixtureDef.friction = 1f
        fixtureDef.density = 0.5f
        fixtureDef.restitution = 0.1f

        val scale = FlyingFrogProperty.WORLD_HEIGHT * 0.05f
        val body: Body = world.createBody(bodyDef)
        val sprite: Sprite = textures!!.createSprite(FlyingFrogProperty.CHARACTER)
        sprite.setOriginCenter()

        bel?.attachFixture(body, FlyingFrogProperty.CHARACTER, fixtureDef, scale)
        body.fixtureList.first().userData = sprite

        hero = PhysicalObjectModel(body, scale)
        body.setTransform(-FlyingFrogProperty.WORLD_WIDTH / 2 + hero!!.realWidth / 2 + FlyingFrogProperty.WORLD_WIDTH * 0.05f, 0f, 0f)

        bodies.add(hero!!)
    }

    /**
     * Defines floor physical object and adds it to the world pool.
     */
    private fun createFloor() {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.StaticBody

        val shape = PolygonShape()
        shape.setAsBox(FlyingFrogProperty.WORLD_WIDTH / 2, FlyingFrogProperty.WORLD_GROUND_SIZE / 2)

        val fixtureDef = FixtureDef()
        fixtureDef.friction = 1f
        fixtureDef.shape = shape
        shape.dispose()

        val body: Body = world.createBody(bodyDef)
        body.createFixture(fixtureDef)
        body.setTransform(0f, -FlyingFrogProperty.WORLD_HEIGHT / 2 + FlyingFrogProperty.WORLD_GROUND_SIZE / 2, 0f)

        val sprite = textures!!.createSprite(FlyingFrogProperty.GROUND_DAY)
        body.userData = sprite

        ground = PhysicalObjectModel(body, FlyingFrogProperty.WORLD_GROUND_SIZE)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }
}

/**
 * Poolable class defining and generating obstacles spawned in game. When obstacle is out of view
 * scope, memory do not have to be freed as this can be reused by another new active obstacle.
 * @param world Box2D world
 * @param textures obstacle textures
 * @param bel physical body editor class, for loading it
 * @param bodies list of all obstacle bodies
 */
private class Obstacle(private var world: World, private var textures: TextureAtlas, private var bel: BodyEditorLoader, private var bodies: ArrayList<PhysicalObjectModel>) : Pool.Poolable {
    //Obstacle unique
    var alive: Boolean = false
    var obstaclePosition = Vector2(FlyingFrogProperty.WORLD_WIDTH / 1.8f, 0f)
    private var timer = 0f
    private var lastTime: Long = 0

    //World bodies
    private var tubeTop: PhysicalObjectModel? = null
    private var tubeBot: PhysicalObjectModel? = null
    var arrow: Sprite? = null

    /** Generating of obstacles
     * @param time time to start moving
     */
    fun init(time: Float) {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.KinematicBody
        val fixtureDef = FixtureDef()
        val scale = FlyingFrogProperty.WORLD_WIDTH * 0.1f

        do {
            obstaclePosition.y = -FlyingFrogProperty.WORLD_HEIGHT / 2 + FlyingFrogProperty.WORLD_GROUND_SIZE * 2 + MathUtils.random() * (FlyingFrogProperty.WORLD_HEIGHT - FlyingFrogProperty.WORLD_GROUND_SIZE * 4) + FlyingFrogProperty.TUBE_GAP_SIZE / 2
        } while (obstaclePosition.y in (bodies[0].body.position.y - bodies[0].realHeight / 2..bodies[0].body.position.y + bodies[0].realHeight / 2))


        if (MathUtils.randomBoolean(0.8f)) {
            arrow = textures.createSprite(FlyingFrogProperty.GREEN_ARROW)

            val bodyBot: Body = world.createBody(bodyDef)
            val bodyTop: Body = world.createBody(bodyDef)
            val sprite = textures.createSprite(FlyingFrogProperty.TUBE)
            sprite.setOrigin(0f, sprite.height)

            bel.attachFixture(bodyBot, FlyingFrogProperty.TUBE, fixtureDef, scale)
            bodyBot.fixtureList.first().userData = sprite
            bodyBot.userData = "tube_bot"

            bel.attachFixture(bodyTop, FlyingFrogProperty.TUBE, fixtureDef, scale)
            bodyTop.fixtureList.first().userData = sprite
            bodyTop.userData = "tube_top"

            tubeBot = PhysicalObjectModel(bodyBot, scale)
            tubeTop = PhysicalObjectModel(bodyTop, scale)

            tubeBot!!.body.setTransform(obstaclePosition.x, obstaclePosition.y - FlyingFrogProperty.TUBE_GAP_SIZE / 2, 0f)
            tubeTop!!.body.setTransform(obstaclePosition.x + tubeTop!!.realWidth, obstaclePosition.y + FlyingFrogProperty.TUBE_GAP_SIZE / 2, MathUtils.degreesToRadians * 180)
        } else {
            arrow = textures.createSprite(FlyingFrogProperty.RED_ARROW)

            val bodyBot: Body = world.createBody(bodyDef)
            val sprite = textures.createSprite(FlyingFrogProperty.ANTI_TUBE)
            sprite.setOriginCenter()

            bel.attachFixture(bodyBot, FlyingFrogProperty.ANTI_TUBE, fixtureDef, scale)
            bodyBot.fixtureList.first().userData = sprite
            bodyBot.userData = "anti_tube"

            tubeBot = PhysicalObjectModel(bodyBot, scale)

            tubeBot!!.body.setTransform(obstaclePosition.x, obstaclePosition.y, 0f)
        }

        arrow?.setSize(FlyingFrogProperty.TUBE_GAP_SIZE / (arrow!!.width / arrow!!.height), FlyingFrogProperty.TUBE_GAP_SIZE)
        arrow?.setOrigin(arrow!!.width, arrow!!.height / 2)
        arrow?.setOriginBasedPosition(FlyingFrogProperty.WORLD_WIDTH / 2 - FlyingFrogProperty.WORLD_WIDTH * 0.05f, obstaclePosition.y)

        bodies.add(tubeBot!!)
        if (tubeTop != null) bodies.add(tubeTop!!)

        timer = time
        lastTime = System.currentTimeMillis()
        alive = true
    }

    /**
     * This method is inherited. Description is in parent class
     * @see Pool.reset()
     */
    override fun reset() {
        alive = false
        bodies.remove(tubeBot!!)
        world.destroyBody(tubeBot?.body)
        if (tubeTop != null) {
            bodies.remove(tubeTop!!)
            world.destroyBody(tubeTop?.body)
        }
        tubeBot = null
        tubeTop = null
        arrow = null
    }

    /**
     * Updates obstacle every render step. Moves it and checks if it's out of scope
     */
    fun update() {
        //Should already move obstacle?
        if ((System.currentTimeMillis() - lastTime) / 1000f >= timer && !tubeBot!!.body.isAwake) {
            tubeBot!!.body.setLinearVelocity(-FlyingFrogProperty.TUBES_SPEED / timer, 0f)
            if (tubeTop != null) {
                tubeTop!!.body.setLinearVelocity(-FlyingFrogProperty.TUBES_SPEED / timer, 0f)
            } else {
                tubeBot!!.body.angularVelocity = 5f
            }
            arrow = null
        }

        //Is out of screen?
        if (FlyingFrogProperty.WORLD_WIDTH / 1.8f + tubeBot!!.body.position.x + tubeBot!!.realWidth <= 0) {
            alive = false
        }
    }

    /**
     *  Creates obstacle to dodge that to fly through
     */
    fun isDangerousObstacle(): Boolean {
        return tubeTop == null
    }

}