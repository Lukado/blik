package lukado.blik.screens.games

import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameType
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.DangerousPathProperty
import com.badlogic.gdx.math.GridPoint2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable
import kotlin.math.round
import kotlin.random.Random
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Dangerous Path game
 */
class DangerousPath(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = DangerousPathProperty.LOCALE_NAME
        override val backgroundImagePath: String = DangerousPathProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = DangerousPathProperty.DIFFICULTY
        override val topScoresPreference: String = DangerousPathProperty.TOP_SCORE
        override val weekScoresPreference: String = DangerousPathProperty.WEEK_SCORE
        override val timesPlayedPreference: String = DangerousPathProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = DangerousPathProperty.FIRST_TIME
        override fun getClassInstance(): KClass<DangerousPath> {
            return DangerousPath::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("dp_game_description")
    override val gameType: EGameType = EGameType.MEMORY
    override val gameDuration: Float = DangerousPathProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null

    private var matrixSize: Int = 0
    private var guessedNumber: Int = 0
    private var difficultyCounter: Int = 0
    private var difficultyHalf: Int = 0
    private val maxObjectsPerRow: Int = 6
    private var generateTime: Long = 0L
    private var isDeadlock: Boolean = false

    private var firstTime: Boolean = true
    private var isShowing: Boolean = false
    private var gameEnd: Boolean = false

    private var gameTime = DangerousPathProperty.GAME_TIME

    //Game actors
    private var gjImage: Image? = null
    private val matrixMemory: ArrayList<Int> = ArrayList()
    private val gameTable: Table = Table()
    private val feedbackTable: Table = Table()

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)
        if (firstTime && gameTime <= DangerousPathProperty.GAME_TIME) {
            DangerousPathProperty.SHOW_TIME.showMatrix(showBoxes = true, fail = false)
        } else if (!firstTime && !isShowing && !gameTable.isTouchable) {
            isShowing = true
            gameTable.children.forEach {
                if (it is Image) {
                    if (it.color == DangerousPathProperty.COLOR_EDGE_POINT) {
                        it.addAction(Actions.sequence(
                                Actions.delay(0.3f),
                                Actions.run {
                                    if (it == gameTable.children.last()) {
                                        isShowing = false
                                        gameTable.touchable = Touchable.enabled
                                    }
                                }))
                        return@forEach
                    } else {
                        it.addAction(Actions.sequence(
                                Actions.color(DangerousPathProperty.COLOR_DEFAULT, 0.3f),
                                Actions.run {
                                    if (it == gameTable.children.last()) {
                                        isShowing = false
                                        gameTable.touchable = Touchable.enabled
                                    }
                                }))
                    }
                }
            }
        }

        gameTime -= delta
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {}

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return if (gameTime <= 0) {
            if (gameEnd) {
                true
            } else {
                gameTime = 0f
                false
            }
        } else false
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        if (gameTable.isTouchable) {
            gameTable.touchable = Touchable.disabled
        }
    }

    /** Method to pause game for showing animations and changing difficulties. Shows correct or
     * wrong choices.
     * @param showBoxes fade in new turn
     * @param fail user missed
     */
    private fun Float.showMatrix(showBoxes: Boolean, fail: Boolean) {
        isShowing = true
        firstTime = false

        if (showBoxes) {
            gameTable.addAction(Actions.sequence(Actions.fadeIn(0.5f, Interpolation.smoother), Actions.run {
                gameTable.children.filterIndexed { i, _ -> !matrixMemory.contains(i - 1) }.forEach {
                    if (it is Image)
                        it.addAction(Actions.sequence(
                                Actions.scaleTo(0f, 0f, this / 8, Interpolation.smoother),
                                Actions.run { it.drawable = TextureRegionDrawable(textures?.findRegion(DangerousPathProperty.BOMB)) },
                                Actions.scaleTo(1f, 1f, this / 8, Interpolation.smoother),
                                Actions.delay(this / 1.5f),
                                Actions.scaleTo(0f, 0f, this / 8, Interpolation.smoother),
                                Actions.run { it.drawable = TextureRegionDrawable(textures?.findRegion(DangerousPathProperty.BOX_HIDDEN)) },
                                Actions.scaleTo(1f, 1f, this / 8, Interpolation.smoother),
                                Actions.run {
                                    isShowing = false
                                }))
                }
            }))
        } else {
            if (fail) {
                matrixMemory.forEach {
                    val item = gameTable.getChild(1 + it) as Image
                    if (item.color == DangerousPathProperty.COLOR_DEFAULT) {
                        item.addAction(Actions.sequence(
                                Actions.color(DangerousPathProperty.COLOR_FAIL, this / 3, Interpolation.smoother),
                                Actions.delay(this / 3),
                                Actions.run {
                                    if (!gameTable.hasActions()) {
                                        gameTable.addAction(Actions.sequence(Actions.fadeOut(0.5f, Interpolation.smoother), Actions.run {
                                            // goto: val maxObjectsPerRow = 6
                                            val prevHalf: Int = if (difficulty > maxObjectsPerRow) {
                                                round(((maxObjectsPerRow + 1) * (maxObjectsPerRow + 1) + (matrixSize - (maxObjectsPerRow + 1) - 1) * (maxObjectsPerRow + 1)) / 2f).toInt()
                                            } else {
                                                round(((matrixSize - 1) * (matrixSize - 1)) / 2f).toInt()
                                            }

                                            if (prevHalf >= difficultyCounter && difficulty > GameConfig.GAME_DIFFICULTIES[0]) {
                                                difficultyDown()
                                            } else {
                                                if (difficultyCounter > 2) difficultyCounter--
                                            }
                                            if (gameTime <= 0) gameEnd = true
                                            if (!gameEnd) createGameObjects()
                                        }))
                                    }
                                }))
                    }
                }
            } else {
                guessedNumber = 0
                feedbackTable.addAction(Actions.sequence(Actions.delay(1f), Actions.parallel(
                        Actions.run {
                            gameTable.addAction(Actions.sequence(Actions.fadeOut(0.5f, Interpolation.smoother), Actions.run {
                                if (difficultyHalf <= difficultyCounter && difficulty < GameConfig.GAME_DIFFICULTIES.size) {
                                    difficultyUp()
                                } else {
                                    if (difficultyCounter < difficultyHalf) difficultyCounter++
                                }
                                gjImage?.rotation = -20f

                                if (gameTime <= 0) gameEnd = true
                                if (!gameEnd) createGameObjects()
                            }))
                        },
                        Actions.fadeOut(0.5f, Interpolation.smoother))
                ))
            }
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))

        gameTime = if (!customGame) DangerousPathProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)
        infoLabel?.setAlignment(Align.top)
        infoContainer?.padTop(20f)

        gjImage = Image(textures?.findRegion(DangerousPathProperty.GOOD_JOB))
        gjImage?.setOrigin(Align.bottomRight)
        gjImage?.rotation = -20f
        feedbackTable.clear()
        feedbackTable.setFillParent(true)
        feedbackTable.touchable = Touchable.disabled
        feedbackTable.add(gjImage)
        feedbackTable.color.a = 0f

        matrixSize = 0
        guessedNumber = 0
        difficultyCounter = 0
        difficultyHalf = 0
        firstTime = true
        isShowing = false
        gameEnd = false

        createGameObjects()
        parent.stage.addActor(gameTable)
        parent.stage.addActor(feedbackTable)
    }

    /**
     * Creates new game grid of boxes. Adds listeners to all boxes which contains most of the game
     * logic. Computes properties for dynamic difficulty change.
     */
    private fun createGameObjects() {
        gameTable.clear()
        gameTable.setFillParent(true)

        //filler to center table
        gameTable.add(Label("", parent.skin))
        var highDifficultyObjectStopper = 0
        guessedNumber = 0
        matrixSize = difficulty + 1

        //Game logic is in the click listener for each image
        for (i in 0 until matrixSize * matrixSize) {
            val image = Image(textures?.findRegion(DangerousPathProperty.BOX_HIDDEN))
            image.color = DangerousPathProperty.COLOR_DISABLED
            image.addListener(object : ClickListener() {
                override fun enter(event: InputEvent?, x: Float, y: Float, pointer: Int, fromActor: Actor?) {
                    isShowing = true
                    if (matrixMemory.contains(i)) {
                        if (image.color != DangerousPathProperty.COLOR_DEFAULT) return

                        guessedNumber++
                        score += difficulty + guessedNumber + difficultyCounter
                        debugLabel?.setText("D=$difficulty : S=$score")

                        if (guessedNumber == matrixMemory.size - 2) {
                            gameTable.touchable = Touchable.disabled
                            feedbackTable.addAction(Actions.parallel(
                                    Actions.fadeIn(0.5f, Interpolation.smoother),
                                    Actions.run { gjImage?.addAction(Actions.rotateBy(20f, 2f, Interpolation.elasticOut)) }
                            ))
                        }

                        image.addAction(Actions.sequence(
                                Actions.color(DangerousPathProperty.COLOR_PATH, 0.3f),
                                Actions.run {
                                    if (guessedNumber == matrixMemory.size - 2) {
                                        DangerousPathProperty.SHOW_TIME.showMatrix(showBoxes = false, fail = false)
                                    }
                                }
                        ))
                    } else {
                        if (feedbackTable.color.a > 0) return
                        image.drawable = TextureRegionDrawable(textures?.findRegion(DangerousPathProperty.BOMB))
                        gameTable.touchable = Touchable.disabled
                        DangerousPathProperty.SHOW_TIME.showMatrix(showBoxes = false, fail = true)
                    }
                    return
                }
            })

            var spacing = DangerousPathProperty.DEFAULT_BOX_SPACING / difficulty
            var sizing = (parent.stage.width - (matrixSize + 1) * spacing) / matrixSize
            if (difficulty > maxObjectsPerRow) {
                if (i % (maxObjectsPerRow + 1) == 0) {
                    gameTable.row()
                    if (++highDifficultyObjectStopper == matrixSize + 1) break
                }
                spacing = DangerousPathProperty.DEFAULT_BOX_SPACING / maxObjectsPerRow
                sizing = (parent.stage.width - (maxObjectsPerRow + 2) * spacing) / (maxObjectsPerRow + 1)
            } else if (i % matrixSize == 0) {
                gameTable.row()
            }

            image.setOrigin(sizing / 2, sizing / 2)

            gameTable.add(image).fillX().space(0f, spacing, spacing, spacing).size(sizing)
        }

        difficultyHalf = round((gameTable.cells.size - 1) / 2f).toInt() + difficulty
        if (difficultyCounter == 0) difficultyCounter = difficultyHalf - difficulty - round(difficulty / 2f).toInt()

        var deadlockCounter = 0
        do {
            generatePath(difficultyCounter - deadlockCounter)
            if (isDeadlock || matrixMemory.isEmpty()) deadlockCounter++
        } while (isDeadlock || matrixMemory.isEmpty())

        debugLabel?.setText("D=$difficulty : S=$score")
        gameTable.touchable = Touchable.disabled
        if (!firstTime) DangerousPathProperty.SHOW_TIME.showMatrix(showBoxes = true, fail = false)
    }

    /**
     * Generates new path of given length between starting and ending point in matrix.
     * @param length path length
     */
    private fun generatePath(length: Int) {
        matrixMemory.clear()
        isDeadlock = false
        var sizeX = matrixSize
        var sizeY = matrixSize

        if (difficulty > maxObjectsPerRow) {
            sizeX = maxObjectsPerRow + 1
            sizeY = (gameTable.children.size - 1) / sizeX
        }
        val visitedMatrix = Array(sizeY) { Array(sizeX) { 0 } }

        val startPoint = GridPoint2(Random.nextInt(sizeX), Random.nextInt(sizeY))

        visitedMatrix[startPoint.y][startPoint.x] = 1

        generateTime = System.currentTimeMillis()

        Direction.values().toMutableList().shuffled().forEach {
            if (isDeadlock) return
            val nextPoint = extendPath(length, startPoint, it, visitedMatrix)
                    ?: return@forEach
            matrixMemory.add(nextPoint.x + sizeX * nextPoint.y)
            matrixMemory.add(startPoint.x + sizeX * startPoint.y)

            (gameTable.getChild(1 + matrixMemory.first()) as Image).color = DangerousPathProperty.COLOR_EDGE_POINT
            (gameTable.getChild(1 + matrixMemory.last()) as Image).color = DangerousPathProperty.COLOR_EDGE_POINT
            return
        }
    }

    /**
     * Recursive function to extend and check path in next move.
     * @param remainingLength remaining length
     * @param startPoint entry position
     * @param direction moved direction
     * @param visitedMatrix matrix of already visited positions
     * @return next point in generated path
     */
    private fun extendPath(remainingLength: Int, startPoint: GridPoint2, direction: Direction, visitedMatrix: Array<Array<Int>>): GridPoint2? {
        // Proceed in the given direction to find our next point.
        val currentPoint = startPoint.cpy()
        currentPoint.add(direction.point)

        // If point is out of bounds OR point is already visited
        if (currentPoint.x < 0 || currentPoint.y < 0 || currentPoint.x >= visitedMatrix.first().size || currentPoint.y >= visitedMatrix.size || visitedMatrix[currentPoint.y][currentPoint.x] == 1) {
            return null
        }

        // Avoid touching/crossing path so far.
        val scanMoves = Direction.values().filterNot { it.name == direction.oppositeDirection }.toMutableList().shuffled()
        scanMoves.forEach {
            val testPoint = currentPoint.cpy()
            testPoint.add(it.point)
            if (testPoint.x >= 0 && testPoint.y >= 0 && testPoint.x < visitedMatrix.first().size && testPoint.y < visitedMatrix.size && visitedMatrix[testPoint.y][testPoint.x] == 1) {
                return null
            }
        }

        visitedMatrix[currentPoint.y][currentPoint.x] = 1

        // Endpoint! Time to bubble back up.
        if (remainingLength == 1) {
            return currentPoint
        }

        scanMoves.forEach {
            if (isDeadlock) return null
            // Search for a feasible solution to a smaller sub-problem.
            val nextPoint = extendPath(remainingLength - 1, currentPoint, it, visitedMatrix)
                    ?: return@forEach
            // Extend that path to include this point.
            if (difficulty > maxObjectsPerRow) matrixMemory.add(nextPoint.x + (maxObjectsPerRow + 1) * nextPoint.y)
            else matrixMemory.add(nextPoint.x + matrixSize * nextPoint.y)

            return currentPoint
        }

        // No such path through this point panned out. Undo our choice to try it.
        visitedMatrix[currentPoint.y][currentPoint.x] = 0

        if ((System.currentTimeMillis() - generateTime) / 1000f > 0.5f) {
            isDeadlock = true
        }
        return null
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }
}

/**
 * Unique enum class for possible moving directions.
 * @param point point direction
 * @param oppositeDirection opposite enum named direction
 */
enum class Direction(val point: GridPoint2, val oppositeDirection: String) {
    NORTH(GridPoint2(0, -1), "SOUTH"),
    SOUTH(GridPoint2(0, 1), "NORTH"),
    EAST(GridPoint2(1, 0), "WEST"),
    WEST(GridPoint2(-1, 0), "EAST")
}