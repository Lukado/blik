package lukado.blik.screens.games

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.physics.box2d.*
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Value
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Pool
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameType
import lukado.blik.screens.BlikGameScreen
import lukado.blik.enums.EGameState
import lukado.blik.models.PhysicalObjectModel
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.BubblesProperty
import lukado.blik.tools.BodyEditorLoader
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Bubbles game
 */
class Bubbles(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = BubblesProperty.LOCALE_NAME
        override val backgroundImagePath: String = BubblesProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = BubblesProperty.DIFFICULTY
        override val topScoresPreference: String = BubblesProperty.TOP_SCORE
        override val weekScoresPreference: String = BubblesProperty.WEEK_SCORE
        override val timesPlayedPreference: String = BubblesProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = BubblesProperty.FIRST_TIME
        override fun getClassInstance(): KClass<Bubbles> {
            return Bubbles::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("bu_game_description")
    override val gameType: EGameType = EGameType.EXECUTIVE
    override val gameDuration: Float = BubblesProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null

    private var targetNumber: Int = 0
    private var currentNumber: Int = 0

    private var gameTime: Float = BubblesProperty.GAME_TIME
    private var spawnTimer: Long = System.currentTimeMillis()

    //Game actors
    private var targetNumberLabel: Label = Label("", parent.skin)
    private var currentNumberLabel: Label = Label("", parent.skin)

    private var feedbackTable: Table = Table()
    private var bottomTable: Table = Table()
    private var groundTexture: Sprite? = null
    private var skyTexture: Sprite? = null

    private val activeBubbles = ArrayList<Bubble>()
    private lateinit var bubblesPool: Pool<Bubble>

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)

        doBubbleSpawning()
        doBubbleUpdate()

        gameTime -= delta
    }

    /** Logic behind bubble spawning and updating from created events */
    private fun doBubbleSpawning() {
        //difficulty changes spawn time, ranges from <2, 1> seconds
        if (gameTime < currentGameDuration && (System.currentTimeMillis() - spawnTimer) / 1000f > 2f - difficulty * 0.1f) {
            spawnTimer = System.currentTimeMillis()
            val bubble = bubblesPool.obtain()
            bubble.init(difficulty)
            activeBubbles.add(bubble)
        }
    }

    /**
     * Updates alive bubbles and processes game progress. Decreases and increases difficulty based
     * on alive bubble number.
     */
    private fun doBubbleUpdate() {
        var newTarget = false
        var tooManyBubbles = false
        for (i in activeBubbles.indices.reversed()) {
            val bub = activeBubbles[i]
            bub.update(difficulty)
            if (!bub.alive) {
                activeBubbles.removeAt(i)
                bubblesPool.free(bub)
                newTarget = true
            } else if (bub.overLimit) {
                tooManyBubbles = true
            }
        }

        if (tooManyBubbles) {
            for (i in 0 until activeBubbles.size / 2) {
                val bub = activeBubbles[activeBubbles.size - i - 1]
                if (bub.bubble?.sprite?.color == BubblesProperty.COLOR_PICKED) {
                    currentNumber -= bub.number
                }

                activeBubbles.removeAt(activeBubbles.size - i - 1)
                bubblesPool.free(bub)
            }
            difficultyDown()
        }

        if (newTarget) {
            if (activeBubbles.size < 3) {
                difficultyUp()
                spawnInitialBubbles()
            }
            generateTargetNumber()
            targetNumberLabel.setText(targetNumber)
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {
        //sky limit texture
        skyTexture?.draw(sb)

        //world bodies (bubbles)
        activeBubbles.forEach { gameObject ->
            gameObject.bubble?.sprite?.setOriginBasedPosition(gameObject.bubble!!.body.position.x, gameObject.bubble!!.body.position.y)
            gameObject.bubble?.sprite?.rotation = MathUtils.radiansToDegrees * gameObject.bubble!!.body.angle
            gameObject.bubble?.sprite?.draw(sb)
        }

        //ground texture
        groundTexture?.draw(sb)
    }

    /**
     * Processes hit bubbles and marks them until all marked bubbles equals to required sum.
     * Main computational logic for this game.
     *
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.touchDown()
     */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return if (state == EGameState.RUNNING && gameTime < currentGameDuration) {
            val vec = camera.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0f))

            //Coloring and scoring in case of completetion
            activeBubbles.forEach { bub ->
                bub.bubble!!.body.fixtureList.forEach {
                    if (it.testPoint(vec.x, vec.y)) {
                        if (bub.bubble!!.sprite?.color == BubblesProperty.COLOR_PICKED) {
                            bub.bubble!!.sprite?.color = BubblesProperty.COLOR_UNPICKED
                            currentNumber -= bub.number
                        } else {
                            bub.bubble!!.sprite?.color = BubblesProperty.COLOR_PICKED
                            currentNumber += bub.number
                        }

                        // target achieved by user, score points
                        if (currentNumber == targetNumber) {
                            var usedBubbles = 0
                            activeBubbles.forEach { bub ->
                                if (bub.bubble?.sprite?.color == BubblesProperty.COLOR_PICKED) {
                                    usedBubbles++
                                    bub.alive = false
                                }
                            }

                            currentNumber = 0

                            score += 10 * difficulty + 50 * usedBubbles
                            debugLabel?.setText("D=$difficulty : S=$score")
                        }

                        currentNumberLabel.setText(currentNumber)
                    }
                }
            }
            false
        } else super.touchDown(screenX, screenY, pointer, button)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return gameTime <= 0
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        spawnTimer = System.currentTimeMillis()
    }

    /**
     * Generation of current goal number to achieve by tapping the bubbles.
     */
    private fun generateTargetNumber() {
        targetNumber = 0
        //maximal possible number or minimal possible
        val tmplist = activeBubbles.shuffled()
        val tmpCeilNum = Random.nextInt(BubblesProperty.MIN_NUMBER + 9, BubblesProperty.MAX_NUMBER + 1)
        if (Random.nextBoolean()) {
            for (i in tmplist.indices) {
                targetNumber += tmplist[i].number
                if (targetNumber > tmpCeilNum) {
                    targetNumber -= tmplist[i].number
                    return
                }
            }
        } else {
            for (i in tmplist.indices) {
                targetNumber += tmplist[i].number
                if (targetNumber > BubblesProperty.MIN_NUMBER) {
                    return
                }
            }
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))
        camera = OrthographicCamera(BubblesProperty.WORLD_WIDTH, BubblesProperty.WORLD_HEIGHT)

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontHuge")
        ls.fontColor = Color.BLACK

        targetNumber = 0
        currentNumber = 0

        gameTime = if (!customGame) BubblesProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)
        targetNumberLabel = Label("$targetNumber", ls)
        currentNumberLabel = Label("$currentNumber", ls)

        world.gravity = Vector2(0f, -10f)

        spawnTimer = System.currentTimeMillis()
        activeBubbles.clear()
        feedbackTable.clear()
        feedbackTable.setFillParent(true)
        feedbackTable.touchable = Touchable.disabled

        createObjects()
        generateTargetNumber()

        targetNumberLabel.setText(targetNumber)
        parent.stage.addActor(feedbackTable)
    }

    /** Creates physical game objects and poolable for bubbles */
    private fun createObjects() {
        createWalls()
        createFloor()
        createSky()

        bubblesPool = object : Pool<Bubble>() {
            override fun newObject(): Bubble? {
                return Bubble(world, textures!!, bel!!)
            }
        }

        spawnInitialBubbles()
    }

    /**
     * Spawns first bubbles before game start.
     */
    private fun spawnInitialBubbles() {
        for (i in 0 until 5 + difficulty) {
            val bubble = bubblesPool.obtain()
            bubble.init(difficulty, 1)
            activeBubbles.add(bubble)
        }
    }

    /** Creates impenetrable static bodies on the sides */
    private fun createWalls() {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.StaticBody

        val shape = PolygonShape()
        shape.setAsBox(1f, BubblesProperty.WORLD_HEIGHT / 2)

        val fixtureDef = FixtureDef()
        fixtureDef.friction = 0.5f
        fixtureDef.shape = shape

        val rightWall: Body = world.createBody(bodyDef)
        rightWall.createFixture(fixtureDef)
        rightWall.setTransform(BubblesProperty.WORLD_WIDTH / 2 + 1f, 0f, 0f)

        val leftWall: Body = world.createBody(bodyDef)
        leftWall.createFixture(fixtureDef)
        leftWall.setTransform(-BubblesProperty.WORLD_WIDTH / 2 - 1f, 0f, 0f)
    }

    /** Creates impenetrable static body as the ground with bottom graphics and UI */
    private fun createFloor() {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.StaticBody

        val shape = PolygonShape()
        shape.setAsBox(BubblesProperty.WORLD_WIDTH / 2, BubblesProperty.WORLD_GROUND_SIZE / 2)

        val fixtureDef = FixtureDef()
        fixtureDef.friction = 0.5f
        fixtureDef.shape = shape
        shape.dispose()

        val ground = world.createBody(bodyDef)
        ground?.createFixture(fixtureDef)
        ground?.setTransform(0f, -BubblesProperty.WORLD_HEIGHT / 2 + BubblesProperty.WORLD_GROUND_SIZE / 2, 0f)

        val bgPixmap = Pixmap(1, 1, Pixmap.Format.RGB565)
        bgPixmap.setColor(BubblesProperty.COLOR_UNPICKED)
        bgPixmap.fill()

        groundTexture = Sprite(Texture(bgPixmap))
        groundTexture?.setSize(BubblesProperty.WORLD_WIDTH, BubblesProperty.WORLD_GROUND_SIZE)
        groundTexture?.setPosition(-BubblesProperty.WORLD_WIDTH / 2, -BubblesProperty.WORLD_HEIGHT / 2)

        bgPixmap.setColor(BubblesProperty.COLOR_SPLITTER)
        bgPixmap.fill()
        val splitter = Table()
        splitter.background = Image(Texture(bgPixmap)).drawable
        bgPixmap.dispose()

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontMini")
        ls.fontColor = BubblesProperty.COLOR_SPLITTER

        bottomTable.clear()
        bottomTable.debug = parent.stage.isDebugAll
        bottomTable.add(targetNumberLabel).expandX().uniform()
        bottomTable.add(splitter).size(Value.percentWidth(0.005f, bottomTable), Value.percentHeight(1f, bottomTable))
        bottomTable.add(currentNumberLabel).expandX().uniform()
        bottomTable.row()
        bottomTable.add(Label(parent.locale?.get("bu_target"), ls)).expandX().colspan(2).padTop(-20f)
        bottomTable.add(Label(parent.locale?.get("bu_current"), ls)).expandX().padTop(-20f)

        //filler
        feedbackTable.add(Table()).expand()
        feedbackTable.row()
        feedbackTable.add(bottomTable).size(parent.stage.width, parent.stage.height * BubblesProperty.WORLD_GROUND_SIZE / BubblesProperty.WORLD_HEIGHT).growX()
    }

    /** Shows limiting sky line after which the game difficulty lowers */
    private fun createSky() {
        val bgPixmap = Pixmap(1, 1, Pixmap.Format.RGB565)
        bgPixmap.setColor(BubblesProperty.COLOR_SKY_LIMIT)
        bgPixmap.fill()

        skyTexture = Sprite(Texture(bgPixmap))
        bgPixmap.dispose()
        skyTexture?.setSize(BubblesProperty.WORLD_WIDTH, 4 * BubblesProperty.WORLD_HEIGHT / Gdx.graphics.height)
        skyTexture?.setPosition(-BubblesProperty.WORLD_WIDTH / 2, BubblesProperty.WORLD_HEIGHT / 2 - BubblesProperty.BUBBLE_SIZE - BubblesProperty.BUBBLE_SIZE * 0.5f * (GameConfig.GAME_DIFFICULTIES.last() - difficulty))
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }
}

/**
 * Poolable class defining and generating bubbles spawned in game. When bubble is removed by
 * achieving goal number, memory do not have to be freed as this can be reused by another new
 * active bubble.
 * @param world Box2D world
 * @param textures element textures
 * @param bel physical body editor class, for loading it
 */
private class Bubble(private var world: World, private var textures: TextureAtlas, private var bel: BodyEditorLoader) : Pool.Poolable {
    var bubble: PhysicalObjectModel? = null
    var alive: Boolean = false
    var overLimit: Boolean = false
    var number = 0

    /**
     * Generation of new bubble spawned and randomly choosen position under sky limit with given
     * physical properties.
     * @param difficulty current difficulty
     * @param initBubbles boolean substitution (0, 1 are allowed only)
     */
    fun init(difficulty: Int, initBubbles: Int = 0) {
        val bodyDef = BodyDef()
        bodyDef.type = BodyDef.BodyType.DynamicBody
        val fixtureDef = FixtureDef()
        fixtureDef.friction = 0.01f
        fixtureDef.density = 0.5f
        fixtureDef.restitution = 0.3f

        number = Random.nextInt(9) + 1

        val body: Body = world.createBody(bodyDef)
        val sprite = textures.createSprite(BubblesProperty.BUBBLE.plus(number))
        sprite.setOriginCenter()

        bel.attachFixture(body, BubblesProperty.BUBBLE, fixtureDef, BubblesProperty.BUBBLE_SIZE)
        body.fixtureList.first().userData = sprite
        body.userData = "bubble"

        bubble = PhysicalObjectModel(body, BubblesProperty.BUBBLE_SIZE)

        val bodyArray: Array<Body> = Array()
        world.getBodies(bodyArray)
        var x: Float
        var y: Float
        val deadlock = System.currentTimeMillis()
        do {
            var boxColliding = false
            x = Random.nextFloat() * (BubblesProperty.WORLD_WIDTH - BubblesProperty.BUBBLE_SIZE) - (BubblesProperty.WORLD_WIDTH - BubblesProperty.BUBBLE_SIZE) / 2
            y = BubblesProperty.WORLD_HEIGHT / 2 - BubblesProperty.BUBBLE_SIZE - BubblesProperty.BUBBLE_SIZE * 0.5f * (GameConfig.GAME_DIFFICULTIES.last() - difficulty) - initBubbles * Random.nextFloat() * (BubblesProperty.WORLD_HEIGHT - BubblesProperty.WORLD_GROUND_SIZE - 2 * BubblesProperty.BUBBLE_SIZE - BubblesProperty.BUBBLE_SIZE * 0.5f * (GameConfig.GAME_DIFFICULTIES.last() - difficulty))
            bodyArray.forEach {
                if (BubblesProperty.BUBBLE_SIZE * sqrt(2f) > sqrt((it.position.x - x).pow(2) + (it.position.y - y).pow(2))) boxColliding = true
            }
            if ((System.currentTimeMillis() - deadlock) / 1000f > 0.1f) boxColliding = false
        } while (boxColliding)
        bubble!!.body.setTransform(x, y, 0f)

        alive = true
    }

    /**
     * This method is inherited. Description is in parent class
     * @see Pool.reset()
     */
    override fun reset() {
        alive = false
        overLimit = false

        world.destroyBody(bubble!!.body)
        bubble = null
    }

    /**
     * Checks if bubble position is over sky limit
     */
    fun update(difficulty: Int) {
        if (bubble?.body?.position?.y!! > BubblesProperty.WORLD_HEIGHT / 2 - BubblesProperty.BUBBLE_SIZE - BubblesProperty.BUBBLE_SIZE * 0.5f * (GameConfig.GAME_DIFFICULTIES.last() - difficulty)) {
            overLimit = true
        }
    }

    override fun toString(): String {
        return "$number"
    }
}