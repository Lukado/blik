package lukado.blik.screens.games

import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameType
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.BoxFinderProperty
import kotlin.math.round
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Box Finder game
 */
class BoxFinder(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = BoxFinderProperty.LOCALE_NAME
        override val backgroundImagePath: String = BoxFinderProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = BoxFinderProperty.DIFFICULTY
        override val topScoresPreference: String = BoxFinderProperty.TOP_SCORE
        override val weekScoresPreference: String = BoxFinderProperty.WEEK_SCORE
        override val timesPlayedPreference: String = BoxFinderProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = BoxFinderProperty.FIRST_TIME
        override fun getClassInstance(): KClass<BoxFinder> {
            return BoxFinder::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("bf_game_description")
    override val gameType: EGameType = EGameType.MEMORY
    override val gameDuration: Float = BoxFinderProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null

    private var matrixSize: Int = 0
    private var guessedNumber: Int = 0
    private var difficultyCounter: Int = 0
    private var difficultyHalf: Int = 0

    private var firstTime: Boolean = true
    private var isShowing: Boolean = false
    private var gameEnd: Boolean = false

    private var gameTime = BoxFinderProperty.GAME_TIME

    //Game actors
    private var gjImage: Image? = null
    private var matrixMemory = emptyArray<Int>()
    private var gameTable = Table()
    private val feedbackTable = Table()

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)
        if (firstTime && gameTime <= currentGameDuration) {
            BoxFinderProperty.SHOW_TIME.showMatrix(showBoxes = true, fail = false)
        } else if (!firstTime && !isShowing && !gameTable.isTouchable) {
            isShowing = true
            gameTable.children.forEach {
                if (it is Image) {
                    it.addAction(Actions.sequence(
                            Actions.color(BoxFinderProperty.COLOR_DEFAULT, 0.2f),
                            Actions.run {
                                if (it == gameTable.children.last()) {
                                    isShowing = false
                                    gameTable.touchable = Touchable.enabled
                                }
                            }))
                }
            }

        }

        gameTime -= delta
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {}

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return if (gameTime <= 0) {
            if (gameEnd) {
                true
            } else {
                gameTime = 0f
                false
            }
        } else false
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        if (gameTable.isTouchable) {
            gameTable.touchable = Touchable.disabled
        }
    }

    /** Method to pause game for showing animations and changing difficulties. Shows correct or
     * wrong choices.
     * @param showBoxes fade in new turn
     * @param fail user missed
     */
    private fun Float.showMatrix(showBoxes: Boolean, fail: Boolean) {
        isShowing = true
        firstTime = false

        if (showBoxes) {
            matrixMemory.forEach {
                gameTable.addAction(Actions.sequence(Actions.fadeIn(0.5f, Interpolation.smoother), Actions.run {
                    (gameTable.getChild(1 + it) as Image).addAction(Actions.sequence(
                            Actions.scaleTo(0f, 0f, this / 8, Interpolation.smoother),
                            Actions.color(BoxFinderProperty.COLOR_GOOD),
                            Actions.scaleTo(1f, 1f, this / 8, Interpolation.smoother),
                            Actions.delay(this / 1.5f),
                            Actions.scaleTo(0f, 0f, this / 8, Interpolation.smoother),
                            Actions.color(BoxFinderProperty.COLOR_DISABLED),
                            Actions.scaleTo(1f, 1f, this / 8, Interpolation.smoother),
                            Actions.run {
                                isShowing = false
                            }))
                }))
            }
        } else {
            if (fail) {
                matrixMemory.forEach {
                    (gameTable.getChild(1 + it) as Image).addAction(Actions.sequence(
                            Actions.color(BoxFinderProperty.COLOR_GOOD, this / 3, Interpolation.smoother),
                            Actions.delay(this / 3),
                            Actions.run {
                                gameTable.addAction(Actions.sequence(Actions.fadeOut(0.5f, Interpolation.smoother), Actions.run {
                                    // goto: val maxObjectsPerRow = 6
                                    val prevHalf: Int = if (difficulty > 6) {
                                        round((7 * 7 + (matrixSize - 7 - 1) * 7) / 2f).toInt()
                                    } else {
                                        round(((matrixSize - 1) * (matrixSize - 1)) / 2f).toInt()
                                    }

                                    if (prevHalf >= difficultyCounter && difficulty > GameConfig.GAME_DIFFICULTIES[0]) {
                                        difficultyDown()
                                    } else {
                                        if (difficultyCounter > 1) difficultyCounter--
                                    }
                                    if (gameTime <= 0) gameEnd = true
                                    if (!gameEnd) createGameObjects()
                                }))
                            }))
                }
            } else {
                guessedNumber = 0
                feedbackTable.addAction(Actions.sequence(Actions.delay(1f), Actions.parallel(
                        Actions.run {
                            gameTable.addAction(Actions.sequence(Actions.fadeOut(0.5f, Interpolation.smoother), Actions.run {
                                if (difficultyHalf <= difficultyCounter && difficulty < GameConfig.GAME_DIFFICULTIES.size) {
                                    difficultyUp()
                                } else {
                                    if (difficultyCounter < difficultyHalf) difficultyCounter++
                                }
                                gjImage?.rotation = -20f

                                if (gameTime <= 0) gameEnd = true
                                if (!gameEnd) createGameObjects()
                            }))
                        },
                        Actions.fadeOut(0.5f, Interpolation.smoother))
                ))
            }
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))

        gameTime = if (!customGame) BoxFinderProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)
        infoLabel?.setAlignment(Align.top)
        infoContainer?.padTop(20f)

        gjImage = Image(textures?.findRegion(BoxFinderProperty.GOOD_JOB))
        gjImage?.setOrigin(Align.bottomRight)
        gjImage?.rotation = -20f
        feedbackTable.clear()
        feedbackTable.setFillParent(true)
        feedbackTable.touchable = Touchable.disabled
        feedbackTable.add(gjImage)
        feedbackTable.color.a = 0f

        matrixSize = 0
        guessedNumber = 0
        difficultyCounter = 0
        difficultyHalf = 0
        firstTime = true
        isShowing = false
        gameEnd = false

        createGameObjects()
        parent.stage.addActor(gameTable)
        parent.stage.addActor(feedbackTable)
    }

    /**
     * Generates grid of boxes and adds listeners for each box, that contain main game logic. Also
     * computes properties for dynamic difficulty changing.
     */
    private fun createGameObjects() {
        gameTable.clear()
        gameTable.setFillParent(true)

        //filler to center table
        gameTable.add(Label("", parent.skin))
        var highDifficultyObjectStopper = 0
        val maxObjectsPerRow = 6 //+1 cuz im lazy to refractor this
        guessedNumber = 0
        matrixSize = difficulty + 1

        //Game logic is in the click listener for each image
        for (i in 0 until matrixSize * matrixSize) {
            val image = Image(textures?.findRegion(BoxFinderProperty.BOX_HIDDEN))
            image.color = BoxFinderProperty.COLOR_DISABLED
            image.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    isShowing = true
                    if (matrixMemory.contains(i)) {
                        if (image.hasActions() || image.color == BoxFinderProperty.COLOR_GOOD) return

                        guessedNumber++
                        score += difficulty + guessedNumber + difficultyCounter
                        debugLabel?.setText("D=$difficulty : S=$score")

                        if (guessedNumber == matrixMemory.size) {
                            gameTable.touchable = Touchable.disabled
                            feedbackTable.addAction(Actions.parallel(
                                    Actions.fadeIn(0.5f, Interpolation.smoother),
                                    Actions.run { gjImage?.addAction(Actions.rotateBy(20f, 2f, Interpolation.elasticOut)) }
                            ))
                        }

                        image.addAction(Actions.sequence(
                                Actions.scaleTo(0f, 0f, BoxFinderProperty.SHOW_TIME / 8, Interpolation.smoother),
                                Actions.color(BoxFinderProperty.COLOR_GOOD),
                                Actions.scaleTo(1f, 1f, BoxFinderProperty.SHOW_TIME / 8, Interpolation.smoother),
                                Actions.run {
                                    if (guessedNumber == matrixMemory.size) {
                                        BoxFinderProperty.SHOW_TIME.showMatrix(showBoxes = false, fail = false)
                                    }
                                }
                        ))
                    } else {
                        if (feedbackTable.color.a > 0) return
                        image.color = BoxFinderProperty.COLOR_FAIL
                        gameTable.touchable = Touchable.disabled
                        BoxFinderProperty.SHOW_TIME.showMatrix(showBoxes = false, fail = true)
                    }
                }
            })

            var spacing = BoxFinderProperty.DEFAULT_BOX_SPACING / difficulty
            var sizing = (parent.stage.width - (matrixSize + 1) * spacing) / matrixSize
            if (difficulty > maxObjectsPerRow) {
                if (i % (maxObjectsPerRow + 1) == 0) {
                    gameTable.row()
                    if (++highDifficultyObjectStopper == matrixSize + 1) break
                }
                spacing = BoxFinderProperty.DEFAULT_BOX_SPACING / maxObjectsPerRow
                sizing = (parent.stage.width - (maxObjectsPerRow + 2) * spacing) / (maxObjectsPerRow + 1)
            } else if (i % matrixSize == 0) {
                gameTable.row()
            }

            image.setOrigin(sizing / 2, sizing / 2)

            gameTable.add(image).fillX().space(0f, spacing, spacing, spacing).size(sizing)
        }

        difficultyHalf = round((gameTable.cells.size - 1) / 2f).toInt()
        if (difficultyCounter == 0) difficultyCounter = difficultyHalf - round(difficulty / 2f).toInt()
        matrixMemory = (0 until gameTable.cells.size - 1).shuffled().take(difficultyCounter).toTypedArray()

        debugLabel?.setText("D=$difficulty : S=$score")
        gameTable.touchable = Touchable.disabled
        if (!firstTime) BoxFinderProperty.SHOW_TIME.showMatrix(showBoxes = true, fail = false)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
     override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }
}