package lukado.blik.screens.games

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.utils.Align
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameState
import lukado.blik.enums.EGameType
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.FollowOwlProperty
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Follow Owl game
 */
class FollowOwl(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = FollowOwlProperty.LOCALE_NAME
        override val backgroundImagePath: String = FollowOwlProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = FollowOwlProperty.DIFFICULTY
        override val topScoresPreference: String = FollowOwlProperty.TOP_SCORE
        override val weekScoresPreference: String = FollowOwlProperty.WEEK_SCORE
        override val timesPlayedPreference: String = FollowOwlProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = FollowOwlProperty.FIRST_TIME
        override fun getClassInstance(): KClass<FollowOwl> {
            return FollowOwl::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("fo_game_description")
    override val gameType: EGameType = EGameType.VISUAL_SPATIAL
    override val gameDuration: Float = FollowOwlProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null

    private var difficultyCounter: Int = parent.preferences.getInteger(FollowOwlProperty.DIFFICULTY_COUNTER, 0)
    private var tappedPosition: Int = -1

    private var gameTime: Float = FollowOwlProperty.GAME_TIME
    private var guessedNumber: Int = 0

    private var firstTime: Boolean = true
    private var isShowing: Boolean = false
    private var gameEnd: Boolean = false
    private var touchable = false

    //Game actors
    private var gjImage: Image? = null
    private val feedbackTable: Table = Table()

    private var owlPositions: ArrayList<Int> = ArrayList()
    private var boxes: ArrayList<Image> = ArrayList()

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)
        if (firstTime && gameTime <= currentGameDuration) {
            FollowOwlProperty.SHOW_TIME.showMatrix(showMovement = true, fail = false)
        } else if (!firstTime && !isShowing && !touchable) {
            isShowing = true
            boxes.forEach {
                it.addAction(Actions.sequence(
                        Actions.color(FollowOwlProperty.COLOR_DEFAULT, FollowOwlProperty.SHOW_TIME / 10f),
                        Actions.run {
                            if (it == boxes.last()) {
                                touchable = true
                                isShowing = false
                            }
                        }))
            }
        }

        gameTime -= delta
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {
        boxes.forEach {
            if (state == EGameState.RUNNING) it.act(Gdx.graphics.deltaTime)
            it.draw(sb, 1f)
        }
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return if (gameTime <= 0) {
            if (gameEnd) {
                if (!customGame) {
                    parent.preferences.putInteger(FollowOwlProperty.DIFFICULTY_COUNTER, difficultyCounter)
                    parent.preferences.flush()
                }
                true
            } else {
                gameTime = 0f
                false
            }
        } else false
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        if (touchable) {
            touchable = false
        }
    }

    /**
     * Method to pause game for showing animations. Shows correct or wrong guesses and starts new
     * turn.
     * @param showMovement show owl movement
     * @param fail user missed
     */
    private fun Float.showMatrix(showMovement: Boolean, fail: Boolean) {
        isShowing = true
        firstTime = false

        if (showMovement) {
            boxes.forEach {
                it.addAction(Actions.sequence(
                        Actions.fadeIn(0.5f, Interpolation.smoother),
                        Actions.delay(this / 2f),
                        Actions.run {
                            if (it == boxes.last()) {
                                moveOwl(0)
                            }
                        }))
            }
        } else {
            if (fail) {
                boxes[tappedPosition].addAction(Actions.sequence(
                        Actions.color(FollowOwlProperty.COLOR_FAIL),
                        Actions.delay(this / 3),
                        Actions.run {
                            boxes.forEach {
                                it.addAction(Actions.sequence(
                                        Actions.fadeOut(0.5f, Interpolation.smoother),
                                        Actions.run {
                                            if (it == boxes.last()) {
                                                difficultyCounter.minus(2)

                                                if (difficultyCounter <= -3) {
                                                    difficultyCounter = 0
                                                    difficultyDown()
                                                }
                                                guessedNumber = 0
                                                if (gameTime <= 0) gameEnd = true
                                                if (!gameEnd) createGameObjects()
                                            }
                                        }))
                            }
                        }))
            } else {
                guessedNumber++
                score += difficulty * 5 + guessedNumber * 10
                debugLabel?.setText("D=$difficulty : S=$score : DC=$difficultyCounter")

                boxes[tappedPosition].addAction(Actions.sequence(
                        Actions.color(FollowOwlProperty.COLOR_GOOD),
                        Actions.color(FollowOwlProperty.COLOR_DEFAULT, FollowOwlProperty.SHOW_TIME / 3)
                ))

                if (guessedNumber == owlPositions.size) {
                    touchable = false
                    feedbackTable.addAction(Actions.parallel(
                            Actions.fadeIn(0.5f, Interpolation.smoother),
                            Actions.run { gjImage?.addAction(Actions.rotateBy(20f, 2f, Interpolation.elasticOut)) }
                    ))

                    boxes[tappedPosition].addAction(Actions.sequence(
                            Actions.run {
                                difficultyCounter++
                                if (difficultyCounter >= difficulty) {
                                    difficultyUp()
                                    difficultyCounter = 0
                                }

                                feedbackTable.addAction(Actions.sequence(Actions.delay(1f), Actions.parallel(
                                        Actions.run {
                                            boxes.forEach {
                                                it.addAction(Actions.sequence(
                                                        Actions.fadeOut(0.5f, Interpolation.smoother),
                                                        Actions.run {
                                                            if (it == boxes.last()) {
                                                                if (gameTime <= 0) gameEnd = true
                                                                if (!gameEnd) createGameObjects()
                                                            }
                                                        }
                                                ))
                                            }
                                        },
                                        Actions.fadeOut(0.5f, Interpolation.smoother))
                                ))
                            }
                    ))
                }
            }
        }
    }

    /** Method for showing animation of owl movement. */
    private fun moveOwl(positionNumber: Int) {
        val currImg = boxes[owlPositions[positionNumber]]
        currImg.addAction(Actions.sequence(
                Actions.run {
                    currImg.color = FollowOwlProperty.COLOR_DEFAULT
                    currImg.drawable = Image(textures?.findRegion(FollowOwlProperty.OWL)).drawable
                },
                Actions.delay(0.9f - difficulty / 15f),
                Actions.run {
                    currImg.color = FollowOwlProperty.COLOR_DISABLED
                    currImg.drawable = Image(textures?.findRegion(FollowOwlProperty.BOX)).drawable
                },
                Actions.delay(0.9f - difficulty / 15f),
                Actions.run {
                    if (positionNumber == owlPositions.size - 1) isShowing = false
                    else moveOwl(positionNumber + 1)
                }
        ))
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))

        gameTime = if (!customGame) FollowOwlProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)
        infoLabel?.setAlignment(Align.top)
        infoContainer?.padTop(20f)

        gjImage = Image(textures?.findRegion(FollowOwlProperty.GOOD_JOB))
        gjImage?.setOrigin(Align.bottomRight)

        feedbackTable.clear()
        feedbackTable.setFillParent(true)
        feedbackTable.touchable = Touchable.disabled
        //filler to center response table
        feedbackTable.add(Label("", parent.skin)).row()
        feedbackTable.add(gjImage).expand()
        feedbackTable.color.a = 0f

        difficultyCounter = parent.preferences.getInteger(FollowOwlProperty.DIFFICULTY_COUNTER, 0)
        tappedPosition = -1
        guessedNumber = 0
        firstTime = true
        isShowing = false
        gameEnd = false
        touchable = false
        owlPositions.clear()
        boxes.clear()

        createGameObjects()
        parent.stage.addActor(feedbackTable)
    }

    /** Creates game defining randomly positioned boxes, where owl can move. */
    private fun createGameObjects() {
        boxes.clear()
        guessedNumber = 0
        gjImage?.rotation = -20f

        //Game logic is in the click listener for each image
        for (i in 0 until FollowOwlProperty.MAXIMUM_BOXES) {
            val image = Image(textures?.findRegion(FollowOwlProperty.BOX))
            image.color = FollowOwlProperty.COLOR_DISABLED
            if (!firstTime) image.color.a = 0f
            image.setSize(FollowOwlProperty.DEFAULT_BOX_SIZE, FollowOwlProperty.DEFAULT_BOX_SIZE)

            var x: Float
            var y: Float
            do {
                var boxColliding = false
                x = Random.nextInt((camera.viewportWidth - FollowOwlProperty.DEFAULT_BOX_SIZE).toInt()).toFloat()
                y = Random.nextInt((camera.viewportHeight - FollowOwlProperty.DEFAULT_BOX_SIZE - 2 * topRightLabel!!.height).toInt()).toFloat()
                boxes.forEach {
                    if (FollowOwlProperty.DEFAULT_BOX_SIZE * sqrt(2f) > sqrt((it.x - x).pow(2) + (it.y - y).pow(2))) boxColliding = true
                }
            } while (boxColliding)
            image.setPosition(x, y)

            boxes.add(image)
        }

        owlPositions = ArrayList()
        while (owlPositions.size != difficulty) {
            val pos = Random.nextInt(FollowOwlProperty.MAXIMUM_BOXES)
            if (owlPositions.isEmpty() || owlPositions.last() != pos) owlPositions.add(pos)
        }

        debugLabel?.setText("D=$difficulty : S=$score : DC=$difficultyCounter")
        touchable = false
        if (!firstTime) FollowOwlProperty.SHOW_TIME.showMatrix(showMovement = true, fail = false)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }

    /**
     * Calculates touch position and tells if user hit the box. Calls other methods based on box hits.
     *
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.touchDown()
     */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        if (touchable) {
            val coords = camera.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0f))
            boxes.forEachIndexed { i, it ->
                if (it.hit(coords.x - it.x, coords.y - it.y, true) != null) {
                    tappedPosition = i
                    if (owlPositions[guessedNumber] == i) {
                        FollowOwlProperty.SHOW_TIME.showMatrix(showMovement = false, fail = false)
                    } else {
                        touchable = false
                        FollowOwlProperty.SHOW_TIME.showMatrix(showMovement = false, fail = true)
                    }
                    return@forEachIndexed
                }
            }
        }
        return super.touchDown(screenX, screenY, pointer, button)
    }
}