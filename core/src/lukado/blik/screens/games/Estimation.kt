package lukado.blik.screens.games

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Value
import com.badlogic.gdx.utils.Align
import lukado.blik.Blik
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameState
import lukado.blik.enums.EGameType
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import lukado.blik.screens.games.properties.EstimationProperty
import kotlin.math.roundToInt
import kotlin.random.Random
import kotlin.reflect.KClass

/**
 * Logic, graphics rendering, physics controlling for Estimation game
 */
class Estimation(private val parent: Blik, description: Boolean) : BlikGameScreen(parent, description, Info) {
    //Class companion
    companion object Info : IGameCompanion {
        override val localeName: String = EstimationProperty.LOCALE_NAME
        override val backgroundImagePath: String = EstimationProperty.THUMBNAIL_NAME
        override val currentDifficultyPreference: String = EstimationProperty.DIFFICULTY
        override val topScoresPreference: String = EstimationProperty.TOP_SCORE
        override val weekScoresPreference: String = EstimationProperty.WEEK_SCORE
        override val timesPlayedPreference: String = EstimationProperty.TIMES_PLAYED
        override val isFirstTimePreference: String = EstimationProperty.FIRST_TIME
        override fun getClassInstance(): KClass<Estimation> {
            return Estimation::class
        }
    }

    //Interfaces
    override val gameThumbnail: Image = Image(thumbnails.findRegion(backgroundImagePath))
    override val gameDescription: String = parent.locale!!.get("es_game_description")
    override val gameType: EGameType = EGameType.EXECUTIVE
    override val gameDuration: Float = EstimationProperty.GAME_TIME

    //Class unique
    private var textures: TextureAtlas? = null
    private var pointsInRow: Int = parent.preferences.getInteger(EstimationProperty.DIFFICULTY_COUNTER)
    private var currDecimals: Int = 0
    private var gameTime: Float = EstimationProperty.GAME_TIME
    private var timeTaken: Long = System.currentTimeMillis()
    private var pausedTime: Long = 0L

    private var lineScaleFactor: Float = (EstimationProperty.WORLD_HEIGHT - EstimationProperty.WORLD_HEIGHT * 0.1f) / EstimationProperty.MAX_AXIS_NUMBER
    private var lineY: Float = -EstimationProperty.WORLD_HEIGHT
    private var result: Double = 0.0
    private var gameEnd: Boolean = false
    private var isAnimation: Boolean = false

    //Game actors
    private val gameTable: Table = Table()
    private val resultTable: Table = Table()
    private val feedbackTable: Table = Table()
    private val axisImages: ArrayList<Image> = ArrayList()
    private val generatedNumbers: ArrayList<Double> = ArrayList()
    private var lineTexture: Texture? = null
    private var preciseLineTexture: Texture? = null
    private var dotTexture: Texture? = null

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.doLogicStep()
     */
    override fun doLogicStep(delta: Float) {
        gameTime = gameReadyCountdown(gameTime, currentGameDuration)

        if (!gameTable.isVisible && gameTime < currentGameDuration && !isAnimation) gameTable.isVisible = true
        else if (isAnimation && !resultTable.isVisible) resultTable.isVisible = true

        if (pausedTime != 0L) {
            timeTaken += System.currentTimeMillis() - pausedTime
            pausedTime = 0L
        }
        gameTime -= delta
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.redrawSprites()
     */
    override fun redrawSprites() {
        //Axis display
        axisImages.forEachIndexed { index, it ->
            it.draw(sb, 1f)
            if ((index + 1) % 10 == 0) {
                val font = parent.skin?.getFont("osifontMini")
                val gl = GlyphLayout(font, "${index + 1}")
                font?.color = Color.DARK_GRAY
                font?.draw(sb, gl, it.x - gl.width * 1.3f, it.y + gl.height + it.height / 8f)
            }
        }

        //line
        if (!isAnimation) sb.draw(lineTexture, -EstimationProperty.WORLD_WIDTH / 4f - 0.03f * EstimationProperty.WORLD_HEIGHT / 2f, lineY - 2f * lineScaleFactor + (difficulty / 7f) * lineScaleFactor, EstimationProperty.WORLD_WIDTH, 4f * lineScaleFactor - 2f * (difficulty / 7f) * lineScaleFactor)
        sb.draw(lineTexture, -EstimationProperty.WORLD_WIDTH / 4f - 0.03f * EstimationProperty.WORLD_HEIGHT / 2f, lineY - 2f * lineScaleFactor + (difficulty / 7f) * lineScaleFactor, 0.03f * EstimationProperty.WORLD_HEIGHT, 4f * lineScaleFactor - 2f * (difficulty / 7f) * lineScaleFactor)
        if (isAnimation) sb.draw(preciseLineTexture, -EstimationProperty.WORLD_WIDTH / 4f - 0.5f * lineScaleFactor + 0.25f * (difficulty / 7f) * lineScaleFactor, result.toFloat() * lineScaleFactor - EstimationProperty.WORLD_HEIGHT / 2 - 0.5f * lineScaleFactor + 0.25f * (difficulty / 7f) * lineScaleFactor, lineScaleFactor - 0.5f * (difficulty / 7f) * lineScaleFactor, lineScaleFactor - 0.5f * (difficulty / 7f) * lineScaleFactor)
        sb.draw(dotTexture, -EstimationProperty.WORLD_WIDTH / 4f - 1f, lineY - 1f, 2f, 2f)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.isGameEnd()
     */
    override fun isGameEnd(): Boolean {
        return if (gameTime < 0) {
            if (gameEnd) {
                if (!customGame) {
                    parent.preferences.putInteger(EstimationProperty.DIFFICULTY_COUNTER, pointsInRow)
                    parent.preferences.flush()
                }
                true
            } else {
                gameTime = 0f
                false
            }
        } else false
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.pausedGameStep()
     */
    override fun pausedGameStep() {
        if (gameTable.isVisible) gameTable.isVisible = false
        if (resultTable.isVisible) resultTable.isVisible = false
        if (pausedTime == 0L) pausedTime = System.currentTimeMillis()
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.createGame()
     */
    override fun createGame() {
        textures = parent.manager.loadGameAssets(getGameAssetPath(localeName))
        camera = OrthographicCamera(EstimationProperty.WORLD_WIDTH, EstimationProperty.WORLD_HEIGHT)

        gameTime = if (!customGame) EstimationProperty.GAME_TIME + GameConfig.COUNTDOWN_TIME
        else customTime + GameConfig.COUNTDOWN_TIME
        currentGameDuration = gameTime - GameConfig.COUNTDOWN_TIME

        pointsInRow = parent.preferences.getInteger(EstimationProperty.DIFFICULTY_COUNTER)
        lineY = -EstimationProperty.WORLD_HEIGHT
        result = 0.0
        gameEnd = false
        isAnimation = false
        pausedTime = 0L

        topRightLabel = Label("%.1f".format(currentGameDuration), parent.skin)
        gameTable.setFillParent(true)
        gameTable.isVisible = false

        var bgPixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
        bgPixmap.setColor(EstimationProperty.RECT_COLOR)
        bgPixmap.fill()
        lineTexture = Texture(bgPixmap)

        bgPixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
        val c = EstimationProperty.GOOD_COLOR
        c.a = 0.7f
        bgPixmap.setColor(c)
        bgPixmap.fill()
        preciseLineTexture = Texture(bgPixmap)

        bgPixmap = Pixmap(1, 1, Pixmap.Format.RGBA8888)
        bgPixmap.setColor(EstimationProperty.DOT_COLOR)
        bgPixmap.fill()
        dotTexture = Texture(bgPixmap)
        bgPixmap.dispose()

        resultTable.clear()
        feedbackTable.clear()
        feedbackTable.setFillParent(true)
        feedbackTable.touchable = Touchable.disabled
        feedbackTable.align(Align.left)
        feedbackTable.add(resultTable).width(Value.percentWidth(0.7f, gameTable)).row()

        createNumericalAxis()
        createNumberSection()
        parent.stage.addActor(gameTable)
        parent.stage.addActor(feedbackTable)
    }

    /**
     * Creates texture displayed as numerical axis on the screen left side
     */
    private fun createNumericalAxis() {
        axisImages.clear()

        var axisPoint: Image
        for (i in 1 until EstimationProperty.MAX_AXIS_NUMBER + 1) {
            val pointSize = when {
                i % 10 == 0 -> 0.025f * EstimationProperty.WORLD_HEIGHT
                i % 5 == 0 -> 0.015f * EstimationProperty.WORLD_HEIGHT
                else -> 0.005f * EstimationProperty.WORLD_HEIGHT
            }
            axisPoint = Image(textures?.createPatch(EstimationProperty.AXIS_BAR))
            axisPoint.setSize(pointSize, pointSize)
            axisPoint.setPosition(-EstimationProperty.WORLD_WIDTH / 4f - axisPoint.width / 2f, -EstimationProperty.WORLD_HEIGHT / 2f + i * lineScaleFactor - pointSize / 2f)

            axisImages.add(axisPoint)
        }
    }

    /**
     * Creates labels of generated numbers to sum on the screen right side
     */
    private fun createNumberSection() {
        gameTable.clear()
        generatedNumbers.clear()
        generateNumbers()

        val ls = Label.LabelStyle()
        ls.font = parent.skin?.getFont("osifontHuge")
        ls.fontColor = Color.BLACK

        generatedNumbers.forEach {
            val numberLabel = if (generatedNumbers.first() == it) Label("${it.round(currDecimals)}", ls) else Label("+${it.round(currDecimals)}", ls)
            gameTable.add(numberLabel).padLeft(Value.percentWidth(0.3f + Random.nextFloat() / 5, gameTable)).row()
        }

        timeTaken = System.currentTimeMillis()
    }

    /**
     * Generates numbers to sum based on difficulty
     */
    private fun generateNumbers() {
        result = 0.0
        val maxNums = (difficulty / (GameConfig.GAME_DIFFICULTIES.size / 6f)).roundToInt()
        currDecimals = when (difficulty) {
            1 -> 0
            in 2..3 -> 1
            in 8..GameConfig.GAME_DIFFICULTIES.size -> 3
            else -> 2
        }

        for (i in 0 until 1 + maxNums) {
            generatedNumbers.add(Random.nextDouble(0.1, (EstimationProperty.MAX_AXIS_NUMBER / (1.toDouble() + maxNums))).round(currDecimals))
            result += generatedNumbers.last()
        }
        result = result.round(currDecimals)
        debugLabel?.setText("$result")
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.dispose()
     */
    override fun gameDispose() {
        textures = null
        if (parent.manager.am.isLoaded(getGameAssetPath(localeName))) parent.manager.am.unload(getGameAssetPath(localeName))
    }

    /**
     * Computes touched coordinates into previously created numerical axis as main game logic.
     * Shows response based on the precision of the sum to hit.
     *
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.touchUp()
     */
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return if (state == EGameState.RUNNING && gameTime < currentGameDuration && !isAnimation) {
            gameTable.isVisible = false
            feedbackTable.color.a = 1f
            isAnimation = true
            resultTable.clear()

            val bg = Image(textures?.findRegion(EstimationProperty.RESULT_BOX))
            val responseText: String
            val time = (System.currentTimeMillis() - timeTaken) / 1000f

            val projectedRange = (lineY + EstimationProperty.WORLD_HEIGHT / 2) / lineScaleFactor
            val difficultyRange = 2f - (difficulty / 7f)
            if (result in projectedRange - difficultyRange..projectedRange + difficultyRange) {
                pointsInRow++

                val preciseRange = 0.5f - 0.25f * (difficulty / 7f)
                score += if (result in projectedRange - preciseRange..projectedRange + preciseRange) {
                    bg.color = EstimationProperty.PERFECT_COLOR
                    responseText = parent.locale!!.get("es_perfect")
                    ((difficulty * 50 + pointsInRow * 40 + generatedNumbers.size * 30 + currDecimals * 20) / (time / 2f)).toInt()
                } else {
                    bg.color = EstimationProperty.GOOD_COLOR
                    responseText = parent.locale!!.get("es_good")
                    ((difficulty * 50 + pointsInRow * 40 + generatedNumbers.size * 30 + currDecimals * 20) / time).toInt()
                }

                if (pointsInRow > difficulty * 2f) {
                    difficultyUp()
                    pointsInRow = 0
                }
                debugLabel?.setText("D=$difficulty S=$score")
            } else {
                bg.color = EstimationProperty.FAIL_COLOR
                responseText = parent.locale!!.get("es_miss")
                if (pointsInRow <= 0) pointsInRow--
                else pointsInRow = 0

                if (pointsInRow < -2) {
                    difficultyDown()
                    pointsInRow = 0
                }
            }

            var ls = Label.LabelStyle()
            ls.font = parent.skin?.getFont("osifontMedium")
            ls.fontColor = bg.color
            resultTable.add(Label(responseText, ls)).padBottom(5f).row()
            ls = Label.LabelStyle()
            ls.font = parent.skin?.getFont("osifontSmall")
            ls.fontColor = Color.BLACK
            resultTable.add(Label(parent.locale!!.get("es_hit"), ls)).row()
            ls = Label.LabelStyle()
            ls.font = parent.skin?.getFont("osifontHuge")
            ls.fontColor = Color.BLACK
            resultTable.add(Label("%.2f".format((lineY + EstimationProperty.WORLD_HEIGHT / 2) / lineScaleFactor), ls))

            resultTable.background = bg.drawable
            feedbackTable.addAction(Actions.sequence(
                    Actions.moveToAligned(parent.stage.width, (lineY + EstimationProperty.WORLD_HEIGHT / 2f) * (parent.stage.height / EstimationProperty.WORLD_HEIGHT), Align.left),
                    Actions.moveToAligned(parent.stage.width / 3.5f, (lineY + EstimationProperty.WORLD_HEIGHT / 2f) * (parent.stage.height / EstimationProperty.WORLD_HEIGHT), Align.left, 0.5f, Interpolation.smoother),
                    Actions.delay(1f),
                    Actions.fadeOut(0.5f),
                    Actions.run {
                        if (gameTime <= 0) gameEnd = true
                        isAnimation = false
                        lineY = -EstimationProperty.WORLD_HEIGHT
                        createNumberSection()
                    }))
            false
        } else return super.touchUp(screenX, screenY, pointer, button)
    }

    /**
     * Renders line on dragging that shows where the user hits.
     *
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.touchDragged()
     */
    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return if (state == EGameState.RUNNING && !isAnimation) {
            if (gameTime < currentGameDuration) lineY = camera.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0f)).y
            if (lineY > axisImages.last().y + axisImages.last().height / 2f) lineY = axisImages.last().y + axisImages.last().height / 2f
            false
        } else return super.touchDragged(screenX, screenY, pointer)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see BlikGameScreen.touchDown()
     */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return if (state == EGameState.RUNNING && !isAnimation) {
            if (gameTime < currentGameDuration) lineY = camera.unproject(Vector3(screenX.toFloat(), screenY.toFloat(), 0f)).y
            if (lineY > axisImages.last().y + axisImages.last().height / 2f) lineY = axisImages.last().y + axisImages.last().height / 2f
            false
        } else return super.touchDown(screenX, screenY, pointer, button)
    }

    /** Formats numbers into given decimals
     *  @param decimals decimals number
     *  @return formatted number
     */
    private fun Double.round(decimals: Int = 2): Double = "%.${decimals}f".format(this).replace(",", ".").toDouble()
}