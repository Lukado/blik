package lukado.blik.screens.games.properties

import com.badlogic.gdx.graphics.Color

object DangerousPathProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "dangerous_path"
    const val LOCALE_NAME = "dangerous_path"

    /** Texture names */
    const val BOX_HIDDEN = "box"
    const val GOOD_JOB = "good"
    const val BOMB = "bomb2"

    /** Game preferences */
    const val TOP_SCORE = "dp.score"
    const val WEEK_SCORE = "dp.week"
    const val DIFFICULTY = "dp.diff"
    const val TIMES_PLAYED = "dp.played"
    const val FIRST_TIME = "dp.first.time"

    val COLOR_DEFAULT: Color = Color.WHITE
    val COLOR_DISABLED: Color = Color.valueOf("A9B1B1")
    val COLOR_PATH: Color = Color.valueOf("FDE74C")
    val COLOR_EDGE_POINT: Color = Color.YELLOW
    val COLOR_FAIL: Color = Color.RED

    /** Game constants */
    const val DEFAULT_BOX_SPACING = 20f
    const val SHOW_TIME = 3f

    /** Game difficulty constants */
    const val GAME_TIME = 60f
}