package lukado.blik.screens.games.properties

object MosquitoProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "mosquito2"
    const val LOCALE_NAME = "mosquito"

    /** Texture names */
    const val BOX = "box"
    const val MOSQUITO = "mosquito"
    const val GOOD_JOB = "good"
    const val ARROW = "green_arrow"

    /** Game preferences */
    const val TOP_SCORE = "mo.score"
    const val WEEK_SCORE = "mo.week"
    const val DIFFICULTY = "mo.diff"
    const val TIMES_PLAYED = "mo.played"
    const val FIRST_TIME = "mo.first.time"
    const val DIFFICULTY_COUNTER = "mo.diffCounter"

    /** Game constants */
    const val DEFAULT_BOX_SPACING = 20f
    const val SHOW_TIME = 3f

    /** Game difficulty constants */
    const val GAME_TIME = 60f
}