package lukado.blik.screens.games.properties

object FlyingFrogProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "flying_frog2"
    const val LOCALE_NAME = "flying_frog"

    /** Texture names */
    const val CHARACTER = "apu"
    const val BG_DAY = "bg_day"
    const val BG_NIGHT = "bg_night"
    const val GROUND_DAY = "ground_day"
    const val GROUND_NIGHT = "ground_night"
    const val GREEN_ARROW = "green_arrow"
    const val RED_ARROW = "red_arrow"
    const val TUBE = "tube"
    const val ANTI_TUBE = "anti_tube"

    /** Game preferences */
    const val TOP_SCORE = "ff.score"
    const val WEEK_SCORE = "ff.week"
    const val DIFFICULTY = "ff.diff"
    const val TIMES_PLAYED = "ff.played"
    const val FIRST_TIME = "ff.first.time"

    /** Game constants */
    const val WORLD_HEIGHT = 40f
    const val WORLD_WIDTH = WORLD_HEIGHT / 2f
    const val WORLD_GROUND_SIZE = WORLD_HEIGHT*0.05f
    const val TUBE_GAP_SIZE = WORLD_HEIGHT*0.1f


    /** Game difficulty constants */
    const val CHARACTER_SPEED = 3f //mul difficulty level
    const val TUBES_SPEED = 18f //div difficulty level
    const val TUBES_SPAWN_TIME = 1.8f //div difficulty level
    const val PAUSE_TIME = 2f //time to pause game for increment/decrement show
    const val LEVEL_INCREASE_TIME = 30f //sec - time to increase difficulty
    const val GAME_TIME = 60f
}