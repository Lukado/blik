package lukado.blik.screens.games.properties

object ColorConfusionProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "color_confusion"
    const val LOCALE_NAME = "color_confusion"

    /** Texture names */
    const val CARD = "text_card"
    const val GOOD = "good_mark"
    const val BAD = "bad_mark"

    /** Game preferences */
    const val TOP_SCORE = "cc.score"
    const val WEEK_SCORE = "cc.week"
    const val DIFFICULTY = "cc.diff"
    const val TIMES_PLAYED = "cc.played"
    const val FIRST_TIME = "cc.first.time"

    /** Game constants */
    const val DEFAULT_BOX_SPACING = 5f
    const val THROW_DELAY = 0.5f
    const val THROW_SPEED = 0.3f

    /** Game difficulty constants */
    const val GAME_TIME = 60f
    const val GUESSED_THRESHOLD = 30
}