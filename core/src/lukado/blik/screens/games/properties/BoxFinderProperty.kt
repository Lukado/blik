package lukado.blik.screens.games.properties

import com.badlogic.gdx.graphics.Color

object BoxFinderProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "box_finder"
    const val LOCALE_NAME = "box_finder"

    /** Texture names */
    const val BOX_HIDDEN = "box_hidden"
    const val GOOD_JOB = "good"

    /** Game preferences */
    const val TOP_SCORE = "bf.score"
    const val WEEK_SCORE = "bf.week"
    const val DIFFICULTY = "bf.diff"
    const val TIMES_PLAYED = "bf.played"
    const val FIRST_TIME = "bf.first.time"

    /** Game constants */
    const val DEFAULT_BOX_SPACING = 20f
    const val SHOW_TIME = 3f

    val COLOR_DEFAULT: Color = Color.WHITE
    val COLOR_DISABLED: Color = Color.valueOf("A9B1B1")
    val COLOR_FAIL: Color = Color.RED
    val COLOR_GOOD: Color = Color.valueOf("FDE74C")

    /** Game difficulty constants */
    const val GAME_TIME = 60f
}