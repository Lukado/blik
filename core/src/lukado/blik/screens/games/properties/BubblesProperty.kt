package lukado.blik.screens.games.properties

import com.badlogic.gdx.graphics.Color

object BubblesProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "bubbles"
    const val LOCALE_NAME = "bubbles"

    /** Texture names */
    const val BUBBLE = "bubble"

    /** Game preferences */
    const val TOP_SCORE = "bu.score"
    const val WEEK_SCORE = "bu.week"
    const val DIFFICULTY = "bu.diff"
    const val TIMES_PLAYED = "bu.played"
    const val FIRST_TIME = "bu.first.time"

    /** Game constants */
    const val WORLD_HEIGHT = 40f
    const val WORLD_WIDTH = WORLD_HEIGHT / 2f
    const val WORLD_GROUND_SIZE = WORLD_HEIGHT*0.15f
    const val BUBBLE_SIZE = WORLD_WIDTH * 0.15f

    val COLOR_UNPICKED: Color = Color.WHITE
    val COLOR_SPLITTER: Color = Color.BLACK
    val COLOR_SKY_LIMIT: Color = Color.valueOf("9BD1E5")
    val COLOR_PICKED: Color = Color.valueOf("FDE74C")

    /** Game difficulty constants */
    const val GAME_TIME = 60f
    const val MAX_NUMBER = 25
    const val MIN_NUMBER = 10
}