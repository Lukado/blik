package lukado.blik.screens.games.properties

import com.badlogic.gdx.graphics.Color

object TwoCarsProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "two_cars2"
    const val LOCALE_NAME = "two_cars"

    /** Texture names */
    const val BLUE_CAR = "blue"
    const val BLUE_SQUARE = "blue_square"
    const val BLUE_CIRCLE = "blue_circle"
    const val RED_CAR = "red"
    const val RED_SQUARE = "red_square"
    const val RED_CIRCLE = "red_circle"
    const val CAR = "car"

    /** Game preferences */
    const val TOP_SCORE = "tc.score"
    const val WEEK_SCORE = "tc.week"
    const val DIFFICULTY = "tc.diff"
    const val TIMES_PLAYED = "tc.played"
    const val FIRST_TIME = "tc.first.time"

    /** Game constants */
    const val WORLD_HEIGHT = 100f
    const val WORLD_WIDTH = WORLD_HEIGHT / 2f

    const val CAR_SPEED = 2f

    const val ELEMENTS_SCALE = WORLD_WIDTH * 0.1f
    const val ELEMENTS_SPEED = 5f
    // Range difference between blue and red elements Y-position
    const val ELEMENTS_OFFSET = 3f

    val COLOR_MAIN_LINE: Color = Color.valueOf("A50C0A")
    val COLOR_SIDE_LINE: Color = Color.valueOf("B7332A")

    /** Game difficulty constants */
    const val GAME_TIME = 60f
}