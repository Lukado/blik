package lukado.blik.screens.games.properties

import com.badlogic.gdx.graphics.Color

object EstimationProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "estimation"
    const val LOCALE_NAME = "estimation"

    /** Texture names */
    const val AXIS_BAR = "bar2"
    const val RESULT_BOX = "result_box"

    /** Game preferences */
    const val TOP_SCORE = "es.score"
    const val WEEK_SCORE = "es.week"
    const val DIFFICULTY = "es.diff"
    const val TIMES_PLAYED = "es.played"
    const val FIRST_TIME = "es.first.time"
    const val DIFFICULTY_COUNTER = "es.diffCounter"

    /** Game constants */
    const val WORLD_HEIGHT = 400f
    const val WORLD_WIDTH = WORLD_HEIGHT / 2f
    const val MAX_AXIS_NUMBER = 30

    val RECT_COLOR: Color = Color.valueOf("F8F0FB88")
    val DOT_COLOR: Color = Color.valueOf("B3001BFF")
    val PERFECT_COLOR: Color = Color.valueOf("FFC846")
    val GOOD_COLOR: Color = Color.FOREST
    val FAIL_COLOR: Color = Color.FIREBRICK

    /** Game difficulty constants */
    const val GAME_TIME = 60f
}