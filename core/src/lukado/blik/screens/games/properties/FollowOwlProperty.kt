package lukado.blik.screens.games.properties

import com.badlogic.gdx.graphics.Color
import lukado.blik.configuration.Config

object FollowOwlProperty {
    /** Default properties */
    const val THUMBNAIL_NAME = "owl2"
    const val LOCALE_NAME = "follow_owl"

    /** Texture names */
    const val BOX = "box"
    const val OWL = "owl"
    const val GOOD_JOB = "good"

    /** Game preferences */
    const val TOP_SCORE = "fo.score"
    const val WEEK_SCORE = "fo.week"
    const val DIFFICULTY = "fo.diff"
    const val TIMES_PLAYED = "fo.played"
    const val FIRST_TIME = "fo.first.time"
    const val DIFFICULTY_COUNTER = "fo.diffCounter"

    /** Game constants */
    const val MAXIMUM_BOXES = 10
    const val DEFAULT_BOX_SIZE = Config.APP_WIDTH/6f
    const val SHOW_TIME = 3f

    val COLOR_DEFAULT: Color = Color.WHITE
    val COLOR_DISABLED: Color = Color.valueOf("A9B1B1")
    val COLOR_GOOD: Color = Color.GREEN
    val COLOR_FAIL: Color = Color.RED

    /** Game difficulty constants */
    const val GAME_TIME = 60f
}