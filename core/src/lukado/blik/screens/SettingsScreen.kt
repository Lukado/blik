package lukado.blik.screens

import com.badlogic.gdx.*
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.scenes.scene2d.ui.List
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Scaling
import lukado.blik.Blik
import lukado.blik.configuration.AssetConfig
import lukado.blik.configuration.Config
import lukado.blik.configuration.PreferenceConfig
import lukado.blik.enums.EGenderType
import lukado.blik.enums.ELocale
import lukado.blik.enums.EScreen
import lukado.blik.models.UserModel
import java.util.*

/**
 * Settings screen for the user containing app, login, locale and user options. User can adjust
 * these options to his preferences. User options are only important for information gathering
 * from games.
 */
class SettingsScreen(parent: Blik) : BlikNavigationPanel(parent), Screen, Observer, InputProcessor {
    //Class unique
    private val preferences: Preferences = parent.preferences
    private var languageList: List<String>? = null
    private var isMusicEnabled: Boolean
        get() = preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)
        private set(enable) {
            preferences.putBoolean(PreferenceConfig.MUSIC_ENABLED, enable)
            preferences.flush()
        }
    private val labelStyle: LabelStyle
    private var screenState: EScreen = EScreen.SETTINGS
    private val multiplexer: InputMultiplexer = InputMultiplexer()

    //Animations
    private var loading: TextureRegion? = null
    private var loadRepeatAnim: Animation<TextureRegion>? = null
    private var stateTime = 0f

    //Sounds
    private var checkSound: Sound
    private var buttonSound: Sound

    //Table actors
    private val table: Table = Table()
    private val innerTable: Table = Table()
    private var loginTable: Table = Table()

    private var languageButton: TextButton? = null
    private var updateButton: TextButton? = null
    private var logoutButton: TextButton? = null
    private var back: TextButton? = null

    private var emailRegistrationButton: ImageButton? = ImageButton(parent.skin, "email")
    private var googleRegistrationButton: ImageButton? = ImageButton(parent.skin, "google")
    private var facebookRegistrationButton: ImageButton? = ImageButton(parent.skin, "facebook")
    private var editButton: ImageButton? = ImageButton(parent.skin, "edit")
    private var onlineButton: ImageButton? = if (parent.firebaseManager.hasConnection) ImageButton(parent.skin?.getDrawable("online")) else ImageButton(parent.skin?.getDrawable("offline"))

    private var emailConfirmNewButton: TextButton? = null
    private var emailLoginButton: TextButton? = null
    private var emailNewButton: TextButton? = null
    private var emailPassRecoveryConfirm: TextButton? = null
    private var emailPassRecoveryButton: TextButton? = null

    private var nameTextField: TextField? = null
    private var genderSelectBox: SelectBox<String>? = null
    private var ageSelectBox: SelectBox<String>? = null
    private var emailTextField: TextField? = null
    private var passwordTextField: TextField? = null
    private var passwordConfirmTextField: TextField? = null

    private var emailLabel: Label? = null
    private var passwordLabel: Label? = null
    private var passwordConfirmLabel: Label? = null
    private var errorLabel: Label? = null

    init {
        multiplexer.addProcessor(parent.stage)
        multiplexer.addProcessor(this)

        val ls = LabelStyle()
        ls.font = parent.skin?.getFont("osifontSmall")
        ls.fontColor = Color.RED

        errorLabel = Label("", ls)
        errorLabel?.setAlignment(Align.center)
        errorLabel?.setWrap(true)

        labelStyle = LabelStyle()
        labelStyle.font = parent.skin?.getFont("osifontSmall")
        labelStyle.fontColor = Color.BLACK
//        labelStyle.background = parent.skin?.getDrawable("text_button")

        val tbs = TextButton.TextButtonStyle()
        tbs.font = parent.skin?.getFont("osifontMedium")
        tbs.fontColor = Color.BLACK
        tbs.up = parent.skin?.getDrawable("text_button")
        tbs.down = parent.skin?.getDrawable("text_button_down")

        languageButton = TextButton(parent.locale?.locale?.language, tbs)
        updateButton = TextButton(parent.locale?.get("accept_changes"), parent.skin)
        logoutButton = TextButton(parent.locale?.get("logout_title"), parent.skin)
        emailLoginButton = TextButton(parent.locale?.get("login_title"), parent.skin)
        emailConfirmNewButton = TextButton(parent.locale?.get("create_account"), parent.skin)
        emailNewButton = TextButton(parent.locale?.get("create_account_label"), parent.skin, "hyperlink")
        editButton = ImageButton(parent.skin, "edit")
        emailPassRecoveryButton = TextButton(parent.locale?.get("pass_reset_label"), parent.skin, "hyperlink")
        emailPassRecoveryConfirm = TextButton(parent.locale?.get("pass_reset"), parent.skin)
        nameTextField = TextField("", parent.skin)
        nameTextField?.alignment = Align.center
        emailTextField = TextField("", parent.skin)
        passwordTextField = TextField("", parent.skin)
        passwordConfirmTextField = TextField("", parent.skin)
        passwordTextField?.isPasswordMode = true
        passwordConfirmTextField?.isPasswordMode = true
        passwordTextField?.setPasswordCharacter('*')
        passwordConfirmTextField?.setPasswordCharacter('*')

        var index = 0
        val languages = Array<String>()
        ELocale.values().forEach {
            languages.add(it.languageName)
            if (it.languageTag == parent.locale?.locale?.toLanguageTag()) {
                index = languages.size - 1
            }
        }
        languageList = List(parent.skin)
        languageList?.setItems(languages)
        languageList?.selectedIndex = index

        loadSelectBoxItems()

        val loadBackground: TextureAtlas = parent.manager.am.get(AssetConfig.LOAD_REPEAT)
        val background = loadBackground.createSprites()

        loadRepeatAnim = Animation(0.05f, background)
        loading = background.first()

        checkSound = parent.manager.createSound("music/tap.mp3")
        buttonSound = parent.manager.createSound("music/click.mp3")
    }

    /**
     * Creates interface based on the current screen parameter. The interface changes with different
     * screen type parameter which differs for contrasting option changing.
     * @param eScreen screen interface requirement
     */
    private fun createUi(eScreen: EScreen) {
        table.clear()
        table.setFillParent(true)
        screenState = eScreen

        //Table begins
        innerTable.clear()
        innerTable.background = null

        when (eScreen) {
            //User information change interface
            EScreen.SETTINGS_USER -> {
                loadSelectBoxItems()

                nameTextField?.text = if (parent.preferences.getString(PreferenceConfig.USERNAME) == "universal_name") parent.locale?.get("universal_name") else parent.preferences.getString(PreferenceConfig.USERNAME)
                val nameLabel = Label(parent.locale?.get("nick_title") + ":", labelStyle)
                nameLabel.setAlignment(Align.right)
                val ageLabel = Label(parent.locale?.get("age_title") + ":", labelStyle)
                ageLabel.setAlignment(Align.right)
                val genderLabel = Label(parent.locale?.get("gender_title") + ":", labelStyle)
                genderLabel.setAlignment(Align.right)
                back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
                back?.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        passwordConfirmTextField?.text = ""
                        passwordTextField?.text = ""
                        emailTextField?.text = ""
                        errorLabel?.setText("")
                        keyDown(Input.Keys.BACK)
                    }
                })

                innerTable.row().padBottom(Config.DEFAULT_TABLE_PADDING).padTop(Config.DEFAULT_TABLE_PADDING * 2).right()
                innerTable.add(nameLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(nameTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)

                innerTable.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                innerTable.add(genderLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(genderSelectBox).growX().padRight(Config.DEFAULT_TABLE_PADDING)

                innerTable.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                innerTable.add(ageLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(ageSelectBox).growX().padRight(Config.DEFAULT_TABLE_PADDING)

                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING).colspan(2)
                innerTable.add(updateButton).fillX()
                innerTable.row().padTop(Config.DEFAULT_TABLE_PADDING / 4).colspan(2)
                innerTable.add(errorLabel)
                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING / 2).expand().bottom().padBottom(Config.DEFAULT_TABLE_PADDING / 2).colspan(2)
                innerTable.add(back)
            }
            //App language change interface
            EScreen.SETTINGS_LANGUAGE -> {
                //Language picker area
                innerTable.add(languageList).grow().pad(Config.DEFAULT_TABLE_PADDING, Config.DEFAULT_TABLE_PADDING, 0f, Config.DEFAULT_TABLE_PADDING)
                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING)
                innerTable.add(updateButton).fillX()
            }
            //Email creation interface
            EScreen.EMAIL_CREATE -> {
                innerTable.background = Image(loading).drawable
                emailLabel = Label(parent.locale?.get("new_email") + ":", labelStyle)
                emailLabel?.setAlignment(Align.right)
                passwordLabel = Label(parent.locale?.get("new_password") + ":", labelStyle)
                passwordLabel?.setAlignment(Align.right)
                passwordConfirmLabel = Label(parent.locale?.get("confirm_password") + ":", labelStyle)
                passwordConfirmLabel?.setAlignment(Align.right)
                back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
                back?.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        passwordConfirmTextField?.text = ""
                        passwordTextField?.text = ""
                        emailTextField?.text = ""
                        errorLabel?.setText("")
                        createUi(EScreen.EMAIL_LOGIN)
                    }
                })

                innerTable.row().padBottom(Config.DEFAULT_TABLE_PADDING).padTop(Config.DEFAULT_TABLE_PADDING * 2).right()
                innerTable.add(emailLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(emailTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                innerTable.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                innerTable.add(passwordLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(passwordTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                innerTable.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                innerTable.add(passwordConfirmLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(passwordConfirmTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                innerTable.row().colspan(2).padTop(Config.DEFAULT_TABLE_PADDING)
                innerTable.add(emailConfirmNewButton).expandX().center().width(Value.percentWidth(0.6f, innerTable))
                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING / 2).expand().bottom().padBottom(Config.DEFAULT_TABLE_PADDING / 2).colspan(2)
                innerTable.add(back)
            }
            //Email login interface
            EScreen.EMAIL_LOGIN -> {
                innerTable.background = Image(loading).drawable
                emailLabel = Label(parent.locale?.get("email") + ":", labelStyle)
                emailLabel?.setAlignment(Align.right)
                passwordLabel = Label(parent.locale?.get("password") + ":", labelStyle)
                passwordLabel?.setAlignment(Align.right)
                back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
                back?.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        errorLabel?.setText("")
                        keyDown(Input.Keys.BACK)
                    }
                })

                innerTable.row().padBottom(Config.DEFAULT_TABLE_PADDING).padTop(Config.DEFAULT_TABLE_PADDING * 2).right()
                innerTable.add(emailLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(emailTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                innerTable.row().padTop(Config.DEFAULT_TABLE_PADDING).padBottom(Config.DEFAULT_TABLE_PADDING).right()
                innerTable.add(passwordLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(passwordTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                innerTable.row().colspan(2).padTop(Config.DEFAULT_TABLE_PADDING)
                innerTable.add(emailLoginButton).expandX().center().width(Value.percentWidth(0.6f, innerTable))
                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING).colspan(2)
                innerTable.add(emailPassRecoveryButton)
                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING / 2).expand().bottom().colspan(2)
                innerTable.add(emailNewButton)
                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING / 2).padBottom(Config.DEFAULT_TABLE_PADDING / 2).colspan(2)
                innerTable.add(back)
            }
            EScreen.EMAIL_RECOVERY -> {
                emailLabel = Label(parent.locale?.get("email") + ":", labelStyle)
                emailLabel?.setAlignment(Align.right)
                back = TextButton(parent.locale?.get("back"), parent.skin, "hyperlink")
                back?.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        if (!parent.firebaseManager.isConnecting) {
                            errorLabel?.setText("")
                            createUi(EScreen.EMAIL_LOGIN)
                        }
                    }
                })

                innerTable.row().padBottom(Config.DEFAULT_TABLE_PADDING).padTop(Config.DEFAULT_TABLE_PADDING * 2).right()
                innerTable.add(emailLabel).padLeft(Config.DEFAULT_TABLE_PADDING).padRight(Config.DEFAULT_TABLE_PADDING).fillX()
                innerTable.add(emailTextField).growX().padRight(Config.DEFAULT_TABLE_PADDING)
                innerTable.row().colspan(2).padTop(Config.DEFAULT_TABLE_PADDING)
                innerTable.add(emailPassRecoveryConfirm).expandX().center().width(Value.percentWidth(0.6f, table))
                innerTable.row().pad(Config.DEFAULT_TABLE_PADDING / 2).expand().bottom().padBottom(Config.DEFAULT_TABLE_PADDING).colspan(2)
                innerTable.add(back)
            }
            //Default settings interface
            else -> {
                val ls = LabelStyle()
                ls.font = parent.skin?.getFont("osifontMedium")
                ls.fontColor = Color.BLACK

                var username = if (parent.preferences.getString(PreferenceConfig.USERNAME, "universal_name") == "universal_name") parent.locale?.get("universal_name") else parent.preferences.getString(PreferenceConfig.USERNAME)
                val gl = GlyphLayout()
                gl.setText(parent.skin?.getFont("osifont"), username)
                while (gl.width > Config.APP_WIDTH - 2 * nameTextField!!.height) {
                    username = username!!.substring(0, username.length - 4) + ".. "
                    gl.setText(parent.skin?.getFont("osifont"), username)
                }

                val nameLabel = Label(username, parent.skin)
                val ageLabel = Label(parent.locale?.get("age_title"), ls)
                val genderLabel = Label(parent.locale?.get("gender_title"), ls)
                val currentGenderLabel = Label(parent.locale?.get(parent.preferences.getString(PreferenceConfig.GENDER, EGenderType.UNKNOWN.localeName)), ls)
                val age = when (parent.preferences.getInteger(PreferenceConfig.AGE, 0)) {
                    0 -> parent.locale?.get(EGenderType.UNKNOWN.localeName)
                    in 1..12 -> parent.locale?.get("under_age") + " 13"
                    100 -> parent.locale?.get("over_age") + " 99"
                    else -> parent.preferences.getInteger(PreferenceConfig.AGE, 0).toString()
                }
                val currentAgeLabel = Label(age, ls)

                val musicLabel = Label(parent.locale?.get("music_title"), ls)
                val musicCheckbox = CheckBox(null, parent.skin)
                musicCheckbox.isChecked = isMusicEnabled
                musicCheckbox.addListener(object : ChangeListener(){
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        isMusicEnabled = musicCheckbox.isChecked
                        if(isMusicEnabled) checkSound.play()
                    }
                })
                val languageLabel = Label(parent.locale?.get("language_title"), ls)
                val loginLabel = Label(parent.locale?.get("login_title"), parent.skin)
                editButton?.isVisible = false
                onlineButton?.isVisible = parent.firebaseManager.isAdmin

                //User area
                val usernameTable = Table()
                usernameTable.add(onlineButton).expandX().size(nameLabel.height / 2).center().uniformX()
                usernameTable.add(nameLabel)
                usernameTable.add(editButton).expandX().size(nameLabel.height).center().uniformX()
                innerTable.add(usernameTable).colspan(2).padTop(Config.DEFAULT_TABLE_PADDING).growX()

                innerTable.row().growX().colspan(2)
                innerTable.add(Image(parent.skin?.getRegion("textfield")))
                innerTable.row().padTop(Config.DEFAULT_TABLE_PADDING / 2)

                //Scrollpane area
                val spTable = Table()
                spTable.skin = parent.skin

                //User settings area
                if (parent.firebaseManager.existingUser) {
                    editButton?.isVisible = true

                    spTable.row().uniformX()
                    spTable.add(genderLabel)
                    spTable.add(currentGenderLabel)

                    spTable.row().uniformX().padTop(Config.DEFAULT_TABLE_PADDING)
                    spTable.add(ageLabel)
                    spTable.add(currentAgeLabel)

                    spTable.row().padTop(Config.DEFAULT_TABLE_PADDING / 2).growX().colspan(2)
                    spTable.add(Image(parent.skin?.getRegion("textfield")))
                    spTable.row().padTop(Config.DEFAULT_TABLE_PADDING).uniformX()
                }

                //Settings area
                spTable.add(musicLabel)
                musicCheckbox.image.setScaling(Scaling.fill)
                spTable.add(musicCheckbox).center().size(musicLabel.height)

                spTable.row().padTop(Config.DEFAULT_TABLE_PADDING).uniformX().expandY().top()
                spTable.add(languageLabel)
                spTable.add(languageButton)

                //Login area, if user does not exist
                if (!parent.firebaseManager.existingUser) {
                    spTable.row().colspan(2).padTop(Config.DEFAULT_TABLE_PADDING * 2).center()
                    spTable.add(loginLabel)
                    spTable.row().growX().colspan(2)
                    spTable.add(Image(parent.skin?.getRegion("textfield")))

                    loginTable.clear()

                    spTable.row().pad(Config.DEFAULT_TABLE_PADDING).growX().colspan(2)
                    if (parent.firebaseManager.isConnecting) {
                        stateTime = 0f
                        loginTable.add(Image(loading)).grow().size(Config.DEFAULT_BUTTON_SIZE - 15f, Config.DEFAULT_BUTTON_SIZE)
                    } else {
                        loginTable.row().growX().size(Config.DEFAULT_BUTTON_SIZE)
                        loginTable.add(emailRegistrationButton)
                        loginTable.add(facebookRegistrationButton)
                        loginTable.add(googleRegistrationButton)
                    }
                    spTable.add(loginTable).top()
                }

                //Logout area
                if (parent.firebaseManager.existingUser) {
                    spTable.row().pad(Config.DEFAULT_TABLE_PADDING).colspan(2).growX()
                    spTable.add(logoutButton)
                }

                val scrollPane = ScrollPane(spTable)
                scrollPane.setSmoothScrolling(true)
                scrollPane.setScrollingDisabled(true, false)

                innerTable.add(scrollPane).grow().padTop(Config.DEFAULT_TABLE_PADDING).top()
            }
        }

        //Error display area
        innerTable.row().colspan(2)
        innerTable.add(errorLabel).center()

        table.add(innerTable).grow()
        //Navbar
        table.row()
        table.add(getNavigationBar()).bottom().growX().colspan(2)
        parent.stage.addActor(table)
    }

    /** Function that assets every button in the interface its listeners */
    private fun buttonsListening() {
        editButton?.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                editButton?.isDisabled = true
                Gdx.input.setCatchKey(Input.Keys.BACK, true)
                Gdx.input.inputProcessor = multiplexer
                createUi(EScreen.SETTINGS_USER)
            }
        })

        languageButton?.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                Gdx.input.setCatchKey(Input.Keys.BACK, true)
                Gdx.input.inputProcessor = multiplexer
                createUi(EScreen.SETTINGS_LANGUAGE)
            }
        })

        updateButton?.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                if (screenState == EScreen.SETTINGS_LANGUAGE) {
                    parent.manager.setLocale(ELocale.values()[languageList!!.selectedIndex])
                    languageButton?.setText(parent.locale?.locale?.language)
                    updateButton?.setText(parent.locale?.get("accept_changes"))
                    logoutButton?.setText(parent.locale?.get("logout_title"))
                    emailLoginButton?.setText(parent.locale?.get("login_title"))
                    emailConfirmNewButton?.setText(parent.locale?.get("create_account"))
                    emailNewButton?.setText(parent.locale?.get("create_account_label"))
                    emailPassRecoveryButton?.setText(parent.locale?.get("pass_reset_label"))
                    emailPassRecoveryConfirm?.setText(parent.locale?.get("pass_reset"))
                } else if (screenState == EScreen.SETTINGS_USER) {
                    parent.preferences.putString(PreferenceConfig.GENDER, EGenderType.values()[genderSelectBox?.selectedIndex!!].localeName)
                    val agePref = when (ageSelectBox?.selectedIndex) {
                        0 -> 0
                        1 -> 9
                        ageSelectBox?.items?.size?.minus(1) -> 99
                        else -> ageSelectBox?.selected?.toInt()!!
                    }
                    parent.preferences.putInteger(PreferenceConfig.AGE, agePref)
                    val namePref = if (nameTextField?.text == parent.locale?.get("universal_name")) "universal_name" else nameTextField?.text
                    parent.preferences.putString(PreferenceConfig.USERNAME, namePref)
                    parent.preferences.flush()

                    if (parent.firebaseManager.existingUser) parent.firebaseManager.updateUser(namePref!!, EGenderType.values()[genderSelectBox?.selectedIndex!!].localeName, agePref.toLong())
                }

                parent.stage.unfocusAll()
                Gdx.input.setOnscreenKeyboardVisible(false)

                keyDown(Input.Keys.BACK)
            }
        })

        googleRegistrationButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.googleLogin()
                    keyDown(Input.Keys.BACK)
                }
            }
        })

        facebookRegistrationButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.facebookLogin()
                    keyDown(Input.Keys.BACK)
                }
            }
        })

        emailRegistrationButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                errorLabel?.setText("")
                Gdx.input.setCatchKey(Input.Keys.BACK, true)
                Gdx.input.inputProcessor = multiplexer
                createUi(EScreen.EMAIL_LOGIN)
            }
        })

        emailConfirmNewButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.emailLogin(emailTextField!!.text, passwordTextField!!.text, passwordConfirmTextField?.text)
                    innerTable.children.forEach { it.addAction(Actions.fadeOut(Config.FADE_IN_OUT_SPEED)) }
                    parent.stage.unfocusAll()
                    Gdx.input.setOnscreenKeyboardVisible(false)
                }
            }
        })


        emailLoginButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                if (!parent.firebaseManager.isConnecting) {
                    parent.firebaseManager.emailLogin(emailTextField!!.text, passwordTextField!!.text)
                    innerTable.children.forEach { it.addAction(Actions.fadeOut(Config.FADE_IN_OUT_SPEED)) }
                    parent.stage.unfocusAll()
                    Gdx.input.setOnscreenKeyboardVisible(false)
                }
            }
        })

        emailNewButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                errorLabel?.setText("")
                Gdx.input.setCatchKey(Input.Keys.BACK, true)
                Gdx.input.inputProcessor = multiplexer
                createUi(EScreen.EMAIL_CREATE)
                parent.stage.unfocusAll()
                Gdx.input.setOnscreenKeyboardVisible(false)
            }
        })

        logoutButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if(parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) buttonSound.play()
                parent.firebaseManager.userLogout()
            }
        })

        emailPassRecoveryButton?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    errorLabel?.setText("")
                    createUi(EScreen.EMAIL_RECOVERY)
                }
            }
        })

        emailPassRecoveryConfirm?.addListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.firebaseManager.isConnecting) {
                    errorLabel?.setText("")
                    parent.firebaseManager.sendLostPassEmail(emailTextField!!.text)
                    parent.stage.unfocusAll()
                    Gdx.input.setOnscreenKeyboardVisible(false)
                }
            }
        })
    }

    /** Method to reset/stop interface animations */
    private fun resetGui() {
        stateTime = 0f
        loading!!.setRegion(loadRepeatAnim!!.getKeyFrame(stateTime, true))
        innerTable.act(Gdx.graphics.deltaTime)
        innerTable.children.forEach { it.addAction(Actions.fadeIn(Config.FADE_IN_OUT_SPEED)) }

        //WHY IS THIS HERE?!?! I surely had a reason... where is my past existence to explain it now
//        if (!emailTextField?.text.isNullOrBlank()) {
//            passwordConfirmTextField?.text = ""
//            passwordTextField?.text = ""
//            emailTextField?.text = ""
//            errorLabel?.setText("")
//            createUi(EScreen.SETTINGS)
//        }
    }

    /** Method to fill select boxes for age and gender with appropriate data of current locale */
    private fun loadSelectBoxItems() {
        ageSelectBox?.clearItems()
        genderSelectBox?.clearItems()

        val ages = Array<String>()
        ages.add(parent.locale?.get("unknown"))
        ages.add(parent.locale?.get("under_age") + " 13")
        for (i in 13..99) {
            ages.add(i.toString())
        }
        ages.add(parent.locale?.get("over_age") + " 99")
        ageSelectBox = SelectBox(parent.skin)
        ageSelectBox?.items = ages
        ageSelectBox?.setAlignment(Align.center)
        val currAge = parent.preferences.getInteger(PreferenceConfig.AGE, 0)
        ageSelectBox?.selectedIndex = when (currAge) {
            0 -> 0
            in 1..12 -> 1
            else -> currAge + 2 - 13
        }
        ageSelectBox?.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Gdx.input.setOnscreenKeyboardVisible(false)
                super.clicked(event, x, y)
            }
        })

        genderSelectBox = SelectBox(parent.skin)
        val genders = Array<String>()
        EGenderType.values().forEach { genders.add(parent.locale?.get(it.localeName)) }
        genderSelectBox?.items = genders
        genderSelectBox?.setAlignment(Align.center)
        genderSelectBox?.selected = parent.locale?.get(parent.preferences.getString(PreferenceConfig.GENDER, EGenderType.UNKNOWN.localeName))
        genderSelectBox?.addListener(object : ClickListener() {
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                Gdx.input.setOnscreenKeyboardVisible(false)
                super.clicked(event, x, y)
            }
        })
    }

    /** This method is inherited. Interface changes with notified data from observer. */
    override fun update(observable: Observable, data: Any?) {
        Gdx.app.postRunnable(Runnable {
            if (parent.screen !is SettingsScreen) {
                parent.firebaseManager.deleteObserver(this)
                return@Runnable
            }
            when (data) {
                is UserModel -> {
                    errorLabel?.setText("")
                    resetGui()
                    keyDown(Input.Keys.BACK)
                }
                is String -> {
                    errorLabel?.isVisible = true
                    errorLabel?.setText("")
                    errorLabel?.style?.fontColor = Color.RED
                    errorLabel?.clearActions()
                    when (data) {
                        Config.CONN_ERR -> {
                            errorLabel?.setText(parent.locale?.get("connection_error"))
                        }

                        Config.EMAIL_ALREADY_EXISTS -> {
                            errorLabel?.setText(parent.locale?.get("email_already_exists"))
                        }

                        Config.LOGIN_FAILED -> {
                            errorLabel?.setText(parent.locale?.get("login_failed"))
                        }

                        Config.DATABASE_ERR -> {
                            errorLabel?.setText(parent.locale?.get("database_error"))
                        }

                        Config.EMAIL_NOT_VALID -> {
                            errorLabel?.setText(parent.locale?.get("email_invalid"))
                        }

                        Config.PASSWORD_NOT_VALID -> {
                            errorLabel?.setText(parent.locale?.get("password_invalid"))
                        }

                        Config.PASSWORD_NOT_MATCH -> {
                            errorLabel?.setText(parent.locale?.get("password_invalid_match"))
                        }

                        Config.USER_NOT_EXISTS -> {
                            errorLabel?.setText(parent.locale?.get("user_not_exists"))
                        }
                        Config.PASSWORD_RESET_FAIL -> {
                            errorLabel?.setText(parent.locale?.get("pass_reset_fail"))
                        }
                        Config.PASSWORD_RESET_OK -> {
                            createUi(EScreen.EMAIL_LOGIN)
                            errorLabel?.style?.fontColor = Color.GREEN
                            errorLabel?.setText(parent.locale?.get("pass_reset_sent"))
                            errorLabel?.addAction(Actions.sequence(
                                    Actions.delay(3f),
                                    Actions.fadeOut(1f),
                                    Actions.run {
                                        errorLabel?.setText("")
                                        errorLabel?.style?.fontColor = Color.RED
                                        errorLabel?.isVisible = true
                                    }))
                        }
                        else -> {
                            errorLabel?.setText("")
                        }
                    }
                    resetGui()
                    createUi(screenState)
                }
                is Boolean -> {
                    val ibs = ImageButton.ImageButtonStyle()
                    ibs.imageUp = if (data) parent.skin?.getDrawable("online") else parent.skin?.getDrawable("offline")
                    onlineButton?.style = ibs
                }
                else -> {
                    resetGui()
                    createUi(screenState)
                    Gdx.app.log("Fireapp Settings Screen", data.toString())
                }
            }
        })
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.show()
     */
    override fun show() {
        parent.stage.clear()
        parent.firebaseManager.addObserver(this)

        val ibs = ImageButton.ImageButtonStyle()
        ibs.imageUp = if (parent.firebaseManager.hasConnection) parent.skin?.getDrawable("online") else parent.skin?.getDrawable("offline")
        onlineButton?.style = ibs

        errorLabel?.setText("")
        keyDown(Input.Keys.BACK)
        buttonsListening()

        innerTable.addAction(Actions.sequence(Actions.alpha(0f), Actions.fadeIn(Config.FADE_IN_OUT_SPEED)))
        //Forced act of action.alpha(), otherwise screen stutters before first stage.act()
        innerTable.act(Gdx.graphics.deltaTime)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.render()
     */
    override fun render(delta: Float) {
        Gdx.gl.glClearColor(1f, 0.784f, 0.275f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        if (delta < 0.3 && parent.firebaseManager.isConnecting) {
            stateTime += delta
            loading!!.setRegion(loadRepeatAnim!!.getKeyFrame(stateTime, true))
        }

        parent.stage.act(Gdx.graphics.deltaTime)
        parent.stage.draw()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.hide()
     */
    override fun hide() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.pause()
     */
    override fun pause() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.resume()
     */
    override fun resume() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.dispose()
     */
    override fun dispose() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.resize()
     */
    override fun resize(width: Int, height: Int) {
        parent.stage.viewport.update(width, height, true)
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor.touchUp()
     */
    override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        return false
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor.mouseMoved()
     */
    override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
        return false
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor.keyTyped()
     */
    override fun keyTyped(character: Char): Boolean {
        return false
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor.scrolled()
     */
    override fun scrolled(amount: Int): Boolean {
        return false
    }

    /** This method is inherited. Description is in parent class
     *  @see InputProcessor.keyUp()
     */
    override fun keyUp(keycode: Int): Boolean {
        return false
    }

    /** @see InputProcessor.touchDragged() */
    override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
        return false
    }

    /** @see InputProcessor.keyDown()
     * This method is inherited. Listens for HW back button and does actions instead of leaving application
     */
    override fun keyDown(keycode: Int): Boolean {
        return when (keycode) {
            Input.Keys.BACK -> {
                if (screenState == EScreen.EMAIL_CREATE) {
                    createUi(EScreen.EMAIL_LOGIN)
                } else {
                    Gdx.input.inputProcessor = parent.stage
                    Gdx.input.setCatchKey(Input.Keys.BACK, false)
                    createUi(EScreen.SETTINGS)
                }
                true
            }
            else -> {
                false
            }
        }
    }

    /** @see InputProcessor.touchDown() */
    override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
        parent.stage.unfocusAll()
        Gdx.input.setOnscreenKeyboardVisible(false)
        return false
    }
}