package lukado.blik.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.Array
import lukado.blik.Blik
import lukado.blik.configuration.AssetConfig
import lukado.blik.configuration.Config
import lukado.blik.configuration.GameConfig
import lukado.blik.enums.EGameType
import lukado.blik.models.GlobalStatModel
import lukado.blik.screens.games.interfaces.IGameCompanion
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

/**
 * Statistics screen shows online comparation against this user. The comparison is dont using radar graph
 * or by classic progress/bar graphs. The user is mostly compared to the top and average scores throughout
 * different categories of his choice.
 */
class StatisticsScreen(parent: Blik) : BlikNavigationPanel(parent), Screen, Observer {
    //Class unique
    private var categoryCheck: Boolean
    private var comparingToGlobalAvg: Boolean
    private var loadingBoxes: Boolean
    private var selectedGameSave: Int
    private var gameChoice: IGameCompanion? = null
    private var gameTypeColor: Color? = null
    private var databaseData: Any? = null

    private var sr: ShapeRenderer
    private var sb: SpriteBatch
    private var radarChartValues: ArrayList<Float>
    private var gameChartValues: ArrayList<Float>

    private val chartFontLayout: GlyphLayout
    private val chartTypeFontLayout: GlyphLayout
    private val chartFont: BitmapFont
    private val chartTypeFont: BitmapFont
    private val chartFontOffset = 10f

    private var gameList: List<Int>

    //Table actors
    private val table: Table = Table()
    private val innerTable: Table = Table()
    private val gameSelectBox: SelectBox<String> = SelectBox(parent.skin, "small")
    private val categorySelectBox: SelectBox<String> = SelectBox(parent.skin, "small")
    private var issueLabel: Label

    //Animations
    private var loading: TextureRegion? = null
    private var loadRepeatAnim: Animation<TextureRegion>? = null
    private var stateTime = 0f

    init {
        parent.firebaseManager.addObserver(this)
        categoryCheck = true
        comparingToGlobalAvg = true
        loadingBoxes = false
        selectedGameSave = 0
        radarChartValues = ArrayList()
        gameChartValues = ArrayList()
        gameList = ArrayList()

        val lsm = Label.LabelStyle()
        lsm.font = parent.skin?.getFont("osifontMedium")
        lsm.fontColor = Color.BLACK

        issueLabel = Label(parent.locale?.get("connection_issue_stats"), lsm)
        issueLabel.wrap = true
        issueLabel.setAlignment(Align.center)

        val loadBackground: TextureAtlas = parent.manager.am.get(AssetConfig.LOAD_REPEAT)
        val background = loadBackground.createSprites()

        loadRepeatAnim = Animation(0.05f, background)
        loading = background.first()

        sb = SpriteBatch()
        sr = ShapeRenderer()
        sr.color = Color.WHITE
        ShaderProgram.pedantic = false

        chartFontLayout = GlyphLayout()
        chartTypeFontLayout = GlyphLayout()
        chartFont = parent.skin?.getFont("osifontMedium")!!
        chartFont.color = Color.BLACK
        chartTypeFont = parent.skin?.getFont("osifontMini")!!

        gameSelectBox.addCaptureListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (gameSelectBox.selected == null || loadingBoxes || !parent.stage.actors.contains(table)) return

                if (categorySelectBox.items.size > 0) categorySelectBox.selectedIndex = 0
                gameChoice = null
                EGameType.values().forEach {
                    it.games.forEach { ot ->
                        if (parent.locale?.get(ot.localeName) == gameSelectBox.selected) {
                            gameChoice = ot
                            gameTypeColor = it.color
                            return@forEach
                        }
                    }
                    if (gameChoice != null) return@forEach
                }
                parent.firebaseManager.getGlobalStatistics(gameChoice?.localeName ?: "")
                table.background = Image(loading).drawable
                innerTable.isVisible = false
                categorySelectBox.isDisabled = true
                databaseData = null
            }
        })

        categorySelectBox.addCaptureListener(object : ChangeListener() {
            override fun changed(event: ChangeEvent?, actor: Actor?) {
                if (!parent.stage.actors.contains(table) || databaseData !is GlobalStatModel || categorySelectBox.selectedIndex < 0 || loadingBoxes || gameChoice == null) return
                createUi()
            }
        })
    }

    /** Creates default interface for statistics, containing user demographics for each cognitive
     * category. If the user is logged in and connected to the internet, he can compare against
     * other users. Also contains button to calculate update global statistics - visible only for admins.
     */
    private fun createUi() {
        table.clear()
        table.setFillParent(true)
        table.debug = parent.stage.isDebugAll
        table.background = null

        innerTable.clear()
        innerTable.debug = parent.stage.isDebugAll
        innerTable.isVisible = true

        if (parent.firebaseManager.existingUser && parent.firebaseManager.hasConnection) {
            if (databaseData != null) {
                if (databaseData is Map<*,*>) {
                    val userMapStat = HashMap<String, List<Long>>()
                    EGameType.values().forEach {
                        var gameAverage = 0L
                        var gameTop = 0L
                        it.games.forEach { ot ->
                            gameList = parent.gameManager.getTopScorePreferences(ot)
                            gameAverage += gameList.average().toLong()
                            gameTop += gameList.first()
                        }
                        userMapStat[it.name] = arrayListOf(gameAverage / it.games.size, gameTop / it.games.size)
                    }

                    radarChartValues.clear()
                    if (comparingToGlobalAvg) {
                        EGameType.values().forEach {
                            var item = ((userMapStat[it.name]?.get(0)
                                    ?: 0).toFloat() / (((databaseData as HashMap<*, *>)[it.name] as List<*>)[0] as Long))
                            item = if (item > 1f) 1f else item
                            radarChartValues.add(item)
                        }
                        innerTable.add(Label(parent.locale?.get("global_avg"), parent.skin)).expand().top().pad(Config.DEFAULT_TABLE_PADDING).colspan(2).row()
                    } else {
                        EGameType.values().forEach {
                            var item = ((userMapStat[it.name]?.get(1)
                                    ?: 0).toFloat() / (((databaseData as HashMap<*, *>)[it.name] as List<*>)[1] as Long))
                            item = if (item > 1f) 1f else item
                            radarChartValues.add(item)
                        }
                        innerTable.add(Label(parent.locale?.get("global_top"), parent.skin)).expand().top().pad(Config.DEFAULT_TABLE_PADDING).colspan(2).row()
                    }

                    categorySelectBox.isDisabled = true
                } else {
                    val lsm = Label.LabelStyle()
                    lsm.font = parent.skin?.getFont("osifontMedium")
                    lsm.fontColor = Color.BLACK

                    val lss = Label.LabelStyle()
                    lss.font = parent.skin?.getFont("osifontMini")
                    lss.fontColor = Color.BLACK

                    gameList = parent.gameManager.getTopScorePreferences(gameChoice!!)
                    gameChartValues.clear()

                    val gsm = databaseData as GlobalStatModel

                    innerTable.add(Label(parent.locale?.get("game_global_top"), lsm)).expandX().align(Align.left).pad(Config.DEFAULT_TABLE_PADDING, Config.DEFAULT_TABLE_PADDING * 2f, 0f, Config.DEFAULT_TABLE_PADDING)
                    innerTable.add(Label("Maximum", lss)).width(Config.DEFAULT_TABLE_PADDING * 2.8f).padTop(Config.DEFAULT_TABLE_PADDING).align(Align.center).row()
                    innerTable.add(Label("${gameList.first().toInt()}", lsm)).expandX().pad(Config.DEFAULT_TABLE_PADDING / 2f, Config.DEFAULT_TABLE_PADDING, 0f, 0f)
                    innerTable.add(Label("${gsm.theBestScore!!.toInt()}", lsm)).width(Config.DEFAULT_TABLE_PADDING * 2.8f).padTop(Config.DEFAULT_TABLE_PADDING / 2f).row()

                    innerTable.add(Label(parent.locale?.get("game_global_avg"), lsm)).expandX().align(Align.left).pad(Config.DEFAULT_TABLE_PADDING, Config.DEFAULT_TABLE_PADDING * 2f, 0f, Config.DEFAULT_TABLE_PADDING).colspan(2).row()
                    innerTable.add(Label("${gameList.average().toInt()}", lsm)).expandX().pad(Config.DEFAULT_TABLE_PADDING / 2f, Config.DEFAULT_TABLE_PADDING, 0f, 0f)
                    innerTable.add(Label("${gsm.averageScore!!.toInt()}", lsm)).width(Config.DEFAULT_TABLE_PADDING * 2.8f).padTop(Config.DEFAULT_TABLE_PADDING / 2f).row()

                    var item: Float = if (gameList.first().toFloat() / gsm.theBestScore!!.toFloat() > 1) 1f else gameList.first().toFloat() / gsm.theBestScore!!.toFloat()
                    gameChartValues.add(item)
                    item = if (gameList.average().toFloat() / gsm.averageScore!!.toFloat() > 1) 1f else gameList.average().toFloat() / gsm.averageScore!!.toFloat()
                    gameChartValues.add(item)

                    var section: Map<String, ArrayList<Long>>
                    if (categorySelectBox.selectedIndex > 0) {
                        var number: Long
                        if (categoryCheck) {
                            section = gsm.scoreByDifficulty!!
                            number = if (gsm.scoreByDifficulty!!["theBestScore"]!![categorySelectBox.selectedIndex - 1] < 1) 1L else gsm.scoreByDifficulty!!["theBestScore"]!![categorySelectBox.selectedIndex - 1]
                            item = if (gameList.first().toFloat() / number > 1f) 1f else gameList.first().toFloat() / number
                            gameChartValues.add(item)

                            number = if (gsm.scoreByDifficulty!!["averageScore"]!![categorySelectBox.selectedIndex - 1] < 1) 1L else gsm.scoreByDifficulty!!["averageScore"]!![categorySelectBox.selectedIndex - 1]
                            item = if (gameList.average().toFloat() / number > 1f) 1f else gameList.average().toFloat() / number
                            gameChartValues.add(item)
                        } else {
                            section = gsm.scoreByAge!!
                            number = if (gsm.scoreByAge!!["theBestScore"]!![categorySelectBox.selectedIndex - 1] < 1) 1L else gsm.scoreByAge!!["theBestScore"]!![categorySelectBox.selectedIndex - 1]
                            item = if (gameList.first().toFloat() / number > 1f) 1f else gameList.first().toFloat() / number
                            gameChartValues.add(item)

                            number = if (gsm.scoreByAge!!["averageScore"]!![categorySelectBox.selectedIndex - 1] < 1) 1L else gsm.scoreByAge!!["averageScore"]!![categorySelectBox.selectedIndex - 1]
                            item = if (gameList.average().toFloat() / number > 1f) 1f else gameList.average().toFloat() / number
                            gameChartValues.add(item)
                        }

                        innerTable.add(Label(parent.locale?.get("game_category_top"), lsm)).expandX().align(Align.left).pad(Config.DEFAULT_TABLE_PADDING, Config.DEFAULT_TABLE_PADDING * 2f, 0f, Config.DEFAULT_TABLE_PADDING).colspan(2).row()
                        innerTable.add(Label("${gameList.first().toInt()}", lsm)).expandX().pad(Config.DEFAULT_TABLE_PADDING / 2f, Config.DEFAULT_TABLE_PADDING, 0f, 0f)
                        innerTable.add(Label("${section["theBestScore"]!![categorySelectBox.selectedIndex - 1]}", lsm)).width(Config.DEFAULT_TABLE_PADDING * 2.8f).padTop(Config.DEFAULT_TABLE_PADDING / 2f).row()

                        innerTable.add(Label(parent.locale?.get("game_category_avg"), lsm)).expandX().align(Align.left).pad(Config.DEFAULT_TABLE_PADDING, Config.DEFAULT_TABLE_PADDING * 2f, 0f, Config.DEFAULT_TABLE_PADDING).colspan(2).row()
                        innerTable.add(Label("${gameList.average().toInt()}", lsm)).expandX().pad(Config.DEFAULT_TABLE_PADDING / 2f, Config.DEFAULT_TABLE_PADDING, 0f, 0f)
                        innerTable.add(Label("${section["averageScore"]!![categorySelectBox.selectedIndex - 1]}", lsm)).width(Config.DEFAULT_TABLE_PADDING * 2.8f).padTop(Config.DEFAULT_TABLE_PADDING / 2f).row()
                    }

                    innerTable.add(Label("", parent.skin)).expand().colspan(2).row()
                    categorySelectBox.isDisabled = false
                }
            } else {
                innerTable.add(Label(parent.locale?.get("downloading"), parent.skin)).expand().top().pad(Config.DEFAULT_TABLE_PADDING / 4).colspan(2).row()
            }

            val lsm = Label.LabelStyle()
            lsm.font = parent.skin?.getFont("osifontMedium")
            lsm.fontColor = Color.BLACK
            val gamesLabel = Label(parent.locale?.get("stats_game"), lsm)
            val categoryLabel = if (categoryCheck) Label(parent.locale?.get("stats_difficulty"), lsm) else Label(parent.locale?.get("age_title"), lsm)

            val controlTable = Table()
            val choiceTable = Table()
            choiceTable.add(gamesLabel).padBottom(Config.DEFAULT_TABLE_PADDING / 4)
            choiceTable.add(categoryLabel).padLeft(Config.DEFAULT_TABLE_PADDING / 2).padBottom(Config.DEFAULT_TABLE_PADDING / 4)
            choiceTable.row()
            choiceTable.add(gameSelectBox)
            choiceTable.add(categorySelectBox).padLeft(Config.DEFAULT_TABLE_PADDING / 2)
            val refreshButton = ImageButton(parent.skin, "refresh")
            refreshButton.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    selectedGameSave = gameSelectBox.selectedIndex
                    if (databaseData is Map<*, *>) {
                        comparingToGlobalAvg = !comparingToGlobalAvg
                    } else {
                        loadingBoxes = true
                        categoryCheck = !categoryCheck
                        loadSelectBoxItems(categoryCheck)
                    }
                    createUi()
                }
            })
            controlTable.add(choiceTable).expandX()
            controlTable.add(refreshButton).size(gamesLabel.height * 1.5f).padLeft(Config.DEFAULT_TABLE_PADDING).expandX()
            innerTable.add(controlTable).pad(Config.DEFAULT_TABLE_PADDING / 4).expandX().colspan(2)
            innerTable.row()

        } else if (parent.firebaseManager.existingUser) {
            issueLabel.setText(parent.locale?.get("connection_issue_stats"))

            innerTable.add(issueLabel).width(Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 2).expand().center()
            innerTable.row()
        } else {
            issueLabel.setText(parent.locale?.get("not_logged"))

            innerTable.add(issueLabel).width(Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 2).expand().center()
            innerTable.row()
        }

        if (parent.firebaseManager.isAdmin) {
            val globalCalculator = TextButton("Secret button!", parent.skin)
            globalCalculator.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    parent.firebaseManager.calculateGlobalStats()
                    super.clicked(event, x, y)
                }
            })
            innerTable.add(globalCalculator).bottom().padTop(Config.DEFAULT_TABLE_PADDING).colspan(2)
            innerTable.row()
        }

        table.add(innerTable).grow().padBottom(Config.DEFAULT_TABLE_PADDING)
        table.row()
        table.add(getNavigationBar()).bottom().growX()
        parent.stage.addActor(table)
    }


    /** Method to fill select boxes for games, difficulty and age with appropriate data of current locale
     * @param difficulty is difficulty select box category shown
     * */
    private fun loadSelectBoxItems(difficulty: Boolean) {
        gameSelectBox.clearItems()
        categorySelectBox.clearItems()

        val items = Array<String>()
        items.add(parent.locale?.get("all_select_choice"))
        EGameType.values().forEach { it.games.forEach { ot -> items.add(parent.locale?.get(ot.localeName)) } }
        gameSelectBox.items = items
        gameSelectBox.setAlignment(Align.center)
        gameSelectBox.selectedIndex = selectedGameSave

        items.clear()
        items.add(parent.locale?.get("all_select_choice"))
        if (difficulty) {
            GameConfig.GAME_DIFFICULTIES.forEach { items.add(it.toString()) }
        } else {
            GameConfig.GAME_DIFFICULTIES.forEachIndexed { i, it ->
                if (GameConfig.GAME_DIFFICULTIES.last() != it) items.add("${10 * i}-${10 * i + 9}")
                else items.add("${10 * i}+")
            }
        }
        categorySelectBox.items = items
        categorySelectBox.setAlignment(Align.center)
        loadingBoxes = false
    }

    /**
     * Core method for own graphics drawing charts. Draws radar chart if all games are compared or
     * progress charts if single game is compared. Uses ShapeRenderer for drawing.
     */
    private fun drawCharts() {
        if (databaseData is Map<*, *> && radarChartValues.isNotEmpty() && !innerTable.children.contains(issueLabel)) {
            val r = parent.stage.viewport.worldHeight * 0.8f - parent.stage.viewport.worldHeight * 0.63f //graph radius
            val xCentre = parent.stage.viewport.worldWidth * 0.5f    //graph centre x
            val yCentre = parent.stage.viewport.worldHeight * 0.6f   //graph centre y
            val theta = 90f * PI / 180         //graph rotation

            sr.begin(ShapeRenderer.ShapeType.Filled)
            //draw radar chart bg
            EGameType.values().forEachIndexed { index, _ ->
                val nextVert = (index + 1) % EGameType.values().size
                sr.triangle((r * cos(2f * PI * index / EGameType.values().size + theta) + xCentre).toFloat(), (r * sin(2f * PI * index / EGameType.values().size + theta) + yCentre).toFloat(),
                        (r * cos(2f * PI * nextVert / EGameType.values().size + theta) + xCentre).toFloat(), (r * sin(2f * PI * nextVert / EGameType.values().size + theta) + yCentre).toFloat(),
                        xCentre, yCentre, Color.WHITE, Color.WHITE, Color.WHITE)
            }

            //draw lines over radar chart
            for (i in 1..3) {
                val rPart = r * (0.25f * i)
                EGameType.values().forEachIndexed { index, _ ->
                    val nextVert = (index + 1) % EGameType.values().size
                    sr.rectLine((rPart * cos(2f * PI * index / EGameType.values().size + theta) + xCentre).toFloat(), (rPart * sin(2f * PI * index / EGameType.values().size + theta) + yCentre).toFloat(),
                            (rPart * cos(2f * PI * nextVert / EGameType.values().size + theta) + xCentre).toFloat(), (rPart * sin(2f * PI * nextVert / EGameType.values().size + theta) + yCentre).toFloat(),
                            0.5f, Color.GRAY, Color.GRAY)
                }
            }

            //draw game type lines over radar chart
            EGameType.values().forEachIndexed { index, eGameType ->
                sr.rectLine((r * cos(2f * PI * index / EGameType.values().size + theta) + xCentre).toFloat(), (r * sin(2f * PI * index / EGameType.values().size + theta) + yCentre).toFloat(),
                        xCentre, yCentre, 2f, eGameType.color, eGameType.color)
            }

            //draw user data lines
            radarChartValues.forEachIndexed { index, it ->
                val currHeight = r * it
                val nextVert = (index + 1) % radarChartValues.size
                val nextHeight = r * radarChartValues[nextVert]
                sr.rectLine((currHeight * cos(2f * PI * index / EGameType.values().size + theta) + xCentre).toFloat(), (currHeight * sin(2f * PI * index / EGameType.values().size + theta) + yCentre).toFloat(),
                        (nextHeight * cos(2f * PI * nextVert / EGameType.values().size + theta) + xCentre).toFloat(), (nextHeight * sin(2f * PI * nextVert / EGameType.values().size + theta) + yCentre).toFloat(),
                        2f, Color.SKY, Color.SKY)

                var offsetX = 0f
                var offsetY = 0f
                var offsetTypeX = 0f
                var offsetTypeY = 0f
                sb.begin()
                chartFontLayout.setText(chartFont, "${(it * 100).toInt()}%")
                when (index) {
                    0 -> {
                        offsetX = -chartFontLayout.width / 2f
                        offsetY = chartFontLayout.height + chartFontOffset
                        chartTypeFont.color = EGameType.ATTENTION.color
                        chartTypeFontLayout.setText(chartTypeFont, parent.locale?.get(EGameType.ATTENTION.name))
                        offsetTypeX = offsetX + chartFontLayout.width / 2f - chartTypeFontLayout.width / 2f
                        offsetTypeY = offsetY + chartFontLayout.height
                    }
                    1 -> {
                        offsetX = -chartFontLayout.width - chartFontOffset
                        offsetY = chartFontLayout.height / 2f
                        chartTypeFont.color = EGameType.MEMORY.color
                        chartTypeFontLayout.setText(chartTypeFont, parent.locale?.get(EGameType.MEMORY.name))
                        offsetTypeX = offsetX + chartFontLayout.width / 2f - chartTypeFontLayout.width / 2f
                        offsetTypeY = offsetY + chartFontLayout.height
                    }
                    2 -> {
                        offsetX = -chartFontLayout.width - chartFontOffset
                        offsetY = -chartFontOffset
                        chartTypeFont.color = EGameType.LANGUAGE.color
                        chartTypeFontLayout.setText(chartTypeFont, parent.locale?.get(EGameType.LANGUAGE.name))
                        offsetTypeX = offsetX + chartFontLayout.width / 2f - chartTypeFontLayout.width / 2f
                        offsetTypeY = offsetY - chartFontLayout.height - chartFontOffset
                    }
                    3 -> {
                        offsetX = chartFontOffset
                        offsetY = -chartFontOffset
                        chartTypeFont.color = Color.valueOf("FFF700")
                        chartTypeFontLayout.setText(chartTypeFont, parent.locale?.get(EGameType.VISUAL_SPATIAL.name))
                        offsetTypeX = offsetX + chartFontLayout.width / 2f - chartTypeFontLayout.width / 2f
                        offsetTypeY = offsetY - chartFontLayout.height - chartFontOffset
                    }
                    4 -> {
                        offsetX = chartFontOffset
                        offsetY = chartFontLayout.height / 2f
                        chartTypeFont.color = EGameType.EXECUTIVE.color
                        chartTypeFontLayout.setText(chartTypeFont, parent.locale?.get(EGameType.EXECUTIVE.name))
                        offsetTypeX = offsetX + chartFontLayout.width / 2f - chartTypeFontLayout.width / 2f
                        offsetTypeY = offsetY + chartFontLayout.height
                    }
                }
                chartFont.draw(sb, chartFontLayout,
                        (r * cos(2f * PI * index / EGameType.values().size + theta) + xCentre).toFloat() + offsetX,
                        (r * sin(2f * PI * index / EGameType.values().size + theta) + yCentre).toFloat() + offsetY)
                chartTypeFont.draw(sb, chartTypeFontLayout,
                        (r * cos(2f * PI * index / EGameType.values().size + theta) + xCentre).toFloat() + offsetTypeX,
                        (r * sin(2f * PI * index / EGameType.values().size + theta) + yCentre).toFloat() + offsetTypeY)
                sb.end()
            }
            sr.end()
        } else if (databaseData is GlobalStatModel && gameChartValues.isNotEmpty() && !innerTable.children.contains(issueLabel)) {
            chartFontLayout.setText(parent.skin?.getFont("osifontMedium"), "G")
            sr.begin(ShapeRenderer.ShapeType.Filled)
            var offsetY = 0f

            println(gameChartValues)
            println(Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 3f)
            println(Config.APP_WIDTH)
            println(parent.stage.viewport.worldWidth)
            gameChartValues.forEach {
                sr.rectLine(Config.DEFAULT_TABLE_PADDING,
                        parent.stage.viewport.worldHeight - Config.DEFAULT_TABLE_PADDING*1.6f - chartFontLayout.height*2.5f - offsetY,
                        Config.DEFAULT_TABLE_PADDING + Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 4f,
                        parent.stage.viewport.worldHeight - Config.DEFAULT_TABLE_PADDING*1.6f - chartFontLayout.height*2.5f - offsetY,
                        Config.DEFAULT_TABLE_PADDING * 2f,
                        AssetConfig.PROG_COLOR_A, AssetConfig.PROG_COLOR_B
                )

                if (it > 0) {
                    val progColA = Color(Color.rgba8888(gameTypeColor))
                    progColA.a = 0.2f
                    val progColB = Color(Color.rgba8888(gameTypeColor))
                    progColB.a = 0.8f

                    sr.rectLine(Config.DEFAULT_TABLE_PADDING,
                            parent.stage.viewport.worldHeight - Config.DEFAULT_TABLE_PADDING*1.6f - chartFontLayout.height*2.5f - offsetY,
                            Config.DEFAULT_TABLE_PADDING + (Config.APP_WIDTH - Config.DEFAULT_TABLE_PADDING * 4f) * it,
                            parent.stage.viewport.worldHeight - Config.DEFAULT_TABLE_PADDING*1.6f - chartFontLayout.height*2.5f - offsetY,
                            Config.DEFAULT_TABLE_PADDING * 2f,
                            progColA, progColB)
                }
                offsetY += chartFontLayout.height + (Config.DEFAULT_TABLE_PADDING * 3.6f)
            }
            sr.end()
        }
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.show()
     */
    override fun show() {
        parent.stage.clear()
        sb = SpriteBatch()
        sr = ShapeRenderer()
        sr.color = Color.WHITE
        sb.projectionMatrix = parent.stage.camera.combined
        sr.projectionMatrix = parent.stage.camera.combined

        selectedGameSave = 0
        loadSelectBoxItems(categoryCheck)
        createUi()

        if(databaseData==null && parent.firebaseManager.existingUser && parent.firebaseManager.hasConnection) table.background = Image(loading).drawable
        parent.firebaseManager.getGlobalStatistics("")

        innerTable.addAction(Actions.sequence(Actions.alpha(0f), Actions.fadeIn(Config.FADE_IN_OUT_SPEED)))
        //Forced act of action.alpha(), otherwise screen stutters before first stage.act()
        innerTable.act(Gdx.graphics.deltaTime)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.render()
     */
    override fun render(delta: Float) {
        Gdx.gl.glClearColor(1f, 0.784f, 0.275f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        Gdx.gl.glEnable(GL20.GL_BLEND)
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)

        if (delta < 0.3 && table.background != null) {
            stateTime += delta
            loading!!.setRegion(loadRepeatAnim!!.getKeyFrame(stateTime, true))
        }

        drawCharts()

        parent.stage.act(Gdx.graphics.deltaTime)
        parent.stage.draw()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.resize()
     */
    override fun resize(width: Int, height: Int) {
        parent.stage.viewport.update(width, height, true)
    }

    /** This method is inherited. Description is in parent class
     * @see Screen.pause()
     */
    override fun pause() {}

    /** This method is inherited. Description is in parent class
     * @see Screen.resume()
     */
    override fun resume() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.hide()
     */
    override fun hide() {
        parent.disposeMenuScreens()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.dispose()
     */
    override fun dispose() {
        sb.dispose()
        sr.dispose()
        chartFont.dispose()
        chartTypeFont.dispose()
    }

    /** This method is inherited. Description is in parent class
     *  Observer update method to receive data from custom firebase http connections
     */
    override fun update(observable: Observable, data: Any?) {
        Gdx.app.postRunnable(Runnable {
            if (parent.screen !is StatisticsScreen) return@Runnable

            stateTime = 0f
            loading!!.setRegion(loadRepeatAnim!!.getKeyFrame(stateTime, true))
            innerTable.color.a = 1f

            when (data) {
                is Boolean -> {
                    if (databaseData == null) createUi()
                }
                is Map<*, *> -> {
                    databaseData = data
                    createUi()
                }
                is GlobalStatModel -> {
                    data.cleanJsonData()
                    databaseData = data
                    createUi()
                }
                else -> {
                    Gdx.app.log("Fireapp Stat Screen", data.toString())
                }
            }
        })
    }
}