package lukado.blik.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Animation
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import lukado.blik.Blik
import lukado.blik.configuration.AssetConfig
import lukado.blik.enums.EGameType
import lukado.blik.enums.EScreen
import kotlin.reflect.full.primaryConstructor
import com.badlogic.gdx.utils.I18NBundle as I18NBundle1

/**
 * Screen controlling initial loading of all necessary application dependent assets.
 * Loading uses asset manager for this purpose.
 */
class LoadingScreen(private val parent: Blik) : Screen {
    //Class unique
    private val table: Table = Table()
    private val ls = LabelStyle()
    private val skin: Skin
    private var loc: I18NBundle1? = null
    private var loadingOverflowController: Float = 0f
    private var screenChanged: Boolean = false

    //Table actors
    var title: Label? = null

    //Animations
    private var loading: TextureRegion? = null
    private var loadDropAnim: Animation<TextureRegion>? = null
    private var stateTime: Float = 0f

    init {
        //Load kotlin reflections by using one of the imported function, otherwise, it lags when calling for the first time
        EGameType.values().first().games.first().getClassInstance().primaryConstructor

        // manager load loading assets
        parent.manager.loadLoadingFiles()
        parent.manager.am.finishLoading()

        loc = parent.manager.am.get(AssetConfig.LOCALES, I18NBundle1::class.java)
        skin = parent.manager.am.get(AssetConfig.SKIN)
        ls.font = skin.getFont("osifontHuge")
        ls.fontColor = Color.BLACK

        val loadDropTextures: TextureAtlas = parent.manager.am.get(AssetConfig.LOAD_DROP)
        val spritesDrop = loadDropTextures.createSprites()

        loadDropAnim = Animation(0.05f, spritesDrop)
        loading = spritesDrop.first()
    }

    /** Creates animated loading image as background during the loading */
    private fun createUI() {
        table.clear()
        table.setFillParent(true)

        table.background = Image(loading).drawable
        table.row().expandY()
        if (parent.stage.isDebugAll) table.add(title).bottom().padBottom(50f)
        parent.stage.addActor(table)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.show()
     */
    override fun show() {
        title = Label(loc?.format("load_text"), ls)

        createUI()

        parent.manager.loadTextures()

        Gdx.input.inputProcessor = parent.stage
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.render()
     *  Animation loops until all required assets are loaded
     */
    override fun render(delta: Float) {
        Gdx.gl.glClearColor(1f, 0.785f, 0.275f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        if (delta < 0.3 && !screenChanged) {
            stateTime += delta
            loading!!.setRegion(loadDropAnim!!.getKeyFrame(stateTime, true))
        }

        if (parent.manager.am.update()) {
            loadingOverflowController += delta
            if (title!!.textEquals(loc?.format("load_text")) && !screenChanged) title!!.setText(loc?.get("load_done"))
            if ((loadDropAnim!!.getKeyFrameIndex(stateTime) in 50..56 || loadingOverflowController > 2) && !screenChanged) {
                screenChanged = true
                parent.changeScreen(EScreen.LOGIN)
            }
        }

        parent.stage.act(Gdx.graphics.deltaTime)
        parent.stage.draw()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.hide()
     */
    override fun hide() {}
    /** This method is inherited. Description is in parent class
     *  @see Screen.pause()
     */
    override fun pause() {}
    /** This method is inherited. Description is in parent class
     *  @see Screen.resume()
     */
    override fun resume() {}
    /** This method is inherited. Description is in parent class
     *  @see Screen.dispose()
     */
    override fun dispose() {}
    /** This method is inherited. Description is in parent class
     *  @see Screen.resize()
     */
    override fun resize(width: Int, height: Int) {
        parent.stage.viewport.update(width, height, true)
    }
}