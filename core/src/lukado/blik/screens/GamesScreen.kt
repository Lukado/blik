package lukado.blik.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane
import com.badlogic.gdx.scenes.scene2d.ui.Table
import lukado.blik.Blik
import lukado.blik.configuration.Config

/**
 * Screen presenting all playable games in this application. After touching to game redirects
 * user to game specific and detailed controls.
 */
class GamesScreen(parent: Blik) : BlikNavigationPanel(parent), Screen {
    //Class unique
    private var savedScroll: Float = 0f

    //Table actors
    private val table: Table = Table()
    private val innerTable: Table = Table()
    private lateinit var scrollPane: ScrollPane

    /** Creates default game screen interface */
    private fun createUi() {
        table.clear()
        table.setFillParent(true)
        table.debug = parent.stage.isDebugAll

        //Table begin
        innerTable.clear()

        val titleLabel = Label(parent.locale?.format("games_title"), parent.skin)
        innerTable.add(titleLabel).top().padTop(Config.DEFAULT_TABLE_PADDING)
        innerTable.row()

        val spTable = parent.gameManager.getGamesTable()

        scrollPane = ScrollPane(spTable)
        scrollPane.setSmoothScrolling(true)
        scrollPane.setScrollingDisabled(true, false)

        innerTable.add(scrollPane).grow().padTop(Config.DEFAULT_TABLE_PADDING)

        table.add(innerTable).grow()
        table.row()
        table.add(getNavigationBar()).bottom().growX()
        parent.stage.addActor(table)

        scrollPane.layout()
        scrollPane.scrollY = savedScroll
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.show()
     */
    override fun show() {
        parent.stage.clear()

        createUi()

        innerTable.addAction(Actions.sequence(Actions.alpha(0f), Actions.fadeIn(Config.FADE_IN_OUT_SPEED)))
        //Forced act of action.alpha(), otherwise screen stutters before first stage.act()
        innerTable.act(Gdx.graphics.deltaTime)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.render()
     */
    override fun render(delta: Float) {
        Gdx.gl.glClearColor(1f, 0.784f, 0.275f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        parent.stage.act(Gdx.graphics.deltaTime)
        parent.stage.draw()
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.resize()
     */
    override fun resize(width: Int, height: Int) {
        parent.stage.viewport.update(width, height, true)
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.pause()
     */
    override fun pause() {
        savedScroll = scrollPane.scrollY
    }

    /** This method is inherited. Description is in parent class
     *;@see Screen.resume()
     */
    override fun resume() {}

    /** This method is inherited. Description is in parent class
     *  @see Screen.hide()
     */
    override fun hide() {
        savedScroll = scrollPane.scrollY
    }

    /** This method is inherited. Description is in parent class
     *  @see Screen.dispose()
     */
    override fun dispose() {}
}