package lukado.blik.managers

import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import lukado.blik.Blik
import lukado.blik.configuration.AssetConfig
import lukado.blik.configuration.Config
import lukado.blik.enums.EGameType
import lukado.blik.models.ScoreModel
import lukado.blik.screens.games.interfaces.IGameCompanion
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random
import kotlin.reflect.full.primaryConstructor

/**
 * Class to manage all games and provide general functions above them.
 */
class GameManager(private val parent: Blik) {
    private val r: Random = Random(Calendar.getInstance().get(Calendar.DAY_OF_YEAR))

    /**
     * Generates recommended games for today from the existing game types. Only one game per game
     * type is chosen randomly.
     */
    fun getDailyGames(): MutableList<IGameCompanion> {
        val randomArray: ArrayList<IGameCompanion> = ArrayList()

        EGameType.values().forEach {
            randomArray.add(it.games.random(r))
        }

//        randomArray.shuffle(r)
//        return randomArray.subList(0, 5)
        return randomArray
    }

    /**
     * Generates games table including all games for games screen. Every game type background is
     * colored based on the game type enum color.
     * @return table actor with all games
     */
    fun getGamesTable(): Table {
        val outputTable = Table()
        val thumbnails: TextureAtlas = parent.manager.am.get(AssetConfig.GAME_THUMBNAILS)
        outputTable.skin = parent.skin

        val ls: Label.LabelStyle = Label.LabelStyle()
        ls.fontColor = Color.BLACK
        ls.font = parent.skin!!.getFont("osifontSmall")

        var titleTable: Table

        EGameType.values().forEach { type ->
            titleTable = Table()

            val bgPixmap = Pixmap(1, 1, Pixmap.Format.RGB888)
            bgPixmap.setColor(type.color)
            bgPixmap.fill()

            val label = Container(Label(parent.locale?.format(type.toString()), parent.skin))
            titleTable.background = Image(Texture(bgPixmap)).drawable
            bgPixmap.dispose()

            titleTable.add(label).growX().center().colspan(2).padTop(10f)
            titleTable.row()

            var tmp = 0
            type.games.forEach { gameInfo ->
                tmp++
                val rowTable = Table()

                val gameButton = ImageButton(Image(thumbnails.findRegion(gameInfo.backgroundImagePath)).drawable)
                gameButton.addListener(object : ChangeListener() {
                    override fun changed(event: ChangeEvent?, actor: Actor?) {
                        parent.screen = gameInfo.getClassInstance().primaryConstructor?.call(parent, true) as Screen?
                    }
                })

                rowTable.add(gameButton).size(Config.APP_WIDTH * 0.4f).row()
                rowTable.add(Label(parent.locale?.format(gameInfo.localeName), ls)).center()

                if (type.games.size == tmp && tmp % 2 == 1) {
                    titleTable.add(rowTable).expand().pad(10f, 0f, 10f, 0f).colspan(2)
                } else {
                    titleTable.add(rowTable).expand().pad(10f, 0f, 10f, 0f)
                }

                if (tmp % 2 == 0) {
                    titleTable.row()
                }
            }

            outputTable.add(titleTable).grow()
            outputTable.row()
        }

        return outputTable
    }

    /**
     * Saves top ten and weekly scored data for given game. Also prepares those data and filters
     * them to stay actual.
     * @param gameInfo game preferences container
     * @param newScore new score to save
     */
    fun saveScorePreference(gameInfo: IGameCompanion, newScore: Int) {
        val bestScores = ArrayList(getTopScorePreferences(gameInfo))
        bestScores.add(newScore)
        bestScores.sortDescending()
        if (bestScores.size > 10) {
            bestScores.remove(bestScores.last())
        }
        parent.preferences.putString(gameInfo.topScoresPreference, bestScores.joinToString("-"))

        var weekScores = HashMap(getWeekScorePreferences(gameInfo))
        // duplicated code in branched if statement, but it is done just for more clarity
        if (!(weekScores[ScoreModel.mapFormat]
                        ?: Pair("", "")).first.contains(SimpleDateFormat("d.M.", Locale.ENGLISH).format(Calendar.getInstance().time))) {
            weekScores[ScoreModel.mapFormat] = Pair(SimpleDateFormat("d.M.", Locale.ENGLISH).format(Calendar.getInstance().time), newScore)
        } else if (newScore > (weekScores[ScoreModel.mapFormat]?.second ?: 0)) {
            weekScores[ScoreModel.mapFormat] = Pair(SimpleDateFormat("d.M.", Locale.ENGLISH).format(Calendar.getInstance().time), newScore)
        }
        if (weekScores.size>7){
            weekScores = weekScores.toList()
                    .map { Pair<String, Pair<Date, Int>>(it.first, Pair(SimpleDateFormat("d.M.", Locale.ENGLISH).parse(it.second.first), it.second.second)) }
                    .sortedByDescending { it.second.first }
                    .take(7)
                    .map { Pair<String, Pair<String, Int>>(it.first, Pair(SimpleDateFormat("d.M.", Locale.ENGLISH).format(it.second.first), it.second.second))}
                    .toMap() as HashMap<String, Pair<String, Int>>
        }

        var weekScoresStr = ""
        weekScores.forEach {
            val cal: Calendar = Calendar.getInstance()
            cal.time = SimpleDateFormat("d.M.yyyy", Locale.ENGLISH).parse(it.value.first + Calendar.getInstance()[Calendar.YEAR])
            weekScoresStr += "${it.key}.${SimpleDateFormat("yy,d,M", Locale.ENGLISH).format(cal.time)}.${it.value.second}-"
        }
        weekScoresStr = weekScoresStr.dropLast(1)
        parent.preferences.putString(gameInfo.weekScoresPreference, weekScoresStr)

        parent.preferences.flush()
    }

    /**
     * Parses saved preference data for top ten scores all time achieved. The data are saved as
     * string value in format: "1000-900-600-550-342-112-111-11-5-1" (descending)
     * @param gameInfo game preferences container
     * @return sorted list of scores
     */
    fun getTopScorePreferences(gameInfo: IGameCompanion): List<Int> {
        val str: String = try {
            parent.preferences.getString(gameInfo.topScoresPreference, "0")
        } catch (e: ClassCastException) {
            parent.preferences.getInteger(gameInfo.topScoresPreference, 0).toString()
        }
        return str.split("-").map { Integer.parseInt(it) }
    }

    /**
     * Parses saved preference data for weekly achieved scores. The data are saved as string value in
     * format: "sat.20,1,12.1234-sun.20,4,5.4321-mon........" (weekday.year,day,month.score)
     * @param gameInfo game preferences container
     * @return map<weekday, pair<date,score>>
     */
    fun getWeekScorePreferences(gameInfo: IGameCompanion): Map<String, Pair<String, Int>> {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, -1)
        var str = parent.preferences.getString(gameInfo.weekScoresPreference, SimpleDateFormat("EEE.yy,d,M", Locale.ENGLISH).format(cal.time).plus(".0"))
        str = if (str.isNullOrBlank()) SimpleDateFormat("EEE.yy,d,M", Locale.ENGLISH).format(cal.time).plus(".0") else str

        val splitStr: HashMap<String, Pair<Date, Int>> = HashMap()
        str.split("-").forEach {
            val split = it.split(".")
            val date = SimpleDateFormat("yy,d,M", Locale.ENGLISH).parse(split[1])
            if (!splitStr.containsKey(split[0])) splitStr[split[0]] = Pair(date, Integer.parseInt(split[2]))
        }
        return splitStr.toList().sortedBy { value -> value.second.first }.map {
            Pair<String, Pair<String, Int>>(it.first, Pair(SimpleDateFormat("d.M.", Locale.ENGLISH).format(it.second.first), it.second.second))
        }.toMap()
    }

    /**
     * Parses saved preference data for weekly achieved scores. The data are saved as string value in
     * format: "sat.20,1,12.1234-sun.20,4,5.4321-mon........" (weekday.year,day,month.score)
     * Outputs map for database save
     * @param gameInfo game preferences container
     * @return map<string, long>
     */
    fun getWeekScorePreferencesToDbMap(gameInfo: IGameCompanion): Map<String, Long> {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, -1)
        var str = parent.preferences.getString(gameInfo.weekScoresPreference, SimpleDateFormat("EEE.yy,d,M", Locale.ENGLISH).format(cal.time).plus(".0"))
        str = if (str.isNullOrBlank()) SimpleDateFormat("EEE.yy,d,M", Locale.ENGLISH).format(cal.time).plus(".0") else str

        val splitStr: HashMap<String, Long> = HashMap()
        str.split("-").forEach {
            val split = it.split(".")
            splitStr[split[0] + "-" + split[1]] = Integer.parseInt(split[2]).toLong()
        }

        return splitStr
    }

    /**
     * Synchronizes database list and user preferences list to form one unique list of top 10 scores.
     * @param prefScores offline saved top scores
     * @param dbScores online saved top scores
     * @return one composed list with top scores
     */
    fun synchronizeTopScores(prefScores: List<Int>, dbScores: List<Long>?): List<Int>? {
        dbScores?.forEach {
            if(!prefScores.contains(it.toInt())) (prefScores as? ArrayList<Int>)?.add(it.toInt())
        }
        (prefScores as? ArrayList<Int>)?.sortDescending()
        if (prefScores.size > 10) return (prefScores as? ArrayList<Int>)?.take(10)

        return prefScores
    }

    /**
     * Synchronizes two week score saves from offline and online maps to form one map of best and
     * latest week scores.
     * @param prefScores offline saved week scores
     * @param dbScores online saved week scores
     * @return one composed map with latest top week scores
     */
    fun synchronizeWeekScores(prefScores: HashMap<String, Long>, dbScores: HashMap<String, Long>?): Map<String, Long>? {
        dbScores?.forEach {
            if (prefScores.containsKey(it.key) && it.value < prefScores[it.key] ?: 0) return@forEach
            prefScores[it.key] = it.value
        }

        var i = 0

        return prefScores.toList()
                .map {
                    try {
                        Pair(SimpleDateFormat("EEE-yy,d,M", Locale.ENGLISH).parse(it.first), it.second)
                    } catch (e: ParseException) {
                        i++
                        Pair(SimpleDateFormat("yy,d,M", Locale.ENGLISH).parse("20,$i,1"), it.second)
                    }
                 }
                .sortedByDescending { it.first }
                .take(7)
                .map { Pair(SimpleDateFormat("EEE-yy,d,M", Locale.ENGLISH).format(it.first), it.second) }
                .toMap()
    }
}