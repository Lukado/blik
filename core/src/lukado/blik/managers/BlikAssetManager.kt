package lukado.blik.managers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.I18NBundleLoader.I18NBundleParameter
import com.badlogic.gdx.assets.loaders.SkinLoader.SkinParameter
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.g2d.TextureAtlas
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.I18NBundle
import lukado.blik.configuration.AssetConfig
import lukado.blik.Blik
import lukado.blik.configuration.PreferenceConfig
import lukado.blik.enums.ELocale
import java.util.*

/**
 * Asset manager class for this application. Controls loading and unloading assets into application.
 * @see AssetManager
 */
class BlikAssetManager(blik: Blik) {
    val am = AssetManager()
    private val parent = blik
    private var locale: Locale? = null

    /**
     * Loads loading assets to show default animation.
     */
    fun loadLoadingFiles() {
        loadSkin()
        am.load(AssetConfig.LOAD_DROP, TextureAtlas::class.java)

        locale = if (parent.preferences.getString(PreferenceConfig.LANGUAGE, null) == null) null else Locale.forLanguageTag(parent.preferences.getString(PreferenceConfig.LANGUAGE))
        val param = I18NBundleParameter(locale, "UTF-8")
        am.load(AssetConfig.LOCALES, I18NBundle::class.java, param)
    }

    /** Loads skins assets */
    private fun loadSkin() {
        val params = SkinParameter(AssetConfig.TEXTURES)
        am.load(AssetConfig.SKIN, Skin::class.java, params)
    }

    /** Loads texture atlases for unique reasons, mainly animations */
    fun loadTextures(){
        am.load(AssetConfig.TEXTURES, TextureAtlas::class.java)
        am.load(AssetConfig.LOAD_REPEAT, TextureAtlas::class.java)
        am.load(AssetConfig.MAINMENU, TextureAtlas::class.java)
        am.load(AssetConfig.ENDING, TextureAtlas::class.java)
        am.load(AssetConfig.GAME_THUMBNAILS, TextureAtlas::class.java)
        am.load(AssetConfig.GAME_GUIDES, TextureAtlas::class.java)
    }

    /** Loads specific game textures
     * @param path game texture file path
     * @return texture atlas class
     */
    fun loadGameAssets(path: String): TextureAtlas {
        am.load(path, TextureAtlas::class.java)
        am.finishLoadingAsset<TextureAtlas>(path)
        return am.get(path)
    }

    /** Loads music and sets its volume
     * @param path music file path
     * @return music libgdx class
     */
    fun createMusic(path: String?): Music {
        val m = am.get<Music>(path)
        if (parent.preferences.getBoolean(PreferenceConfig.MUSIC_ENABLED, true)) {
            m.volume = 0F
        } else {
            m.volume = 1F
        }
        return m
    }

    /** Loads sound and sets its volume
     * @param path music file path
     * @return music libgdx class
     */
    fun createSound(path: String?): Sound {
        val s = Gdx.audio.newSound(Gdx.files.internal(path));
        return s
    }


    /**
     * Changes locale based on the new parameter locale
     * @param locale
     */
    fun setLocale(locale: ELocale) {
        this.locale = Locale.forLanguageTag(locale.languageTag)
        val param = I18NBundleParameter(this.locale, "UTF-8")

        am.unload(AssetConfig.LOCALES)
        am.finishLoading()
        am.load(AssetConfig.LOCALES, I18NBundle::class.java, param)
        am.finishLoading()

        parent.preferences.putString(PreferenceConfig.LANGUAGE, this.locale.toString())
        parent.preferences.flush()
        parent.locale = parent.manager.am.get(AssetConfig.LOCALES, I18NBundle::class.java)
    }

}