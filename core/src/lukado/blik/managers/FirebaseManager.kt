package lukado.blik.managers

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Net
import com.badlogic.gdx.Net.HttpResponseListener
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Base64Coder
import com.badlogic.gdx.utils.JsonReader
import de.tomgrill.gdxfacebook.core.*
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import lukado.blik.Blik
import lukado.blik.configuration.Config
import lukado.blik.configuration.GameConfig
import lukado.blik.configuration.PreferenceConfig
import lukado.blik.enums.EGameType
import lukado.blik.enums.EGenderType
import lukado.blik.models.GlobalStatModel
import lukado.blik.models.ScoreModel
import lukado.blik.models.UserModel
import lukado.blik.screens.BlikGameScreen
import lukado.blik.screens.LoadingScreen
import lukado.blik.screens.LoginScreen
import lukado.blik.screens.SettingsScreen
import lukado.blik.screens.games.interfaces.IGameCompanion
import pl.mk5.gdx.fireapp.GdxFIRApp
import pl.mk5.gdx.fireapp.GdxFIRAuth
import pl.mk5.gdx.fireapp.GdxFIRDatabase
import pl.mk5.gdx.fireapp.GdxFIRLogger
import pl.mk5.gdx.fireapp.database.ConnectionStatus
import pl.mk5.gdx.fireapp.functional.Consumer
import java.security.KeyFactory
import java.security.PrivateKey
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Main database communication class. Establishes connection between firebase database and
 * this application and provides responses for other screens.
 */
open class FirebaseManager(private val parent: Blik) : Observable(), HttpResponseListener {
    private var hasAlreadySynchronized: Boolean
    var hasConnection: Boolean
    var isConnecting: Boolean
    var isAdmin: Boolean
    var existingUser = parent.preferences.getBoolean(PreferenceConfig.EXISTING_USER, false)
    private var lastLoginType: String = ""
    private var user: UserModel
    private val facebook: GDXFacebook

    init {
        val config = GDXFacebookConfig()
        config.APP_ID = Config.FACEBOOK_APP_ID // required
        config.PREF_FILENAME = ".facebookSessionData" // optional
        config.GRAPH_API_VERSION = "v2.6" // optional
        facebook = GDXFacebookSystem.install(config)

        GdxFIRApp.inst().configure()
        if (!Config.RELEASE) {
            GdxFIRLogger.setEnabled(true)
        }
        hasConnection = false
        isConnecting = false
        isAdmin = false
        hasAlreadySynchronized = false

        user = UserModel(0L, EGenderType.UNKNOWN.localeName, "universal_name")

        GdxFIRDatabase.inst().onConnect().then(Consumer { connection ->
            hasConnection = connection == ConnectionStatus.CONNECTED

            if (hasConnection && !hasAlreadySynchronized && (parent.screen !is LoginScreen && parent.screen !is LoadingScreen && parent.screen !is SettingsScreen && parent.screen !is BlikGameScreen)) {
                when (parent.preferences.getString(PreferenceConfig.USER_LOGIN_TYPE)) {
                    Config.EMAIL -> {
                        emailLogin(parent.preferences.getString(PreferenceConfig.EMAIL, ""), parent.preferences.getString(PreferenceConfig.EMAIL_PASSWORD, ""))
                    }
                    Config.FACEBOOK -> {
                        facebookLogin()
                    }
                    Config.GOOGLE -> {
                        googleLogin()
                    }
                    else -> userLogout()
                }
            }

            checkConnection()
        })
    }

    /** Custom email login processing logic and verification. When passwordConf parameter is provided
     *  new email creation is processed. Returns error or success notification to observables.
     *
     *  @param email full email name
     *  @param password plaintext password
     *  @param passwordConf plaintext password confirmation
     * */
    fun emailLogin(email: String, password: String, passwordConf: String? = null) {
        isConnecting = true
        lastLoginType = Config.EMAIL

        if (email.isBlank() || !isEmailValid(email)) {
            notifyData(Config.EMAIL_NOT_VALID)
            return
        } else if (password.length < 6) {
            notifyData(Config.PASSWORD_NOT_VALID)
            return
        }

        if (passwordConf != null) {
            if (password != passwordConf) {
                notifyData(Config.PASSWORD_NOT_MATCH)
                return
            }

            GdxFIRAuth.inst().createUserWithEmailAndPassword(email, password.toCharArray())
                    .then(Consumer { gdxFirebaseUser ->
                        Gdx.app.log("Fireapp", "UserModel logged: " + gdxFirebaseUser.userInfo.email)
                        parent.preferences.putString(PreferenceConfig.EMAIL, email)
                        parent.preferences.putString(PreferenceConfig.EMAIL_PASSWORD, password)
                        parent.preferences.flush()
                        loadUserData()
                    })
                    .fail { s, _ ->
                        if (s.contains("The email address is already in use by another account")) {
                            Gdx.app.error("Fireapp", "Email already in use: $s")
                            notifyData(Config.EMAIL_ALREADY_EXISTS)
                        } else {
                            Gdx.app.error("Fireapp", "Email create user failed: $s")
                            notifyData(Config.PASSWORD_NOT_VALID)
                        }
                    }
        } else {
            GdxFIRAuth.inst().signInWithEmailAndPassword(email, password.toCharArray())
                    .then(Consumer { gdxFirebaseUser ->
                        Gdx.app.log("Fireapp", "UserModel logged: " + gdxFirebaseUser.userInfo.email)
                        parent.preferences.putString(PreferenceConfig.EMAIL, email)
                        parent.preferences.putString(PreferenceConfig.EMAIL_PASSWORD, password)
                        parent.preferences.flush()
                        loadUserData()
                    })
                    .fail { s, _ ->
                        Gdx.app.error("Fireapp", "Email firebase login failed: $s")
                        notifyData(Config.USER_NOT_EXISTS)
                    }
        }
    }

    /**
     * Google gmail account login processing. This is fully managed by firebase API.
     * Returns error or success notification to observables.
     */
    fun googleLogin() {
        isConnecting = true
        lastLoginType = Config.GOOGLE

        GdxFIRAuth.inst().google().signIn()
                .then(Consumer { gdxFirebaseUser ->
                    Gdx.app.log("Fireapp", "UserModel logged: " + gdxFirebaseUser.userInfo.email)
                    loadUserData()
                })
                .fail { s, _ ->
                    Gdx.app.error("Fireapp", "Google firebase login failed: $s")
                    notifyData(Config.LOGIN_FAILED)
                }
    }

    /**
     * Facebook account login processing. This is managed by GDXFacebook dependency.
     * Returns error notification to observables or calls another method for Facebook data gathering.
     */
    fun facebookLogin() {
        isConnecting = true
        lastLoginType = Config.FACEBOOK

        val permissions = Array<String>()
        permissions.add("public_profile")
        permissions.add("email")

        facebook.signIn(SignInMode.READ, permissions, object : GDXFacebookCallback<SignInResult> {
            // Login successful
            override fun onSuccess(result: SignInResult) {
                //Send http request for facebook data
                handleFacebookData()
                Gdx.app.log("GDX_FB", "Facebook user logged")
            }

            // Error handling
            override fun onError(error: GDXFacebookError) {
                notifyData(Config.LOGIN_FAILED)
                Gdx.app.error("GDX_FB", "Facebook login error: $error")
            }

            // When the user cancels the login process
            override fun onCancel() {
                notifyData(Config.CONN_CANCEL)
            }

            // When the login fails
            override fun onFail(t: Throwable) {
                notifyData(Config.LOGIN_FAILED)
                Gdx.app.error("GDX_FB", "Facebook login failed: $t")
            }
        })
    }

    /**
     * Sends facebook request for user data after handling successful app->facebook connection.
     * Creates custom JWT token for facebook login that is used to communicate with firebase.
     * Returns error or success notification to observables.
     */
    fun handleFacebookData() {
        val request = GDXFacebookGraphRequest()
        request.node = "me" // call against the "me" node.
        request.useCurrentAccessToken()
        request.putField("fields", "name,id,email")

        facebook.graph(request, object : GDXFacebookCallback<JsonResult> {
            override fun onSuccess(result: JsonResult?) {
                val jsonValue = result!!.jsonValue
                user.nick = jsonValue.getString("name")

                // Oh god, gdx-fireapp currently do not support login by facebook, so we go with custom token
                // Parse json response and generate JSON Web Token for firebase token login
                val blikService = JsonReader().parse(Gdx.files.internal("blik-google-service.json"))
                val key = blikService.getString("private_key")!!.replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "")

                val keySpecPKCS8 = PKCS8EncodedKeySpec(Base64Coder.decodeLines(key))
                val keyFactory = KeyFactory.getInstance("RSA")
                val privKey: PrivateKey = keyFactory.generatePrivate(keySpecPKCS8)

                val jwts: String = Jwts.builder()
                        .setSubject(blikService.getString("client_email"))
                        .setIssuer(blikService.getString("client_email"))
                        .setAudience("https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit")
                        .setExpiration(Date(System.currentTimeMillis() + (60 * 60))) //a java.util.Date
                        .setIssuedAt(Date(System.currentTimeMillis())) // for example, now
                        .claim("uid", jsonValue.getString("id")) //just an example id
                        .signWith(privKey, SignatureAlgorithm.RS256)
                        .compact()

                // Login with token
                GdxFIRAuth.inst().signInWithToken(jwts).then(Consumer { gdxFirebaseUser ->
                    if (gdxFirebaseUser.userInfo.email.isNullOrBlank()) {
                        gdxFirebaseUser.updateEmail(jsonValue.getString("email")).then(Consumer {
                            Gdx.app.log("Fireapp", "UserModel logged: " + gdxFirebaseUser.userInfo.email)
                            loadUserData()
                        }).fail { err, _ ->
                            Gdx.app.log("GDX_FB_FIREBASE", "Email update failed: $err")
                            notifyData(Config.EMAIL_ALREADY_EXISTS)
                        }
                    } else {
                        Gdx.app.log("Fireapp", "UserModel logged: " + gdxFirebaseUser.userInfo.email)
                        loadUserData()
                    }
                }).fail { err, _ ->
                    Gdx.app.log("GDX_FB_FIREBASE", "Token sign in failed: $err")
                    notifyData(Config.LOGIN_FAILED)
                }
            }

            // Error handling
            override fun onError(error: GDXFacebookError) {
                notifyData(Config.LOGIN_FAILED)
                Gdx.app.error("GDX_FB", "Facebook login error: $error")
            }

            // When the user cancels the login process
            override fun onCancel() {
                notifyData(Config.CONN_CANCEL)
            }

            // When the login fails
            override fun onFail(t: Throwable) {
                notifyData(Config.LOGIN_FAILED)
                Gdx.app.error("GDX_FB", "Facebook login failed: $t")
            }
        })
    }

    /** Logouts user, removes and saved preferences in device. Sends notify to observables
     *  about logout. */
    fun userLogout() {
        GdxFIRAuth.inst().signOut()
                .then(Consumer {
                    Gdx.app.log("Fireapp", "User successfully logged out")
                    parent.preferences.putBoolean(PreferenceConfig.EXISTING_USER, false)
                    parent.preferences.putString(PreferenceConfig.USER_LOGIN_TYPE, "")
                    parent.preferences.putString(PreferenceConfig.USERNAME, "universal_name")
                    parent.preferences.putString(PreferenceConfig.EMAIL, "")
                    parent.preferences.putString(PreferenceConfig.EMAIL_PASSWORD, "")
                    parent.preferences.putString(PreferenceConfig.GENDER, EGenderType.UNKNOWN.localeName)
                    parent.preferences.putInteger(PreferenceConfig.AGE, 0)
                    parent.preferences.flush()
                    existingUser = false
                    notifyData(Config.LOGGED_OUT)
                }).fail { s, _ ->
                    Gdx.app.error("Fireapp", "Google firebase logout failed: $s")
                }
    }

    /** Downloads and updates current user data from database in current session.
     * Returns error or success notification to observables.
     */
    private fun loadUserData() {
        GdxFIRDatabase.inst().inReference("users/${GdxFIRAuth.inst().currentUser.userInfo.uid}")
                .readValue(UserModel::class.java)
                .then(Consumer { userModel ->
                    if (userModel == null) {
                        Gdx.app.log("Fireapp", "User data do not exist. Uploading current user.")
                        val nick = if (parent.preferences.getString(PreferenceConfig.USERNAME, "universal_name") == "universal_name") GdxFIRAuth.inst().currentUser.userInfo.displayName
                                ?: user.nick!! else parent.preferences.getString(PreferenceConfig.USERNAME)
                        val um = UserModel(parent.preferences.getInteger(PreferenceConfig.AGE, 0).toLong(), parent.preferences.getString(PreferenceConfig.GENDER, EGenderType.UNKNOWN.localeName), nick)
                        uploadUserData(um)
                    } else {
                        Gdx.app.log("Fireapp", "User data acquired")
                        user = userModel
                        if ((user.age != parent.preferences.getInteger(PreferenceConfig.AGE, 0).toLong() && parent.preferences.getInteger(PreferenceConfig.AGE, 0) != 0)
                                || (user.nick != parent.preferences.getString(PreferenceConfig.USERNAME, "universal_name") && parent.preferences.getString(PreferenceConfig.USERNAME, "universal_name") != "universal_name")
                                || (user.gender != parent.preferences.getString(PreferenceConfig.GENDER, EGenderType.UNKNOWN.localeName) && parent.preferences.getString(PreferenceConfig.GENDER, EGenderType.UNKNOWN.localeName) != EGenderType.UNKNOWN.localeName)) {
                            updateUser(parent.preferences.getString(PreferenceConfig.USERNAME, "universal_name"), parent.preferences.getString(PreferenceConfig.GENDER, EGenderType.UNKNOWN.localeName), parent.preferences.getInteger(PreferenceConfig.AGE, 0).toLong())
                        } else {
                            notifyData(user)
                        }
                    }
                })
                .fail { s, _ ->
                    Gdx.app.error("Fireapp", "Could not load user data: $s")
                    notifyData(Config.DATABASE_ERR)
                }
    }

    /** Uploads updated user data to database
     *  Returns error or success notification to observables.
     * @param um user model pojo class
     */
    private fun uploadUserData(um: UserModel) {
        val map = HashMap<String, Any>()
        map["age"] = um.age!!
        map["gender"] = um.gender!!
        map["nick"] = um.nick!!

        GdxFIRDatabase.inst().inReference("users/${GdxFIRAuth.inst().currentUser.userInfo.uid}").updateChildren(map)
                .then(Consumer {
                    Gdx.app.log("Fireapp", "User successfully updated into database")
                    user = um
                    notifyData(user)
                })
                .fail { s, _ ->
                    Gdx.app.error("Fireapp", "Failed to write user info to database: $s")
                    notifyData(Config.DATABASE_ERR)
                }
    }

    /**
     * Updates user user model pojo class with statistic needy settings
     * Returns error or success notification to observables.
     * @param name user name
     * @param gender user sex
     * @param age user age
     */
    fun updateUser(name: String, gender: String, age: Long) {
        val namePref = if (name == parent.locale?.get("universal_name")) "universal_name" else name
        val um = UserModel(age, gender, namePref)
        uploadUserData(um)
    }

    /**
     * Handles connection and synchronization for game score upload. Uploads game score to the
     * database for specific game. Compares score to today's and alltime best and updates changes
     * if needed.
     * Returns error or success notification to observables.
     * @param gameInfo game interface information
     * @param isScoreUpdated if scores were updated in shared preferences
     */
    fun uploadGameData(gameInfo: IGameCompanion, isScoreUpdated: Boolean) {
        if (!parent.firebaseManager.existingUser) return

        var scm = ScoreModel()
        GdxFIRDatabase.inst().inReference("users/${GdxFIRAuth.inst().currentUser.userInfo.uid}/${gameInfo.localeName}")
                .readValue(ScoreModel::class.java)
                .then(Consumer { scoreModel ->
                    if (scoreModel == null) Gdx.app.log("Fireapp", "Score data for ${gameInfo.localeName} does not exist")
                    else {
                        Gdx.app.log("Fireapp", "Score model for ${gameInfo.localeName} successfully loaded")
                        scm = scoreModel
                    }

                    if (isScoreUpdated) {
                        scm.bestScores = parent.gameManager.getTopScorePreferences(gameInfo).map { it.toLong() } as ArrayList<Long>
                        scm.scoresInWeek = parent.gameManager.getWeekScorePreferencesToDbMap(gameInfo) as HashMap<String, Long>
                    }
                    scm.currentDifficulty = parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0]).toLong()
                    scm.timesPlayed = parent.preferences.getInteger(gameInfo.timesPlayedPreference, 1).toLong()

                    GdxFIRDatabase.inst().inReference("users/${GdxFIRAuth.inst().currentUser.userInfo.uid}/${gameInfo.localeName}").setValue(scm)
                            .then(Consumer {
                                Gdx.app.log("Fireapp", "Game data successfuly updated.")
                            })
                            .fail { s, _ ->
                                Gdx.app.error("Fireapp", "Failed to update score data: $s")
                                notifyData(Config.DATABASE_ERR)
                            }
                }).fail { s, _ ->
                    Gdx.app.error("Fireapp", "Could not load score model: $s")
                    notifyData(Config.DATABASE_ERR)
                }
    }

    /**
     * Synchronizes all game data from database with the logged-in device
     */
    private fun synchronizeData() {
        if (!parent.firebaseManager.existingUser) return

        EGameType.values().forEach { type ->
            type.games.forEach { gameInfo ->
                GdxFIRDatabase.inst().inReference("users/${GdxFIRAuth.inst().currentUser.userInfo.uid}/${gameInfo.localeName}")
                        .readValue(ScoreModel::class.java)
                        .then(Consumer { scoreModel ->
                            var scm = ScoreModel()
                            if (scoreModel == null) Gdx.app.log("Fireapp", "Score data for ${gameInfo.localeName} does not exist")
                            else {
                                Gdx.app.log("Fireapp", "Score model for ${gameInfo.localeName} successfully loaded")
                                scm = scoreModel
                            }

                            //difficulty update
                            if (parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0]) >= (scm.currentDifficulty?.toInt()
                                            ?: GameConfig.GAME_DIFFICULTIES[0])) {
                                scm.currentDifficulty = parent.preferences.getInteger(gameInfo.currentDifficultyPreference, GameConfig.GAME_DIFFICULTIES[0]).toLong()
                            } else {
                                parent.preferences.putInteger(gameInfo.currentDifficultyPreference, scm.currentDifficulty!!.toInt())
                            }

                            //times played update
                            if (parent.preferences.getInteger(gameInfo.timesPlayedPreference, 0) >= (scm.timesPlayed?.toInt()
                                            ?: 0)) {
                                scm.timesPlayed = parent.preferences.getInteger(gameInfo.timesPlayedPreference, 0).toLong()
                            } else {
                                parent.preferences.putInteger(gameInfo.timesPlayedPreference, scm.timesPlayed!!.toInt())
                            }

                            //top 10 scores update
                            val synchronizedTopScores = parent.gameManager.synchronizeTopScores(parent.gameManager.getTopScorePreferences(gameInfo), scm.bestScores)
                                    ?: emptyList()
                            parent.preferences.putString(gameInfo.topScoresPreference, synchronizedTopScores.joinToString("-"))
                            scm.bestScores = synchronizedTopScores.map { it.toLong() } as ArrayList<Long>

                            //week scores update
                            val synchronizedWeekScores =
                                    parent.gameManager.synchronizeWeekScores(
                                            parent.gameManager.getWeekScorePreferencesToDbMap(gameInfo) as HashMap<String, Long>,
                                            scm.scoresInWeek)
                                            ?: emptyMap()
                            var weekScoresStr = ""
                            synchronizedWeekScores.forEach {
                                weekScoresStr += "${it.key.replace("-", ".")}.${it.value}-"
                            }
                            weekScoresStr = weekScoresStr.dropLast(1)
                            parent.preferences.putString(gameInfo.weekScoresPreference, weekScoresStr)
                            scm.scoresInWeek = synchronizedWeekScores as HashMap<String, Long>

                            parent.preferences.flush()

                            GdxFIRDatabase.inst().inReference("users/${GdxFIRAuth.inst().currentUser.userInfo.uid}/${gameInfo.localeName}").setValue(scm)
                                    .then(Consumer {
                                        Gdx.app.log("Fireapp", "Game data successfuly updated.")
                                    })
                                    .fail { s, _ ->
                                        Gdx.app.error("Fireapp", "Failed to update score data: $s")
                                        notifyData(Config.DATABASE_ERR)
                                    }
                        }).fail { s, _ ->
                            Gdx.app.error("Fireapp", "Could not load score model: $s")
                            notifyData(Config.DATABASE_ERR)
                        }
            }
        }
    }

    /**
     * Loads global statistics to for statistics screen. This lets the user compare with other users
     * on the statistics screen.
     */
    fun getGlobalStatistics(game: String) {
        if (game.isBlank()) {
            GdxFIRDatabase.inst().inReference("games/")
                    .readValue(Map::class.java)
                    .then(Consumer { gamesMap ->
                        val outputMap = HashMap<String, List<Long>>()

                        EGameType.values().forEach {
                            var gameAverage = 0L
                            var gameTop = 0L
                            it.games.forEach { ot ->
                                gameAverage += ((gamesMap[ot.localeName] as Map<*, *>)["averageScore"] as Long)
                                gameTop += ((gamesMap[ot.localeName] as Map<*, *>)["theBestScore"] as Long)
                            }
                            outputMap[it.name] = arrayListOf(gameAverage / it.games.size, gameTop / it.games.size)
                        }

                        notifyData(outputMap)
                    }).fail { s, _ ->
                        Gdx.app.error("Fireapp", "Could not load global games data: $s")
                        notifyData(Config.DATABASE_ERR)
                    }
        } else {
            GdxFIRDatabase.inst().inReference("games/$game")
                    .readValue(GlobalStatModel::class.java)
                    .then(Consumer { globalStatModel ->
                        if (globalStatModel != null) notifyData(globalStatModel)
                    }).fail { s, _ ->
                        Gdx.app.error("Fireapp", "Could not load the global stat model for game $game: $s")
                        notifyData(Config.DATABASE_ERR)
                    }
        }
    }

    /**
     * Computes global scores for each game and updates its records in games database collection.
     * This method simulates cron job that should work every day automatically.
     */
    fun calculateGlobalStats() {
        GdxFIRDatabase.inst().inReference("users/")
                .readValue(Map::class.java)
                .then(Consumer { users ->
                    val gamesMap = HashMap<String, GlobalStatModel>()
                    val tempCategoryMap = HashMap<String, HashMap<String, ArrayList<ArrayList<Long>>>>()

                    users.forEach {
                        val currUser = (it.value as Map<*, *>)
                        EGameType.values().forEach { type ->
                            type.games.forEach { gameInfo ->

                                if (tempCategoryMap[gameInfo.localeName] == null) {
                                    tempCategoryMap[gameInfo.localeName] = HashMap()
                                    tempCategoryMap[gameInfo.localeName]!!["age"] = ArrayList()
                                    tempCategoryMap[gameInfo.localeName]!!["avgage"] = ArrayList()
                                    tempCategoryMap[gameInfo.localeName]!!["difficulty"] = ArrayList()
                                    tempCategoryMap[gameInfo.localeName]!!["avgdifficulty"] = ArrayList()

                                    for (i in 0..9) {
                                        tempCategoryMap[gameInfo.localeName]!!["age"]!!.add(ArrayList())
                                        tempCategoryMap[gameInfo.localeName]!!["avgage"]!!.add(ArrayList())
                                        tempCategoryMap[gameInfo.localeName]!!["difficulty"]!!.add(ArrayList())
                                        tempCategoryMap[gameInfo.localeName]!!["avgdifficulty"]!!.add(ArrayList())
                                    }
                                }

                                //if model exist for game
                                if (gamesMap[gameInfo.localeName] == null) gamesMap[gameInfo.localeName] = GlobalStatModel(0L)

                                if (currUser[gameInfo.localeName] != null
                                        && (currUser[gameInfo.localeName] as Map<*, *>)["bestScores"] != null) {
                                    val scores = ((currUser[gameInfo.localeName]) as Map<*, *>)["bestScores"] as ArrayList<Long>
                                    val avgscores = scores.toMutableList()
                                    avgscores.removeAll { it == 0L }

                                    //global best scores
                                    if (gamesMap[gameInfo.localeName]!!.theBestScore!! < scores.first()) {
                                        gamesMap[gameInfo.localeName]!!.theBestScore = scores.first()
                                    }

                                    //age temp map filling
                                    if (currUser["age"] != null && currUser["age"] in 1L..99L && scores.first() > 0) {
                                        tempCategoryMap[gameInfo.localeName]!!["age"]?.get(((currUser["age"] as Long) / 10).toInt())?.add(scores.first())
                                        tempCategoryMap[gameInfo.localeName]!!["avgage"]?.get(((currUser["age"] as Long) / 10).toInt())?.add(avgscores.average().toLong())
                                    }

                                    //difficulty temp map filling
                                    if ((currUser[gameInfo.localeName] as Map<*, *>)["currentDifficulty"] != null && scores.first() > 0) {
                                        tempCategoryMap[gameInfo.localeName]!!["difficulty"]?.get((((currUser[gameInfo.localeName] as Map<*, *>)["currentDifficulty"] as Long) - 1).toInt())?.add(scores.first())
                                        tempCategoryMap[gameInfo.localeName]!!["avgdifficulty"]?.get((((currUser[gameInfo.localeName] as Map<*, *>)["currentDifficulty"] as Long) - 1).toInt())?.add(avgscores.average().toLong())
                                    }

                                }
                            }
                        }
                    }

                    //Mapping all values into the Global stat model POJO class
                    gamesMap.forEach {
                        it.value.scoreByDifficulty!!["theBestScore"] = tempCategoryMap[it.key]!!["difficulty"]!!.map { list ->
                            list.max() ?: 0L
                        }.toCollection(ArrayList())
                        it.value.scoreByDifficulty!!["averageScore"] = tempCategoryMap[it.key]!!["avgdifficulty"]!!.map { list -> list.average().toLong() }.toCollection(ArrayList())
                        it.value.scoreByAge!!["theBestScore"] = tempCategoryMap[it.key]!!["age"]!!.map { list ->
                            list.max() ?: 0L
                        }.toCollection(ArrayList())
                        it.value.scoreByAge!!["averageScore"] = tempCategoryMap[it.key]!!["avgage"]!!.map { list -> list.average().toLong() }.toCollection(ArrayList())

                        val globalAvgList = ArrayList<Long>()
                        it.value.scoreByDifficulty!!["averageScore"]?.forEach { value -> if (value != 0L) globalAvgList.add(value) }
                        it.value.scoreByAge!!["averageScore"]?.forEach { value -> if (value != 0L) globalAvgList.add(value) }

                        it.value.averageScore = globalAvgList.average().toLong()
                    }

                    //update global best values
                    GdxFIRDatabase.inst().inReference("games/").updateChildren(gamesMap as Map<String, Any>?)
                            .then(Consumer {
                                Gdx.app.log("Fireapp", "The global best scores data successfuly updated.")
                            })
                            .fail { s, _ ->
                                Gdx.app.error("Fireapp", "Failed to update the global best score data: $s")
                                notifyData(Config.DATABASE_ERR)
                            }
                }).fail { s, _ ->
                    Gdx.app.error("Fireapp", "Could not get all users data: $s")
                }
    }

    /**
     * Does the user has admin rights in the database.
     */
    fun checkAdminRights() {
        if (isAdmin || !existingUser) return
        GdxFIRDatabase.inst().inReference("users/${GdxFIRAuth.inst().currentUser.userInfo.uid}/admin")
                .readValue(Long::class.java)
                .then(Consumer { admin ->
                    if (admin != null && admin == 1L) {
                        isAdmin = true
                        notifyData(isAdmin)
                        Gdx.app.log("Fireapp", "Admin rights successfully achieved by: ${GdxFIRAuth.inst().currentUser.userInfo}")
                    }
                }).fail { s, _ ->
                    Gdx.app.error("Fireapp", "Could not load the admin rights: $s")
                }
    }

    fun sendLostPassEmail(email: String){
        if (email.isBlank() || !isEmailValid(email)) {
            notifyData(Config.EMAIL_NOT_VALID)
            return
        }
        GdxFIRAuth.inst().sendPasswordResetEmail(email)
                .then(Consumer {
                    notifyData(Config.PASSWORD_RESET_OK)
                    Gdx.app.log("Fireapp", "Password reset email has been successfully send.")
                }).fail { s, _ ->
                    Gdx.app.error("Fireapp", "Could not send password reset email: $s")
                    if (s.contains("There is no user record")) notifyData(Config.PASSWORD_RESET_OK)
                    else notifyData(Config.PASSWORD_RESET_FAIL)
                }
    }

    /**
     *  Regex check for valid email structure.
     *  @param email checked email
     */
    private fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    /**
     * Function to lastly process and afterwards notify data to observers. Sends any response
     * except successful connection check.
     * @param data any data that needs to be notified
     */
    protected fun notifyData(data: Any) {
        if (data is UserModel) {
            existingUser = true
            hasConnection = true
            hasAlreadySynchronized = true

            parent.preferences.putBoolean(PreferenceConfig.EXISTING_USER, existingUser)
            parent.preferences.putString(PreferenceConfig.USER_LOGIN_TYPE, lastLoginType)
            parent.preferences.putString(PreferenceConfig.USERNAME, user.nick)
            parent.preferences.putInteger(PreferenceConfig.AGE, user.age!!.toInt())
            parent.preferences.putString(PreferenceConfig.GENDER, user.gender)
            parent.preferences.flush()
            synchronizeData()
//            GdxFIRAnalytics.inst().logEvent(AnalyticsEvent.SIGN_UP, mutableMapOf(Pair(AnalyticsParam.SIGN_UP_METHOD, lastLoginType)))
//            GdxFIRAnalytics.inst().logEvent(AnalyticsEvent.LOGIN, mutableMapOf(Pair(AnalyticsParam.SIGN_UP_METHOD, lastLoginType)))
//            GdxFIRAnalytics.inst().setUserProperty("user_age", user.age.toString())
//            GdxFIRAnalytics.inst().setUserProperty("user_gender", user.gender)
        } else if (data is String && data != Config.CONN_OK && data != Config.CONN_ERR && data != Config.CONN_CANCEL) {
            checkConnection()
        }
        isConnecting = data is String && data == Config.CONN_OK && isConnecting

        if (data != Config.CONN_OK) {
            setChanged()
            notifyObservers(data)
        }
    }

    /**
     * Method to check user connection. It is taken into account that www.google.com is
     * always available for ping command.
     */
    private fun checkConnection() {
        val request = Net.HttpRequest(Net.HttpMethods.GET)
        request.url = "https://google.com"
        Gdx.net.sendHttpRequest(request, this)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see HttpResponseListener.handleHttpResponse()
     */
    override fun handleHttpResponse(httpResponse: Net.HttpResponse) {
        hasConnection = httpResponse.status.statusCode in 200 until 300
        notifyData(hasConnection)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see HttpResponseListener.failed()
     */
    override fun failed(t: Throwable) {
        hasConnection = false
        notifyData(Config.CONN_ERR)
    }

    /**
     * This method is inherited. Description is in parent class
     * @see HttpResponseListener.cancelled()
     */
    override fun cancelled() {
        hasConnection = false
        notifyData(Config.CONN_ERR)
    }

}