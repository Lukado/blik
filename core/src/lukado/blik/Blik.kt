package lukado.blik

import com.badlogic.gdx.Application
import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Preferences
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.utils.I18NBundle
import com.badlogic.gdx.utils.viewport.ExtendViewport
import lukado.blik.configuration.AssetConfig
import lukado.blik.configuration.Config
import lukado.blik.configuration.PreferenceConfig
import lukado.blik.enums.EScreen
import lukado.blik.managers.BlikAssetManager
import lukado.blik.managers.FirebaseManager
import lukado.blik.managers.GameManager
import lukado.blik.screens.*
import java.util.*

/**
 * Main core class that launches this application. Application is focused on person
 * cognitive training.
 *
 * @author Petr Lukasik
 */
class Blik : Game(), Observer {
    //Screens
    private var loadingScreen: LoadingScreen? = null
    private var loginScreen: LoginScreen? = null
    private var menuScreen: MenuScreen? = null
    private var settingsScreen: SettingsScreen? = null
    private var gamesScreen: GamesScreen? = null
    private var statsScreen: StatisticsScreen? = null

    //Tools
    lateinit var manager: BlikAssetManager
    lateinit var firebaseManager: FirebaseManager
    lateinit var gameManager: GameManager

    //Class specific variables
    lateinit var stage: Stage
    lateinit var preferences: Preferences
    var skin: Skin? = null
    var locale: I18NBundle? = null

    /** This method is inherited. Description is in parent class */
    override fun create() {
        if (!Config.RELEASE) {
            Gdx.app.logLevel = Application.LOG_DEBUG
        }

        Gdx.graphics.setVSync(true)

        stage = Stage()
        stage.viewport = ExtendViewport(Config.APP_WIDTH, Config.APP_HEIGHT)

        preferences = Gdx.app.getPreferences(Config.PREFERENCES)
        manager = BlikAssetManager(this)
        firebaseManager = FirebaseManager(this)
        gameManager = GameManager(this)

        loadingScreen = LoadingScreen(this)
        setScreen(loadingScreen)
    }

    /** This method is inherited. Description is in parent class */
    override fun render() {
        if (screen != null) {
            screen.render(Gdx.graphics.deltaTime)
        }
    }

    /** This method is inherited. Description is in parent class */
    override fun resize(width: Int, height: Int) {
        if (screen != null) {
            screen.resize(width, height)
        }
    }

    /** This method is inherited. Description is in parent class */
    override fun dispose() {
        skin?.dispose()
        manager.am.dispose()
        screen?.dispose()
        stage.dispose()
    }

    /**
     * Changes the screen view of the application
     * @param screen requested screen view
     */
    fun changeScreen(screen: EScreen) {
        when (screen) {
            EScreen.MENU -> {
                if (menuScreen == null) {
                    menuScreen = MenuScreen(this)
                    settingsScreen = SettingsScreen(this)
                    gamesScreen = GamesScreen(this)
                    statsScreen = StatisticsScreen(this)
                    loginScreen?.dispose()
                    loadingScreen?.dispose()
                }
                setScreen(menuScreen)
            }
            EScreen.SETTINGS -> {
                setScreen(settingsScreen)
            }
            EScreen.GAMES -> {
                setScreen(gamesScreen)
            }
            EScreen.STATS -> {
                setScreen(statsScreen)
            }
            EScreen.LOGIN -> {
                loadDefaultAssets()
                if (firebaseManager.existingUser) {
                    firebaseManager.addObserver(this)
                    if (this.screen is LoadingScreen) {
                        (this.screen as LoadingScreen).title?.setText(locale?.get("currently_signing"))
                    }
                    when (preferences.getString(PreferenceConfig.USER_LOGIN_TYPE)) {
                        Config.EMAIL -> {
                            firebaseManager.emailLogin(preferences.getString(PreferenceConfig.EMAIL, ""), preferences.getString(PreferenceConfig.EMAIL_PASSWORD, ""))
                        }
                        Config.FACEBOOK -> {
                            firebaseManager.facebookLogin()
                        }
                        Config.GOOGLE -> {
                            firebaseManager.googleLogin()
                        }
                        else -> firebaseManager.userLogout()
                    }
                } else {
                    loginScreen = LoginScreen(this)
                    setScreen(loginScreen)
                }
            }
            else -> {
                if (menuScreen == null) {
                    menuScreen = MenuScreen(this)
                    settingsScreen = SettingsScreen(this)
                    gamesScreen = GamesScreen(this)
                    statsScreen = StatisticsScreen(this)
                }
                setScreen(menuScreen)
            }
        }
    }

    /** Disposes all main screens. Used to dispose unnecessary memory consumption while launching a game. */
    fun disposeMenuScreens() {
        menuScreen?.dispose()
        settingsScreen?.dispose()
        gamesScreen?.dispose()
        loginScreen?.dispose()
        loadingScreen?.dispose()
        statsScreen?.dispose()
    }

    /** Loads default app skin and user locale */
    private fun loadDefaultAssets() {
        skin = manager.am.get(AssetConfig.SKIN)
        locale = manager.am.get(AssetConfig.LOCALES, I18NBundle::class.java)
    }

    /** This method is inherited. Updates data from observer notifications. */
    override fun update(p0: Observable?, p1: Any?) {
        Gdx.app.postRunnable(Runnable {
            if (screen !is LoadingScreen) return@Runnable
            firebaseManager.deleteObserver(this)
            changeScreen(EScreen.MENU)
        })
    }
}