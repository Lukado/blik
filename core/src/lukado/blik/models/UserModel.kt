package lukado.blik.models

import lukado.blik.enums.EGenderType

/**
 * User pojo user class for firebase database usage
 */
class UserModel {
    var age: Long? = null
    var gender: String? = null
    var nick: String? = null

    /** Do not delete empty constructor, important for firebase chaining */
    constructor()

    /** Default constructor for object
     * @param age user age
     * @param gender user sex
     * @param nick user name
     */
    constructor(age: Long, gender: String, nick: String) {
        this.nick = nick
        this.gender = gender
        this.age = age
    }

    /** clears current object properties */
    fun clear(nick: String) {
        this.nick = nick
        this.gender = EGenderType.UNKNOWN.localeName
        this.age = 0L
    }

    override fun toString(): String {
        return "$nick: $gender $age"
    }
}