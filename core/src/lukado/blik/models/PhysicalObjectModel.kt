package lukado.blik.models

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.physics.box2d.Body

/**
 * Custom handling of physical Box2D world objects.
 */
open class PhysicalObjectModel(val body: Body, private var worldScale: Float) {
    var sprite: Sprite? = null
    var dead: Boolean = false

    val realWidth: Float
        get() {
            return (sprite?.width ?: 0f) * worldScale
        }
    val realHeight: Float
        get() {
            return (sprite?.height ?: 0f) * worldScale
        }

    init {
        if (body.fixtureList.notEmpty() && body.fixtureList.first().userData is Sprite) {
            sprite = body.fixtureList.first().userData as Sprite
            countScale()
            sprite!!.setScale(worldScale)
        }
    }

    /** Recalculates real required size of world object, which is normalized by default. Computes
     * real size for sprite and Box2D world scaling
     */
    private fun countScale() {
        var scale: Float
        val originalWidth = sprite!!.width
        val originalHeight = sprite!!.height

        val boundSize = if (sprite!!.width < sprite!!.height) {
            worldScale / sprite!!.width
        } else {
            worldScale / sprite!!.height
        }
        scale = boundSize * originalHeight / originalWidth

        if (scale > boundSize) {
            scale = boundSize
        }
        worldScale = scale
    }

    override fun toString(): String {
        return body.userData as String? ?: "nuthing"
    }
}