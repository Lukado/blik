package lukado.blik.models

import com.badlogic.gdx.utils.JsonReader

/**
 * User pojo global statistics class for firebase database usage
 */
class GlobalStatModel {
    var theBestScore: Long? = null
    var averageScore: Long? = null
    var scoreByDifficulty: HashMap<String, ArrayList<Long>>? = null
    var scoreByAge: HashMap<String, ArrayList<Long>>? = null

    /** Do not delete empty constructor, important for firebase chaining */
    constructor()

    constructor(defaultScores: Long) {
        theBestScore = defaultScores
        averageScore = defaultScores

        scoreByDifficulty = HashMap()
        scoreByDifficulty!!["theBestScore"] = ArrayList()
        scoreByDifficulty!!["averageScore"] = ArrayList()

        scoreByAge = HashMap()
        scoreByAge!!["theBestScore"] = ArrayList()
        scoreByAge!!["averageScore"] = ArrayList()
    }

    override fun toString(): String {
        return "The best=$theBestScore, Average=$averageScore"
    }

    fun cleanJsonData(){
        var parsed = JsonReader().parse(scoreByDifficulty?.get("theBestScore")?.toString())
        scoreByDifficulty?.get("theBestScore")?.clear()
        scoreByDifficulty!!["theBestScore"] = parsed.map { it.get("value").asLong() }.toCollection(ArrayList())

        parsed = JsonReader().parse(scoreByDifficulty?.get("averageScore")?.toString())
        scoreByDifficulty?.get("averageScore")?.clear()
        scoreByDifficulty!!["averageScore"] = parsed.map { it.get("value").asLong() }.toCollection(ArrayList())

        parsed = JsonReader().parse(scoreByAge?.get("theBestScore")?.toString())
        scoreByAge?.get("theBestScore")?.clear()
        scoreByAge!!["theBestScore"] = parsed.map { it.get("value").asLong() }.toCollection(ArrayList())

        parsed = JsonReader().parse(scoreByAge?.get("averageScore")?.toString())
        scoreByAge?.get("averageScore")?.clear()
        scoreByAge!!["averageScore"] = parsed.map { it.get("value").asLong() }.toCollection(ArrayList())
    }
}