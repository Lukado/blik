package lukado.blik.models

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

/**
 * User pojo score class for firebase database usage
 */
class ScoreModel {
    var currentDifficulty: Long? = null
    var timesPlayed: Long? = null
    var scoresInWeek: HashMap<String, Long>? = null
    var bestScores: ArrayList<Long>? = null
    companion object {
        val mapFormat: String = SimpleDateFormat("EEE", Locale.ENGLISH).format(Calendar.getInstance().time)
        val mapDayFormat: String = SimpleDateFormat("d.M.", Locale.ENGLISH).format(Calendar.getInstance().time)
    }

    /** Do not delete empty constructor, important for firebase chaining */
    constructor()

    override fun toString(): String {
        return "Times played=$timesPlayed : Current difficulty=$currentDifficulty : Best score=${arrayOf(bestScores).contentToString()} : Week scores=$scoresInWeek}"
    }
}